module SDL2FS.Core.Tests.Init

open Xunit
open SDL

[<Fact>]
let ``Initialize System With No Flags`` () =
    use s = new System([Flags.None])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Timer Flag`` () =
    use s = new System([Flags.Timer])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Audio Flag`` () =
    use s = new System([Flags.Audio])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Video Flag`` () =
    use s = new System([Flags.Video])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Joystick Flag`` () =
    use s = new System([Flags.Joystick])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Haptic Flag`` () =
    use s = new System([Flags.Haptic])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Game Controller Flag`` () =
    use s = new System([Flags.GameController])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Events Flag`` () =
    use s = new System([Flags.Events])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything`` () =
    use s = new System([Flags.Everything])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With No Flags Then Add Timer`` () =
    use s = new System([Flags.None])
    Assert.True(s.InitSubSystem([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With No Flags Then Add Audio`` () =
    use s = new System([Flags.None])
    Assert.True(s.InitSubSystem([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With No Flags Then Add Video`` () =
    use s = new System([Flags.None])
    Assert.True(s.InitSubSystem([Flags.Video]))
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With No Flags Then Add Joystick`` () =
    use s = new System([Flags.None])
    Assert.True(s.InitSubSystem([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With No Flags Then Add Haptic`` () =
    use s = new System([Flags.None])
    Assert.True(s.InitSubSystem([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With No Flags Then Add Game Controller`` () =
    use s = new System([Flags.None])
    Assert.True(s.InitSubSystem([Flags.GameController]))
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With No Flags Then Add Events`` () =
    use s = new System([Flags.None])
    Assert.True(s.InitSubSystem([Flags.Events]))
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything Then Quit Timer`` () =
    use s = new System([Flags.Everything])
    s.QuitSubSystem([Flags.Timer])
    Assert.False(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything Then Quit Audio`` () =
    use s = new System([Flags.Everything])
    s.QuitSubSystem([Flags.Audio])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.False(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything Then Quit Video`` () =
    use s = new System([Flags.Everything])
    s.QuitSubSystem([Flags.Video])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.False(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything Then Quit Joystick`` () =
    use s = new System([Flags.Everything])
    s.QuitSubSystem([Flags.Joystick])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything Then Quit Haptic`` () =
    use s = new System([Flags.Everything])
    s.QuitSubSystem([Flags.Haptic])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.False(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything Then Quit Game Controller`` () =
    use s = new System([Flags.Everything])
    s.QuitSubSystem([Flags.GameController])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.False(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.False(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

[<Fact>]
let ``Initialize System With Everything Then Quit Events`` () =
    use s = new System([Flags.Everything])
    s.QuitSubSystem([Flags.Events])
    Assert.True(s.WasInit([Flags.Timer]))
    Assert.True(s.WasInit([Flags.Audio]))
    Assert.True(s.WasInit([Flags.Video]))
    Assert.True(s.WasInit([Flags.Joystick]))
    Assert.True(s.WasInit([Flags.Haptic]))
    Assert.True(s.WasInit([Flags.GameController]))
    Assert.True(s.WasInit([Flags.Events]))

