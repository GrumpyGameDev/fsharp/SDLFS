module SDL2FS.Core.Tests.Error

open Xunit
open SDL

[<Fact>]
let ``Ensure Setting Error Allows Subsequent Getting Of That Same Error Message`` () =
    let errorMessage = "Hello, error!"
    Error.set errorMessage
    let result = Error.get()
    Assert.Equal(errorMessage, result)

[<Fact>]
let ``Ensure Clearing Error Allows Subsequent Getting Of A Blank Error Message`` () =
    let errorMessage = "Hello, error!"
    Error.set errorMessage
    Error.clear()
    let result = Error.get()
    Assert.Equal("", result)

