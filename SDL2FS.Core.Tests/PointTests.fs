module SDL2FS.Core.Tests.Point

open Xunit
open SDL

[<Fact>]
let ``Point.create will construct a point from x and y`` () =
    let x = 1
    let y = 2

    let pt = 
        (x,y) |> Point.create

    Assert.Equal(x, pt.X)
    Assert.Equal(y, pt.Y)

[<Fact>]
let ``Point has a member SetX that will assign the X value of the point`` () =
    let firstX = 1
    let y = 2
    let secondX = 3

    let pt = ((firstX,y) |> Point.create).SetX secondX

    Assert.Equal(secondX, pt.X)
    Assert.Equal(y, pt.Y)

[<Fact>]
let ``Point has a member SetY that will assign the Y value of the point`` () =
    let x = 1
    let firstY = 2
    let secondY = 3

    let pt = ((x,firstY) |> Point.create).SetY secondY

    Assert.Equal(x, pt.X)
    Assert.Equal(secondY, pt.Y)

[<Fact>]
let ``Point has a member MoveX that will adjust the X value of the point`` () =
    let x = 1
    let y = 2
    let deltaX = 3

    let pt = ((x,y) |> Point.create).MoveX deltaX

    Assert.Equal(x+deltaX, pt.X)
    Assert.Equal(y, pt.Y)

[<Fact>]
let ``Point has a member MoveY that will adjust the Y value of the point`` () =
    let x = 1
    let y = 2
    let deltaY = 3

    let pt = ((x,y) |> Point.create).MoveY deltaY

    Assert.Equal(x, pt.X)
    Assert.Equal(y+deltaY, pt.Y)

[<Fact>]
let ``Point has a member MoveBy that will adjust the X and Y values of the point`` () =
    let x1 = 1
    let y1 = 2
    let x2 = 3
    let y2 = 4

    let pt = ((x1,y1) |> Point.create).MoveBy ((x2,y2) |> Point.create)

    Assert.Equal(x1+x2, pt.X)
    Assert.Equal(y1+y2, pt.Y)

[<Fact>]
let ``Point has a member ScaleX that will adjust the X value of the point`` () =
    let x = 2
    let y = 3
    let scaleX = 4

    let pt = ((x,y) |> Point.create).ScaleX scaleX

    Assert.Equal(x*scaleX, pt.X)
    Assert.Equal(y, pt.Y)

[<Fact>]
let ``Point has a member ScaleY that will adjust the Y value of the point`` () =
    let x = 2
    let y = 3
    let scaleY = 4

    let pt = ((x,y) |> Point.create).ScaleY scaleY

    Assert.Equal(x, pt.X)
    Assert.Equal(y*scaleY, pt.Y)

[<Fact>]
let ``Point has a member ScaleBy that will adjust the X and Y values of the point`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let pt = ((x1,y1) |> Point.create).ScaleBy ((x2,y2) |> Point.create)

    Assert.Equal(x1*x2, pt.X)
    Assert.Equal(y1*y2, pt.Y)

[<Fact>]
let ``Point has a unary minus that will adjust the X and Y values of the point`` () =
    let x = 2
    let y = 3

    let pt = -((x,y) |> Point.create)

    Assert.Equal(-x, pt.X)
    Assert.Equal(-y, pt.Y)

[<Fact>]
let ``Point has a unary plus that will not adjust the X and Y values of the point`` () =
    let x = 2
    let y = 3

    let pt = +((x,y) |> Point.create)

    Assert.Equal(x, pt.X)
    Assert.Equal(y, pt.Y)

[<Fact>]
let ``Point has a binary plus that will add the X and Y values two points`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let pt = ((x1,y1) |> Point.create) + ((x2,y2) |> Point.create)

    Assert.Equal(x1+x2, pt.X)
    Assert.Equal(y1+y2, pt.Y)

[<Fact>]
let ``Point has a binary minus that will add the X and Y values two points`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let pt = ((x1,y1) |> Point.create) - ((x2,y2) |> Point.create)

    Assert.Equal(x1-x2, pt.X)
    Assert.Equal(y1-y2, pt.Y)

[<Fact>]
let ``Point has a binary multiply that will multiply the X and Y values two points`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let pt = ((x1,y1) |> Point.create) * ((x2,y2) |> Point.create)

    Assert.Equal(x1*x2, pt.X)
    Assert.Equal(y1*y2, pt.Y)

[<Fact>]
let ``Point.moveBy will add the X and Y values two points`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let pt = 
        (((x1,y1) |> Point.create), ((x2,y2) |> Point.create))
        ||> Point.moveBy

    Assert.Equal(x1+x2, pt.X)
    Assert.Equal(y1+y2, pt.Y)

[<Fact>]
let ``Point.scaleBy will multiply the X and Y values two points`` () =
    let x1 = 2
    let y1 = 3
    let x2 = 4
    let y2 = 5

    let pt = 
        (((x1,y1) |> Point.create), ((x2,y2) |> Point.create))
        ||> Point.scaleBy

    Assert.Equal(x1*x2, pt.X)
    Assert.Equal(y1*y2, pt.Y)
