module SDL2FS.Core.Tests.Version

open Xunit
open SDL

let private expectedVersionMajor = 2uy
let private expectedVersionMinor = 0uy
let private expectedVersionPatch  = 8uy
let private expectedRevisionString = "hg-11914:f1084c419f33"
let private expectedRevisionNumber = 11914

[<Fact>]
let ``Ensure SDL Version Is Correct`` () =
    let version = Version.get()
    Assert.Equal(expectedVersionMajor, version.Major)
    Assert.Equal(expectedVersionMinor, version.Minor)
    Assert.Equal(expectedVersionPatch, version.Patch)

[<Fact>]
let ``Ensure SDL Revision Is Correct`` () =
    let revision = Version.getRevision()
    Assert.Equal(expectedRevisionString, revision)

[<Fact>]
let ``Ensure SDL Revision Number is 11914`` () =
    let revisionNumber = Version.getRevisionNumber()
    Assert.Equal(expectedRevisionNumber, revisionNumber)
