module SDL2FS.Core.Tests.Rectangle

open Xunit
open SDL

[<Fact>]
let ``Rectangle.create will make a Rectangle`` () =
    let x = 2
    let y = 3
    let w = 4
    let h = 5

    let r = (x,y,w,h) |> Rectangle.create

    Assert.Equal(x,r.X)
    Assert.Equal(y,r.Y)
    Assert.Equal(w,r.Width)
    Assert.Equal(h,r.Height)

[<Fact>]
let ``Rectangle has a member Contains that checks if a point is contained within the Rectangle`` () =
    let x = 2
    let y = 3
    let w = 4
    let h = 5

    let upperLeft = (x,y) |> Point.create
    let upperRight = (x+w,y) |> Point.create
    let lowerLeft = (x,y+h) |> Point.create
    let lowerRight = (x+w,y+h) |> Point.create
    let middle = (x+w/2,y+w/2) |> Point.create
    let almostLowerRight = (x+w-1,y+h-1) |> Point.create

    let r = (x,y,w,h) |> Rectangle.create

    Assert.True(r.Contains upperLeft)
    Assert.False(r.Contains upperRight)
    Assert.False(r.Contains lowerLeft)
    Assert.False(r.Contains lowerRight)
    Assert.True(r.Contains middle)
    Assert.True(r.Contains almostLowerRight)


[<Fact>]
let ``Rectangle.contains that checks if a point is contained within the Rectangle`` () =
    let x = 2
    let y = 3
    let w = 4
    let h = 5

    let upperLeft = (x,y) |> Point.create
    let upperRight = (x+w,y) |> Point.create
    let lowerLeft = (x,y+h) |> Point.create
    let lowerRight = (x+w,y+h) |> Point.create
    let middle = (x+w/2,y+w/2) |> Point.create
    let almostLowerRight = (x+w-1,y+h-1) |> Point.create

    let r = (x,y,w,h) |> Rectangle.create

    Assert.True(r |> Rectangle.contains upperLeft)
    Assert.False(r |> Rectangle.contains upperRight)
    Assert.False(r |> Rectangle.contains lowerLeft)
    Assert.False(r |> Rectangle.contains lowerRight)
    Assert.True(r |> Rectangle.contains middle)
    Assert.True(r |> Rectangle.contains almostLowerRight)

[<Fact>]
let ``Rectangle has a member IsEmpty that checks for a valid width and height in a rectangle`` () =
    let x = 2
    let y = 3
    let validWidth = 1
    let validHeight = 1
    let invalidWidth = 0
    let invalidHeight = 0

    let r1 = (x,y,validWidth,validHeight) |> Rectangle.create
    let r2 = (x,y,invalidWidth,validHeight) |> Rectangle.create
    let r3 = (x,y,validWidth,invalidHeight) |> Rectangle.create
    let r4 = (x,y,invalidWidth,invalidHeight) |> Rectangle.create

    Assert.False(r1.IsEmpty)
    Assert.True(r2.IsEmpty)
    Assert.True(r3.IsEmpty)
    Assert.True(r4.IsEmpty)

[<Fact>]
let ``Rectangle.isEmpty checks for a valid width and height in a rectangle`` () =
    let x = 2
    let y = 3
    let validWidth = 1
    let validHeight = 1
    let invalidWidth = 0
    let invalidHeight = 0

    let r1 = (x,y,validWidth,validHeight) |> Rectangle.create |> Some
    let r2 = (x,y,invalidWidth,validHeight) |> Rectangle.create |> Some
    let r3 = (x,y,validWidth,invalidHeight) |> Rectangle.create |> Some
    let r4 = (x,y,invalidWidth,invalidHeight) |> Rectangle.create |> Some

    Assert.True(None |> Rectangle.isEmpty)
    Assert.False(r1 |> Rectangle.isEmpty)
    Assert.True(r2 |> Rectangle.isEmpty)
    Assert.True(r3 |> Rectangle.isEmpty)
    Assert.True(r4 |> Rectangle.isEmpty)

[<Fact>]
let ``Rectangle.hasIntersection checks for a common area between two rectangles`` () =
    let x1 = 2
    let y1 = 3
    let w1 = 4
    let h1 = 5

    let x2 = 3
    let y2 = 4
    let w2 = 5
    let h2 = 6

    let x3 = 6
    let y3 = 7
    let w3 = 8
    let h3 = 9

    let r1 = (x1,y1,w1,h1) |> Rectangle.create
    let r2 = (x2,y2,w2,h2) |> Rectangle.create
    let r3 = (x3,y3,w3,h3) |> Rectangle.create

    Assert.True((r1,r1) ||> Rectangle.hasIntersection)
    Assert.True((r1,r2) ||> Rectangle.hasIntersection)
    Assert.False((r1,r3) ||> Rectangle.hasIntersection)
    Assert.True((r2,r3) ||> Rectangle.hasIntersection)

[<Fact>]
let ``Rectangle.intersect determines the intersecting area of two rectangles`` () =
    let x1 = 2
    let y1 = 3
    let w1 = 4
    let h1 = 5

    let x2 = 3
    let y2 = 4
    let w2 = 5
    let h2 = 6

    let x3 = 6
    let y3 = 7
    let w3 = 8
    let h3 = 9

    let r1 = (x1,y1,w1,h1) |> Rectangle.create
    let r2 = (x2,y2,w2,h2) |> Rectangle.create
    let r3 = (x3,y3,w3,h3) |> Rectangle.create

    let i1 = (r1,r1) ||> Rectangle.intersect
    let i2 = (r1,r2) ||> Rectangle.intersect
    let i3 = (r1,r3) ||> Rectangle.intersect
    let i4 = (r2,r3) ||> Rectangle.intersect

    Assert.False(i1.IsEmpty)
    Assert.False(i2.IsEmpty)
    Assert.True(i3.IsEmpty)
    Assert.False(i4.IsEmpty)

    Assert.Equal(x1,i1.X)
    Assert.Equal(y1,i1.Y)
    Assert.Equal(w1,i1.Width)
    Assert.Equal(h1,i1.Height)

    Assert.Equal(x2,i2.X)
    Assert.Equal(y2,i2.Y)
    Assert.Equal(3,i2.Width)
    Assert.Equal(4,i2.Height)

    Assert.Equal(x3,i3.X)
    Assert.Equal(y3,i3.Y)
    Assert.Equal(0,i3.Width)
    Assert.Equal(1,i3.Height)

    Assert.Equal(x3,i4.X)
    Assert.Equal(y3,i4.Y)
    Assert.Equal(2,i4.Width)
    Assert.Equal(3,i4.Height)

[<Fact>]
let ``Rectangle has binary minus that determines the intersecting area of two rectangles`` () =
    let x1 = 2
    let y1 = 3
    let w1 = 4
    let h1 = 5

    let x2 = 3
    let y2 = 4
    let w2 = 5
    let h2 = 6

    let x3 = 6
    let y3 = 7
    let w3 = 8
    let h3 = 9

    let r1 = (x1,y1,w1,h1) |> Rectangle.create
    let r2 = (x2,y2,w2,h2) |> Rectangle.create
    let r3 = (x3,y3,w3,h3) |> Rectangle.create

    let i1 = r1 - r1
    let i2 = r1 - r2
    let i3 = r1 - r3
    let i4 = r2 - r3

    Assert.False(i1.IsEmpty)
    Assert.False(i2.IsEmpty)
    Assert.True(i3.IsEmpty)
    Assert.False(i4.IsEmpty)

    Assert.Equal(x1,i1.X)
    Assert.Equal(y1,i1.Y)
    Assert.Equal(w1,i1.Width)
    Assert.Equal(h1,i1.Height)

    Assert.Equal(x2,i2.X)
    Assert.Equal(y2,i2.Y)
    Assert.Equal( 3,i2.Width)
    Assert.Equal( 4,i2.Height)

    Assert.Equal(x3,i3.X)
    Assert.Equal(y3,i3.Y)
    Assert.Equal( 0,i3.Width)
    Assert.Equal( 1,i3.Height)

    Assert.Equal(x3,i4.X)
    Assert.Equal(y3,i4.Y)
    Assert.Equal( 2,i4.Width)
    Assert.Equal( 3,i4.Height)
    
[<Fact>]
let ``Rectangle.union determines the union area of two rectangles`` () =
    let x1 = 2
    let y1 = 3
    let w1 = 4
    let h1 = 5

    let x2 = 3
    let y2 = 4
    let w2 = 5
    let h2 = 6

    let x3 = 6
    let y3 = 7
    let w3 = 8
    let h3 = 9

    let r1 = (x1,y1,w1,h1) |> Rectangle.create
    let r2 = (x2,y2,w2,h2) |> Rectangle.create
    let r3 = (x3,y3,w3,h3) |> Rectangle.create

    let u1 = (r1,r1) ||> Rectangle.union
    let u2 = (r1,r2) ||> Rectangle.union
    let u3 = (r1,r3) ||> Rectangle.union
    let u4 = (r2,r3) ||> Rectangle.union

    Assert.False(u1.IsEmpty)
    Assert.False(u2.IsEmpty)
    Assert.False(u3.IsEmpty)
    Assert.False(u4.IsEmpty)

    Assert.Equal(x1,u1.X)
    Assert.Equal(y1,u1.Y)
    Assert.Equal(w1,u1.Width)
    Assert.Equal(h1,u1.Height)

    Assert.Equal(x1,u2.X)
    Assert.Equal(y1,u2.Y)
    Assert.Equal( 6,u2.Width)
    Assert.Equal( 7,u2.Height)

    Assert.Equal(x1,u3.X)
    Assert.Equal(y1,u3.Y)
    Assert.Equal(12,u3.Width)
    Assert.Equal(13,u3.Height)

    Assert.Equal(x2,u4.X)
    Assert.Equal(y2,u4.Y)
    Assert.Equal(11,u4.Width)
    Assert.Equal(12,u4.Height)
    
[<Fact>]
let ``Rectangle has a binary plus that determines the union area of two rectangles`` () =
    let x1 = 2
    let y1 = 3
    let w1 = 4
    let h1 = 5

    let x2 = 3
    let y2 = 4
    let w2 = 5
    let h2 = 6

    let x3 = 6
    let y3 = 7
    let w3 = 8
    let h3 = 9

    let r1 = (x1,y1,w1,h1) |> Rectangle.create
    let r2 = (x2,y2,w2,h2) |> Rectangle.create
    let r3 = (x3,y3,w3,h3) |> Rectangle.create

    let u1 = r1 + r1
    let u2 = r1 + r2
    let u3 = r1 + r3
    let u4 = r2 + r3

    Assert.False(u1.IsEmpty)
    Assert.False(u2.IsEmpty)
    Assert.False(u3.IsEmpty)
    Assert.False(u4.IsEmpty)

    Assert.Equal(x1,u1.X)
    Assert.Equal(y1,u1.Y)
    Assert.Equal(w1,u1.Width)
    Assert.Equal(h1,u1.Height)

    Assert.Equal(x1,u2.X)
    Assert.Equal(y1,u2.Y)
    Assert.Equal( 6,u2.Width)
    Assert.Equal( 7,u2.Height)

    Assert.Equal(x1,u3.X)
    Assert.Equal(y1,u3.Y)
    Assert.Equal(12,u3.Width)
    Assert.Equal(13,u3.Height)

    Assert.Equal(x2,u4.X)
    Assert.Equal(y2,u4.Y)
    Assert.Equal(11,u4.Width)
    Assert.Equal(12,u4.Height)
