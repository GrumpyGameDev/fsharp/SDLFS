module Example2

open SDL

let rec eventPump (renderer1:Render.Renderer, renderer2: Render.Renderer, window2: Window.Window) : unit =
    renderer1
    |>* Render.setDrawColor (Pixel.Color.fromRGBA(0x00uy,0xFFuy,0x00uy,0xFFuy))

    renderer2
    |>* Render.setDrawColor (Pixel.Color.fromRGBA(0xFFuy,0x00uy,0x00uy,0xFFuy))

    renderer1
    |>* Render.clear

    renderer2
    |>* Render.clear

    renderer1
    |>* Render.present

    renderer2
    |>* Render.present

    match Event.wait None with
    | Some (Event.KeyDown kd) ->
        match kd.Keysym.Scancode with
        | ScanCode.Escape ->
            ()
        | ScanCode.Space ->
            if ((window2 |> Window.flags).Contains Window.Flags.Shown) then
                Window.hide window2
            else
                Window.show window2

            eventPump (renderer1, renderer2, window2)
        | _ ->
            eventPump (renderer1, renderer2, window2)
    | Some (Event.Quit _) ->
        ()
    | _ ->
        eventPump (renderer1, renderer2, window2)

let windowTitle1 = "Demo #2: main window. press space to show/hide other window"
let windowTitle2 = "Demo #2: other window"
let windowWidth = 640
let windowHeight = 480

let demo () =
    use system = new System([
                            Flags.Video
                            Flags.Events
                        ])  

    use window1 = Window.create (windowTitle1, Window.Position.Undefined, windowWidth, windowHeight, Window.Flags.None)

    use renderer1 = Render.create window1 None Render.Flags.Accelerated

    use window2 = Window.create (windowTitle2, Window.Position.Undefined, windowWidth, windowHeight, Window.Flags.None)

    use renderer2 = Render.create window2 None Render.Flags.Accelerated

    (renderer1, renderer2, window2)
    |> eventPump
