﻿
[<EntryPoint>]
let main argv =
    match argv.Length with
    | 0 ->
        printfn "Usage:"
        printfn "SDL2FS.WindowDemo n"
        printfn "Shows a particular demonstration (n) of the windows subsystem. Clicking the X on the windows or hitting the escape key closes the window."
        printfn "Values for n:"
        printfn "\t1 - initially centered window with a fixed width (basically the same as boilerplate)"
        printfn "\t2 - two windows. press space bar on main window to hide/show other window"
    | n ->
        match System.Int32.TryParse(argv.[0]) with
        | true, 1 ->
            Example1.demo()
        | true, 2 ->
            Example2.demo()
        | _  ->
            ()
    
    0
