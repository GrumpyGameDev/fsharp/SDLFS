module Example1

open SDL

let rec eventPump (renderer:Render.Renderer) : unit =
    renderer
    |>* Render.clear

    renderer
    |>* Render.present

    match Event.wait None with
    | Some (Event.KeyDown kd) ->
        if kd.Keysym.Scancode = ScanCode.Escape then
            ()
        else
            eventPump renderer
    | Some (Event.Quit _) ->
        ()
    | _ ->
        eventPump renderer

let windowTitle = "Demo #1: Initially centered window with a fixed width"
let windowWidth = 640
let windowHeight = 480

let demo () =
    use system = new System([
                            Flags.Video
                            Flags.Events
                        ])  

    use window = Window.create (windowTitle, Window.Position.Centered, windowWidth, windowHeight, Window.Flags.None)

    use renderer = Render.create window None Render.Flags.Accelerated

    renderer
    |> eventPump
