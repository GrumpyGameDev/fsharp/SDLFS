﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System
open SDL
open Microsoft.FSharp.NativeInterop

module Window = 

    type GLattr =
        | RedSize                 = 0
        | GreenSize               = 1
        | BlueSize                = 2
        | AlphaSize               = 3
        | BufferSize              = 4
        | DoubleBuffer            = 5
        | DepthSize               = 6
        | StencilSize             = 7
        | AccumRedSize            = 8
        | AccumGreenSize          = 9
        | AccumBlueSize           = 10
        | AccumAlphaSize          = 11
        | Stereo                  = 12
        | MultiSampleBuffers      = 13
        | MultiSampleSamples      = 14
        | AcceleratedVisual       = 15
        | RetainedBacking         = 16
        | ContextMajorVersion     = 17
        | ContextMinorVersion     = 18
        | ContextEgl              = 19
        | ContextFlags            = 20
        | ContextProfileMask      = 21
        | ShareWithCurrentContext = 22
        | FrameBufferSRGBCapable  = 23
        | ContextReleaseBehavior  = 24

    [<Flags>]
    type GLProfile = 
        | Core           = 0x0001
        | Compatibility  = 0x0002
        | ES             = 0x0004

    [<Flags>]
    type GLFlag = 
        | Debug             = 0x0001
        | ForwardCompatible = 0x0002
        | RobustAccess      = 0x0004
        | ResetIsolation    = 0x0008

    [<Flags>]
    type GLReleaseFlag = 
        | None   = 0x0000
        | Flush  = 0x0001

    [<RequireQualifiedAccess>]
    type Flags = 
        | None               = 0x00000000
        | FullScreen         = 0x00000001
        | OpenGL             = 0x00000002
        | Shown              = 0x00000004
        | Hidden             = 0x00000008
        | Borderless         = 0x00000010
        | Resizable          = 0x00000020
        | Minimized          = 0x00000040
        | Maximized          = 0x00000080
        | InputGrabbed       = 0x00000100
        | InputFocus         = 0x00000200
        | MouseFocus         = 0x00000400
        | FullScreenDesktop  = 0x00001001
        | Foreign            = 0x00000800
        | AllowHighDPI       = 0x00002000
        | MouseCapture       = 0x00004000

    type WindowEvent =
        | None        = 0
        | Shown       = 1
        | Hidden      = 2
        | Exposed     = 3
        | Moved       = 4
        | Resized     = 5
        | SizeChanged = 6
        | Minimized   = 7
        | Maximized   = 8
        | Restored    = 9
        | Enter       = 10
        | Leave       = 11
        | FocusGained = 12
        | FocusLost   = 13
        | Close       = 14

    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateWindow")>]
        extern IntPtr WinSdlCreateWindow(IntPtr title, int x, int y, int w, int h, uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateWindowFrom")>]
        extern IntPtr WinSdlCreateWindowFrom(IntPtr data)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyWindow")>]
        extern void WinSdlDestroyWindow(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateWindowAndRenderer")>]
        extern int WinSdlCreateWindowAndRenderer(int width, int height, uint32 window_flags, IntPtr* window, IntPtr* renderer)//DONE
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyRenderer")>]
        extern void WinSdlDestroyRenderer(IntPtr renderer)//DONE
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderer")>]
        extern IntPtr WinSdlGetRenderer(IntPtr  window)//DONE
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowDisplayIndex")>]
        extern int WinSdlGetWindowDisplayIndex(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowDisplayMode")>]
        extern int WinSdlSetWindowDisplayMode(IntPtr window, IntPtr mode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowDisplayMode")>]
        extern int WinSdlGetWindowDisplayMode(IntPtr window,IntPtr mode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowPixelFormat")>]
        extern uint32 WinSdlGetWindowPixelFormat(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowID")>]
        extern uint32 WinSdlGetWindowID(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowFlags")>]
        extern uint32 WinSdlGetWindowFlags(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowTitle")>]
        extern void WinSdlSetWindowTitle(IntPtr window, IntPtr title)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowTitle")>]
        extern IntPtr WinSdlGetWindowTitle(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowIcon")>]
        extern void WinSdlSetWindowIcon(IntPtr window,IntPtr icon)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowData")>]
        extern IntPtr WinSdlSetWindowData(IntPtr window, IntPtr name,IntPtr userdata)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowData")>]
        extern IntPtr WinSdlGetWindowData(IntPtr window, IntPtr name)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowPosition")>]
        extern void WinSdlSetWindowPosition(IntPtr window,int x, int y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowPosition")>]
        extern void WinSdlGetWindowPosition(IntPtr window,int* x, int* y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowSize")>]
        extern void WinSdlSetWindowSize(IntPtr window, int w,int h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowSize")>]
        extern void WinSdlGetWindowSize(IntPtr window, int* w,int* h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowMinimumSize")>]
        extern void WinSdlSetWindowMinimumSize(IntPtr window,int min_w, int min_h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowMinimumSize")>]
        extern void WinSdlGetWindowMinimumSize(IntPtr window,int* w, int* h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowMaximumSize")>]
        extern void WinSdlSetWindowMaximumSize(IntPtr window,int max_w, int max_h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowMaximumSize")>]
        extern void WinSdlGetWindowMaximumSize(IntPtr window,int* w, int* h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowBordered")>]
        extern void WinSdlSetWindowBordered(IntPtr window,int bordered)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ShowWindow")>]
        extern void WinSdlShowWindow(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HideWindow")>]
        extern void WinSdlHideWindow(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RaiseWindow")>]
        extern void WinSdlRaiseWindow(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MaximizeWindow")>]
        extern void WinSdlMaximizeWindow(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MinimizeWindow")>]
        extern void WinSdlMinimizeWindow(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RestoreWindow")>]
        extern void WinSdlRestoreWindow(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowFullscreen")>]
        extern int WinSdlSetWindowFullscreen(IntPtr window,uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowSurface")>]
        extern IntPtr WinSdlGetWindowSurface(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateWindowSurface")>]
        extern int WinSdlUpdateWindowSurface(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateWindowSurfaceRects")>]
        extern int WinSdlUpdateWindowSurfaceRects(IntPtr window, IntPtr rects,int numrects)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowGrab")>]
        extern void WinSdlSetWindowGrab(IntPtr window,int grabbed)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowGrab")>]
        extern int WinSdlGetWindowGrab(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetGrabbedWindow")>]
        extern IntPtr WinSdlGetGrabbedWindow()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowBrightness")>]
        extern int WinSdlSetWindowBrightness(IntPtr window, float brightness)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowBrightness")>]
        extern float WinSdlGetWindowBrightness(IntPtr window)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowGammaRamp")>]
        extern int WinSdlSetWindowGammaRamp(IntPtr window, IntPtr red, IntPtr green, IntPtr blue)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowGammaRamp")>]
        extern int WinSdlGetWindowGammaRamp(IntPtr window,IntPtr red,IntPtr green,IntPtr blue)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowHitTest")>]
        extern int WinSdlSetWindowHitTest(IntPtr window,IntPtr callback,IntPtr callback_data)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowFromID")>]
        extern IntPtr WinSdlGetWindowFromID(uint32 id)

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateWindow")>]
        extern IntPtr LinuxSdlCreateWindow(IntPtr title, int x, int y, int w, int h, uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateWindowFrom")>]
        extern IntPtr LinuxSdlCreateWindowFrom(IntPtr data)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyWindow")>]
        extern void LinuxSdlDestroyWindow(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateWindowAndRenderer")>]
        extern int LinuxSdlCreateWindowAndRenderer(int width, int height, uint32 window_flags, IntPtr* window, IntPtr* renderer)//DONE
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyRenderer")>]
        extern void LinuxSdlDestroyRenderer(IntPtr renderer)//DONE
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderer")>]
        extern IntPtr LinuxSdlGetRenderer(IntPtr  window)//DONE
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowDisplayIndex")>]
        extern int LinuxSdlGetWindowDisplayIndex(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowDisplayMode")>]
        extern int LinuxSdlSetWindowDisplayMode(IntPtr window, IntPtr mode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowDisplayMode")>]
        extern int LinuxSdlGetWindowDisplayMode(IntPtr window,IntPtr mode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowPixelFormat")>]
        extern uint32 LinuxSdlGetWindowPixelFormat(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowID")>]
        extern uint32 LinuxSdlGetWindowID(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowFlags")>]
        extern uint32 LinuxSdlGetWindowFlags(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowTitle")>]
        extern void LinuxSdlSetWindowTitle(IntPtr window, IntPtr title)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowTitle")>]
        extern IntPtr LinuxSdlGetWindowTitle(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowIcon")>]
        extern void LinuxSdlSetWindowIcon(IntPtr window,IntPtr icon)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowData")>]
        extern IntPtr LinuxSdlSetWindowData(IntPtr window, IntPtr name,IntPtr userdata)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowData")>]
        extern IntPtr LinuxSdlGetWindowData(IntPtr window, IntPtr name)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowPosition")>]
        extern void LinuxSdlSetWindowPosition(IntPtr window,int x, int y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowPosition")>]
        extern void LinuxSdlGetWindowPosition(IntPtr window,int* x, int* y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowSize")>]
        extern void LinuxSdlSetWindowSize(IntPtr window, int w,int h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowSize")>]
        extern void LinuxSdlGetWindowSize(IntPtr window, int* w,int* h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowMinimumSize")>]
        extern void LinuxSdlSetWindowMinimumSize(IntPtr window,int min_w, int min_h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowMinimumSize")>]
        extern void LinuxSdlGetWindowMinimumSize(IntPtr window,int* w, int* h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowMaximumSize")>]
        extern void LinuxSdlSetWindowMaximumSize(IntPtr window,int max_w, int max_h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowMaximumSize")>]
        extern void LinuxSdlGetWindowMaximumSize(IntPtr window,int* w, int* h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowBordered")>]
        extern void LinuxSdlSetWindowBordered(IntPtr window,int bordered)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ShowWindow")>]
        extern void LinuxSdlShowWindow(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HideWindow")>]
        extern void LinuxSdlHideWindow(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RaiseWindow")>]
        extern void LinuxSdlRaiseWindow(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MaximizeWindow")>]
        extern void LinuxSdlMaximizeWindow(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MinimizeWindow")>]
        extern void LinuxSdlMinimizeWindow(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RestoreWindow")>]
        extern void LinuxSdlRestoreWindow(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowFullscreen")>]
        extern int LinuxSdlSetWindowFullscreen(IntPtr window,uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowSurface")>]
        extern IntPtr LinuxSdlGetWindowSurface(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateWindowSurface")>]
        extern int LinuxSdlUpdateWindowSurface(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateWindowSurfaceRects")>]
        extern int LinuxSdlUpdateWindowSurfaceRects(IntPtr window, IntPtr rects,int numrects)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowGrab")>]
        extern void LinuxSdlSetWindowGrab(IntPtr window,int grabbed)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowGrab")>]
        extern int LinuxSdlGetWindowGrab(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetGrabbedWindow")>]
        extern IntPtr LinuxSdlGetGrabbedWindow()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowBrightness")>]
        extern int LinuxSdlSetWindowBrightness(IntPtr window, float brightness)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowBrightness")>]
        extern float LinuxSdlGetWindowBrightness(IntPtr window)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowGammaRamp")>]
        extern int LinuxSdlSetWindowGammaRamp(IntPtr window, IntPtr red, IntPtr green, IntPtr blue)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowGammaRamp")>]
        extern int LinuxSdlGetWindowGammaRamp(IntPtr window,IntPtr red,IntPtr green,IntPtr blue)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetWindowHitTest")>]
        extern int LinuxSdlSetWindowHitTest(IntPtr window,IntPtr callback,IntPtr callback_data)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetWindowFromID")>]
        extern IntPtr LinuxSdlGetWindowFromID(uint32 id)

        let SdlCreateWindow = (WinSdlCreateWindow, LinuxSdlCreateWindow) |> toNative
        let SdlCreateWindowFrom = (WinSdlCreateWindowFrom, LinuxSdlCreateWindowFrom) |> toNative
        let SdlDestroyWindow = (WinSdlDestroyWindow, LinuxSdlDestroyWindow) |> toNative
        let SdlCreateWindowAndRenderer = (WinSdlCreateWindowAndRenderer, LinuxSdlCreateWindowAndRenderer) |> toNative
        let SdlDestroyRenderer = (WinSdlDestroyRenderer, LinuxSdlDestroyRenderer) |> toNative
        let SdlGetRenderer = (WinSdlGetRenderer, LinuxSdlGetRenderer) |> toNative
        let SdlGetWindowDisplayIndex = (WinSdlGetWindowDisplayIndex, LinuxSdlGetWindowDisplayIndex) |> toNative
        let SdlSetWindowDisplayMode = (WinSdlSetWindowDisplayMode, LinuxSdlSetWindowDisplayMode) |> toNative
        let SdlGetWindowDisplayMode = (WinSdlGetWindowDisplayMode, LinuxSdlGetWindowDisplayMode) |> toNative
        let SdlGetWindowPixelFormat = (WinSdlGetWindowPixelFormat, LinuxSdlGetWindowPixelFormat) |> toNative
        let SdlGetWindowID = (WinSdlGetWindowID, LinuxSdlGetWindowID) |> toNative
        let SdlGetWindowFlags = (WinSdlGetWindowFlags, LinuxSdlGetWindowFlags) |> toNative
        let SdlSetWindowTitle = (WinSdlSetWindowTitle, LinuxSdlSetWindowTitle) |> toNative
        let SdlGetWindowTitle = (WinSdlGetWindowTitle, LinuxSdlGetWindowTitle) |> toNative
        let SdlSetWindowIcon = (WinSdlSetWindowIcon, LinuxSdlSetWindowIcon) |> toNative
        let SdlSetWindowData = (WinSdlSetWindowData, LinuxSdlSetWindowData) |> toNative
        let SdlGetWindowData = (WinSdlGetWindowData, LinuxSdlGetWindowData) |> toNative
        let SdlSetWindowPosition = (WinSdlSetWindowPosition, LinuxSdlSetWindowPosition) |> toNative
        let SdlGetWindowPosition = (WinSdlGetWindowPosition, LinuxSdlGetWindowPosition) |> toNative
        let SdlSetWindowSize = (WinSdlSetWindowSize, LinuxSdlSetWindowSize) |> toNative
        let SdlGetWindowSize = (WinSdlGetWindowSize, LinuxSdlGetWindowSize) |> toNative
        let SdlSetWindowMinimumSize = (WinSdlSetWindowMinimumSize, LinuxSdlSetWindowMinimumSize) |> toNative
        let SdlGetWindowMinimumSize = (WinSdlGetWindowMinimumSize, LinuxSdlGetWindowMinimumSize) |> toNative
        let SdlSetWindowMaximumSize = (WinSdlSetWindowMaximumSize, LinuxSdlSetWindowMaximumSize) |> toNative
        let SdlGetWindowMaximumSize = (WinSdlGetWindowMaximumSize, LinuxSdlGetWindowMaximumSize) |> toNative
        let SdlSetWindowBordered = (WinSdlSetWindowBordered, LinuxSdlSetWindowBordered) |> toNative
        let SdlShowWindow = (WinSdlShowWindow, LinuxSdlShowWindow) |> toNative
        let SdlHideWindow = (WinSdlHideWindow, LinuxSdlHideWindow) |> toNative
        let SdlRaiseWindow = (WinSdlRaiseWindow, LinuxSdlRaiseWindow) |> toNative
        let SdlMaximizeWindow = (WinSdlMaximizeWindow, LinuxSdlMaximizeWindow) |> toNative
        let SdlMinimizeWindow = (WinSdlMinimizeWindow, LinuxSdlMinimizeWindow) |> toNative
        let SdlRestoreWindow = (WinSdlRestoreWindow, LinuxSdlRestoreWindow) |> toNative
        let SdlSetWindowFullscreen = (WinSdlSetWindowFullscreen, LinuxSdlSetWindowFullscreen) |> toNative
        let SdlGetWindowSurface = (WinSdlGetWindowSurface, LinuxSdlGetWindowSurface) |> toNative
        let SdlUpdateWindowSurface = (WinSdlUpdateWindowSurface, LinuxSdlUpdateWindowSurface) |> toNative
        let SdlUpdateWindowSurfaceRects = (WinSdlUpdateWindowSurfaceRects, LinuxSdlUpdateWindowSurfaceRects) |> toNative
        let SdlSetWindowGrab = (WinSdlSetWindowGrab, LinuxSdlSetWindowGrab) |> toNative
        let SdlGetWindowGrab = (WinSdlGetWindowGrab, LinuxSdlGetWindowGrab) |> toNative
        let SdlGetGrabbedWindow = (WinSdlGetGrabbedWindow, LinuxSdlGetGrabbedWindow) |> toNative
        let SdlSetWindowBrightness = (WinSdlSetWindowBrightness, LinuxSdlSetWindowBrightness) |> toNative
        let SdlGetWindowBrightness = (WinSdlGetWindowBrightness, LinuxSdlGetWindowBrightness) |> toNative
        let SdlSetWindowGammaRamp = (WinSdlSetWindowGammaRamp, LinuxSdlSetWindowGammaRamp) |> toNative
        let SdlGetWindowGammaRamp = (WinSdlGetWindowGammaRamp, LinuxSdlGetWindowGammaRamp) |> toNative
        let SdlSetWindowHitTest = (WinSdlSetWindowHitTest, LinuxSdlSetWindowHitTest) |> toNative
        let SdlGetWindowFromID = (WinSdlGetWindowFromID, LinuxSdlGetWindowFromID) |> toNative

    type Window(ptr:IntPtr, destroyFunc: IntPtr->unit) = 
        inherit Pointer(ptr, destroyFunc)
        new(ptr:IntPtr) = new Window(ptr, Native.SdlDestroyWindow)
        static member Invalid = new Window(IntPtr.Zero)

    [<RequireQualifiedAccess>]
    type Position =
        | Undefined
        | Centered
        | Absolute of int * int

    //there is no value whatsoever with this function being in curried form
    let create (title:string, position:Position, width:int, height:int, flags:Flags) :Result<Window,string> =
        let windowX, windowY = 
            match position with
            | Position.Undefined -> (0x1FFF0000,0x1FFF0000)
            | Position.Centered -> (0x2FFF0000,0x2FFF0000)
            | Position.Absolute (x,y) -> (x, y)
        match title |> withUtf8String (fun ptr -> Native.SdlCreateWindow(ptr, windowX, windowY, width, height, flags |> uint32)) with
        | x when x = IntPtr.Zero ->
            Error (Error.get())
        | ptr ->
           Ok (new Window(ptr))

    let createFrom (data:IntPtr) : Result<Window, string> =
        match Native.SdlCreateWindowFrom(data) with
        | x when x = IntPtr.Zero ->
            Error (Error.get())
        | ptr ->
            Ok (new Window(ptr))

    let hide (window:Window) :unit =
        Native.SdlHideWindow window.Pointer

    let show (window:Window) :unit =
        Native.SdlShowWindow window.Pointer

    let minimize (window:Window) :unit =
        Native.SdlMinimizeWindow window.Pointer

    let maximize (window:Window) :unit =
        Native.SdlMaximizeWindow window.Pointer

    let raise (window:Window) :unit =
        Native.SdlRaiseWindow window.Pointer

    let restore (window:Window) :unit =
        Native.SdlRestoreWindow window.Pointer

    let setTitle (text:string) (window:Window) : unit =
        text
        |> withUtf8String (fun ptr -> Native.SdlSetWindowTitle(window.Pointer, ptr))

    let getTitle (window:Window) :string =
        Native.SdlGetWindowTitle window.Pointer
        |> intPtrToStringUtf8

    let setSize (w:int,h:int) (window:Window) : unit =
        Native.SdlSetWindowSize(window.Pointer, w, h)

    let getSize (window:Window) :int * int =
        let mutable x = 0
        let mutable y = 0
        let px = &&x
        let py = &&y
        Native.SdlGetWindowSize (window.Pointer, px, py)
        (x, y)

    let setPosition (x:int,y:int) (window:Window) :unit =
        Native.SdlSetWindowPosition(window.Pointer, x, y)

    let getPosition (window:Window) :int * int =
        let mutable x = 0
        let mutable y = 0
        let px = &&x
        let py = &&y
        Native.SdlGetWindowPosition (window.Pointer, px, py)
        (x, y)

    let setBrightness (brightness:float) (window:Window) : Result<unit, string> =
        match Native.SdlSetWindowBrightness(window.Pointer, brightness) with
        | 0 -> Ok ()
        | _ -> Error.get() |> Error

    let getBrightness (window:Window) :float =
        Native.SdlGetWindowBrightness(window.Pointer)

    let setBordered (bordered:bool) (window:Window) :unit =
        Native.SdlSetWindowBordered(window.Pointer, if bordered then 1 else 0)

    let setMaximumSize (w:int,h:int) (window:Window) : unit =
        Native.SdlSetWindowMaximumSize(window.Pointer, w, h)

    let getMaximumSize (window:Window) :int * int =
        let mutable x = 0
        let mutable y = 0
        let px = &&x
        let py = &&y
        Native.SdlGetWindowMaximumSize (window.Pointer, px, py)
        (x, y)

    let setMinimumSize (w:int,h:int) (window:Window) :unit =
        Native.SdlSetWindowMinimumSize(window.Pointer, w, h)

    let getMinimumSize (window:Window) :int * int =
        let mutable x = 0
        let mutable y = 0
        let px = &&x
        let py = &&y
        Native.SdlGetWindowMinimumSize (window.Pointer, px, py)
        (x, y)

    let getGrab (window:Window) :bool =
        Native.SdlGetWindowGrab(window.Pointer)
        <> 0

    let setGrab (grab:bool) (window:Window) =
        Native.SdlSetWindowGrab(window.Pointer,if grab then 1 else 0)

    let getGrabbedWindow () : Window =
        new Window(Native.SdlGetGrabbedWindow())

    let setData (name:string) (data:IntPtr) (window:Window) : IntPtr =
        name
        |> withAsciiString(fun ptr->Native.SdlSetWindowData(window.Pointer,ptr,data))

    let getData (name:string) (window:Window) :IntPtr =
        name
        |> withAsciiString(fun ptr->Native.SdlGetWindowData(window.Pointer,ptr))

    let getPixelFormat (window:Window) :uint32 =
        Native.SdlGetWindowPixelFormat(window.Pointer)

    let getId (window:Window) :uint32 =
        Native.SdlGetWindowID(window.Pointer)

    let flags (window:Window) :Set<Flags> =
        let raw = 
            Native.SdlGetWindowFlags(window.Pointer)
        [
            Flags.FullScreen       
            Flags.OpenGL           
            Flags.Shown            
            Flags.Hidden           
            Flags.Borderless       
            Flags.Resizable        
            Flags.Minimized        
            Flags.Maximized        
            Flags.InputGrabbed     
            Flags.InputFocus       
            Flags.MouseFocus       
            Flags.FullScreenDesktop
            Flags.Foreign          
            Flags.AllowHighDPI     
            Flags.MouseCapture     
        ]
        |> List.fold 
            (fun a i -> 
                if (raw &&& (uint32)i) > 0u then
                    a
                    |> Set.add i
                else
                    a) Set.empty
        

    let updateSurface (window:Window) :Result<unit,string> =
        match Native.SdlUpdateWindowSurface(window.Pointer) with
        | 0 -> Ok ()
        | _ -> Error.get() |> Error


    let getSurface (window:Window) :Result<Pointer,string> =
        match Native.SdlGetWindowSurface(window.Pointer) with
        | x when x = IntPtr.Zero -> Error.get() |> Error
        | ptr ->  new Pointer(ptr, ignore) |> Ok

    let fromId (id:uint32) :Window =
        new Window(Native.SdlGetWindowFromID(id), ignore)

    let getDisplayIndex (window:Window) :int =
        Native.SdlGetWindowDisplayIndex(window.Pointer)

    let getDisplayMode (window:Window) : Video.DisplayMode =
        let mutable mode = SDL.Video.SdlDisplayMode()

        Native.SdlGetWindowDisplayMode(window.Pointer, ((&&mode) |> NativePtr.toNativeInt ))
        |> ignore

        mode
        |> Video.sdlDisplayModeToDisplayMode

    let setDisplayMode (window:Window) (mode:Video.DisplayMode) :unit =
        let mutable mode' = mode |> Video.displayModeToSdlDisplayMode
        let ptr = 
            &&mode'
            |> NativePtr.toNativeInt

        Native.SdlSetWindowDisplayMode(window.Pointer, ptr)
        |> ignore

    let setFullscreen (flags:uint32) (window:Window) =
        Native.SdlSetWindowFullscreen(window.Pointer,flags)
        |> ignore

    let setWindowIcon (icon:Pointer) (window:Window) =
        Native.SdlSetWindowIcon(window.Pointer,icon.Pointer)

    let createWindowAndRenderer (width:int, height:int, flags:Flags) : (Window * Render.Renderer) option =
        let mutable window:IntPtr = IntPtr.Zero
        let mutable renderer:IntPtr = IntPtr.Zero
        let pwindow = &&window
        let prenderer = &&renderer
        let result = 
            Native.SdlCreateWindowAndRenderer(width, height, flags |> uint32, pwindow, prenderer)
        if result=0 then
            Some (new Window(window), new Render.Renderer(renderer))
        else
            if window<>IntPtr.Zero then
                Native.SdlDestroyWindow(window)

            if renderer<> IntPtr.Zero then
                Native.SdlDestroyRenderer(renderer)

            None

    let getRenderer (window:Window) : Render.Renderer option =
        let ptr =
            Native.SdlGetRenderer(window.Pointer)
        if ptr=IntPtr.Zero then
            None
        else
            Some (new Render.Renderer(ptr, ignore))