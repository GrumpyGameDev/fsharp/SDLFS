﻿namespace SDL

open System
open System.Runtime.InteropServices
open SDL
open Microsoft.FSharp.NativeInterop
open SDL.Pixel

#nowarn "9" "51"

module Surface = 
    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlSurface =
        struct
            val flags :    uint32
            val format:    IntPtr//SDL_PixelFormat*
            val w:         int
            val h:         int
            val pitch:     int
            val pixels:    IntPtr
            val userdate:  IntPtr
            val locked:    int
            val lock_data: IntPtr
            val clip_rect: SdlRect
            val map:       IntPtr
            val refcount:  int
        end

    module private Native =
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateRGBSurface")>]
        extern IntPtr WinSdlCreateRGBSurface(uint32 flags, int width, int height, int depth, uint32 Rmask, uint32 Gmask, uint32 Bmask, uint32 Amask)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateRGBSurfaceFrom")>]
        extern IntPtr WinSdlCreateRGBSurfaceFrom(IntPtr pixels, int width, int height, int depth, int pitch, uint32 Rmask, uint32 Gmask, uint32 Bmask, uint32 Amask)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreeSurface")>]
        extern void WinSdlFreeSurface(IntPtr surface)    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfacePalette")>]
        extern  int WinSdlSetSurfacePalette(IntPtr surface, IntPtr palette)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LockSurface")>]
        extern  int WinSdlLockSurface(IntPtr surface)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UnlockSurface")>]
        extern  void WinSdlUnlockSurface(IntPtr surface)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LoadBMP_RW")>]
        extern IntPtr WinSdlLoadBmpRw(IntPtr src, int freesrc)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SaveBMP_RW")>]
        extern int WinSdlSaveBmpRw(IntPtr surface, IntPtr dst, int freedst)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceRLE")>]
        extern int WinSdlSetSurfaceRLE(IntPtr surface, int flag)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetColorKey")>]
        extern int WinSdlSetColorKey(IntPtr surface, int flag, uint32 key)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetColorKey")>]
        extern int WinSdlGetColorKey(IntPtr surface, uint32 * key)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceColorMod")>]
        extern int WinSdlSetSurfaceColorMod(IntPtr surface, uint8 r, uint8 g, uint8 b)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSurfaceColorMod")>]
        extern int WinSdlGetSurfaceColorMod(IntPtr surface, uint8 * r, uint8 * g, uint8 * b)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceAlphaMod")>]
        extern int WinSdlSetSurfaceAlphaMod(IntPtr surface, uint8 alpha)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSurfaceAlphaMod")>]
        extern int WinSdlGetSurfaceAlphaMod(IntPtr surface, uint8 * alpha)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceBlendMode")>]
        extern int WinSdlSetSurfaceBlendMode(IntPtr surface, int blendMode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSurfaceBlendMode")>]
        extern int WinSdlGetSurfaceBlendMode(IntPtr surface, int* blendMode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetClipRect")>]
        extern int  WinSdlSetClipRect(IntPtr surface, SDL.Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetClipRect")>]
        extern void  WinSdlGetClipRect(IntPtr surface, SDL.Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ConvertSurface")>]
        extern IntPtr WinSdlConvertSurface(IntPtr src, IntPtr fmt, uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ConvertSurfaceFormat")>]
        extern IntPtr WinSdlConvertSurfaceFormat(IntPtr src, uint32 pixel_format, uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ConvertPixels")>]
        extern int WinSdlConvertPixels(int width, int height, uint32 src_format, IntPtr src, int src_pitch, uint32 dst_format, IntPtr dst, int dst_pitch)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FillRect")>]
        extern int WinSdlFillRect(IntPtr dst, IntPtr rect, uint32 color)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FillRects")>]
        extern int WinSdlFillRects(IntPtr dst, IntPtr rects, int count, uint32 color)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpperBlit")>]
        extern int WinSdlUpperBlit(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LowerBlit")>]
        extern int WinSdlLowerBlit(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SoftStretch")>]
        extern int WinSdlSoftStretch(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpperBlitScaled")>]
        extern int WinSdlUpperBlitScaled(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LowerBlitScaled")>]
        extern int WinSdlLowerBlitScaled(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateRGBSurface")>]
        extern IntPtr LinuxSdlCreateRGBSurface(uint32 flags, int width, int height, int depth, uint32 Rmask, uint32 Gmask, uint32 Bmask, uint32 Amask)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateRGBSurfaceFrom")>]
        extern IntPtr LinuxSdlCreateRGBSurfaceFrom(IntPtr pixels, int width, int height, int depth, int pitch, uint32 Rmask, uint32 Gmask, uint32 Bmask, uint32 Amask)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreeSurface")>]
        extern void LinuxSdlFreeSurface(IntPtr surface)    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfacePalette")>]
        extern  int LinuxSdlSetSurfacePalette(IntPtr surface, IntPtr palette)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LockSurface")>]
        extern  int LinuxSdlLockSurface(IntPtr surface)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UnlockSurface")>]
        extern  void LinuxSdlUnlockSurface(IntPtr surface)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LoadBMP_RW")>]
        extern IntPtr LinuxSdlLoadBmpRw(IntPtr src, int freesrc)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SaveBMP_RW")>]
        extern int LinuxSdlSaveBmpRw(IntPtr surface, IntPtr dst, int freedst)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceRLE")>]
        extern int LinuxSdlSetSurfaceRLE(IntPtr surface, int flag)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetColorKey")>]
        extern int LinuxSdlSetColorKey(IntPtr surface, int flag, uint32 key)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetColorKey")>]
        extern int LinuxSdlGetColorKey(IntPtr surface, uint32 * key)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceColorMod")>]
        extern int LinuxSdlSetSurfaceColorMod(IntPtr surface, uint8 r, uint8 g, uint8 b)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSurfaceColorMod")>]
        extern int LinuxSdlGetSurfaceColorMod(IntPtr surface, uint8 * r, uint8 * g, uint8 * b)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceAlphaMod")>]
        extern int LinuxSdlSetSurfaceAlphaMod(IntPtr surface, uint8 alpha)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSurfaceAlphaMod")>]
        extern int LinuxSdlGetSurfaceAlphaMod(IntPtr surface, uint8 * alpha)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetSurfaceBlendMode")>]
        extern int LinuxSdlSetSurfaceBlendMode(IntPtr surface, int blendMode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSurfaceBlendMode")>]
        extern int LinuxSdlGetSurfaceBlendMode(IntPtr surface, int* blendMode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetClipRect")>]
        extern int  LinuxSdlSetClipRect(IntPtr surface, SDL.Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetClipRect")>]
        extern void  LinuxSdlGetClipRect(IntPtr surface, SDL.Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ConvertSurface")>]
        extern IntPtr LinuxSdlConvertSurface(IntPtr src, IntPtr fmt, uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ConvertSurfaceFormat")>]
        extern IntPtr LinuxSdlConvertSurfaceFormat(IntPtr src, uint32 pixel_format, uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ConvertPixels")>]
        extern int LinuxSdlConvertPixels(int width, int height, uint32 src_format, IntPtr src, int src_pitch, uint32 dst_format, IntPtr dst, int dst_pitch)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FillRect")>]
        extern int LinuxSdlFillRect(IntPtr dst, IntPtr rect, uint32 color)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FillRects")>]
        extern int LinuxSdlFillRects(IntPtr dst, IntPtr rects, int count, uint32 color)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpperBlit")>]
        extern int LinuxSdlUpperBlit(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LowerBlit")>]
        extern int LinuxSdlLowerBlit(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SoftStretch")>]
        extern int LinuxSdlSoftStretch(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpperBlitScaled")>]
        extern int LinuxSdlUpperBlitScaled(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LowerBlitScaled")>]
        extern int LinuxSdlLowerBlitScaled(IntPtr src, IntPtr srcrect, IntPtr dst, IntPtr dstrect)
    
        let SdlCreateRGBSurface    = (WinSdlCreateRGBSurface    , LinuxSdlCreateRGBSurface    ) |> toNative
        let SdlCreateRGBSurfaceFrom= (WinSdlCreateRGBSurfaceFrom, LinuxSdlCreateRGBSurfaceFrom) |> toNative
        let SdlFreeSurface         = (WinSdlFreeSurface         , LinuxSdlFreeSurface         ) |> toNative
        let SdlSetSurfacePalette   = (WinSdlSetSurfacePalette   , LinuxSdlSetSurfacePalette   ) |> toNative
        let SdlLockSurface         = (WinSdlLockSurface         , LinuxSdlLockSurface         ) |> toNative
        let SdlUnlockSurface       = (WinSdlUnlockSurface       , LinuxSdlUnlockSurface       ) |> toNative
        let SdlLoadBmpRw           = (WinSdlLoadBmpRw           , LinuxSdlLoadBmpRw           ) |> toNative
        let SdlSaveBmpRw           = (WinSdlSaveBmpRw           , LinuxSdlSaveBmpRw           ) |> toNative
        let SdlSetSurfaceRLE       = (WinSdlSetSurfaceRLE       , LinuxSdlSetSurfaceRLE       ) |> toNative
        let SdlSetColorKey         = (WinSdlSetColorKey         , LinuxSdlSetColorKey         ) |> toNative
        let SdlGetColorKey         = (WinSdlGetColorKey         , LinuxSdlGetColorKey         ) |> toNative
        let SdlSetSurfaceColorMod  = (WinSdlSetSurfaceColorMod  , LinuxSdlSetSurfaceColorMod  ) |> toNative
        let SdlGetSurfaceColorMod  = (WinSdlGetSurfaceColorMod  , LinuxSdlGetSurfaceColorMod  ) |> toNative
        let SdlSetSurfaceAlphaMod  = (WinSdlSetSurfaceAlphaMod  , LinuxSdlSetSurfaceAlphaMod  ) |> toNative
        let SdlGetSurfaceAlphaMod  = (WinSdlGetSurfaceAlphaMod  , LinuxSdlGetSurfaceAlphaMod  ) |> toNative
        let SdlSetSurfaceBlendMode = (WinSdlSetSurfaceBlendMode , LinuxSdlSetSurfaceBlendMode ) |> toNative
        let SdlGetSurfaceBlendMode = (WinSdlGetSurfaceBlendMode , LinuxSdlGetSurfaceBlendMode ) |> toNative
        let SdlSetClipRect         = (WinSdlSetClipRect         , LinuxSdlSetClipRect         ) |> toNative
        let SdlGetClipRect         = (WinSdlGetClipRect         , LinuxSdlGetClipRect         ) |> toNative
        let SdlConvertSurface      = (WinSdlConvertSurface      , LinuxSdlConvertSurface      ) |> toNative
        let SdlConvertSurfaceFormat= (WinSdlConvertSurfaceFormat, LinuxSdlConvertSurfaceFormat) |> toNative
        let SdlConvertPixels       = (WinSdlConvertPixels       , LinuxSdlConvertPixels       ) |> toNative
        let SdlFillRect            = (WinSdlFillRect            , LinuxSdlFillRect            ) |> toNative
        let SdlFillRects           = (WinSdlFillRects           , LinuxSdlFillRects           ) |> toNative
        let SdlUpperBlit           = (WinSdlUpperBlit           , LinuxSdlUpperBlit           ) |> toNative
        let SdlLowerBlit           = (WinSdlLowerBlit           , LinuxSdlLowerBlit           ) |> toNative
        let SdlSoftStretch         = (WinSdlSoftStretch         , LinuxSdlSoftStretch         ) |> toNative
        let SdlUpperBlitScaled     = (WinSdlUpperBlitScaled     , LinuxSdlUpperBlitScaled     ) |> toNative
        let SdlLowerBlitScaled     = (WinSdlLowerBlitScaled     , LinuxSdlLowerBlitScaled     ) |> toNative

    type Surface(ptr:IntPtr) = 
        inherit SDL.Utility.Pointer(ptr, Native.SdlFreeSurface)
        static member Invalid = new Surface(IntPtr.Zero)


    let createRGB (width:int,height:int,depth:int) (rmask:uint32,gmask:uint32,bmask:uint32,amask:uint32) :Surface=
        let ptr = Native.SdlCreateRGBSurface(0u,width,height,depth,rmask,gmask,bmask,amask)
        new Surface(ptr)

    let private getFormat (surface:Surface) :IntPtr =
        let sdlSurface = 
            surface.Pointer
            |> NativePtr.ofNativeInt<SdlSurface>
            |> NativePtr.read
        sdlSurface.format

    let fillRect (rect:SDL.Geometry.Rectangle option) (color:SDL.Pixel.Color) (surface:Surface) :bool =
        let format = surface |> getFormat
        SDL.Geometry.withSdlRectPointer (fun r->0 = Native.SdlFillRect(surface.Pointer, r, color |> SDL.Pixel.mapColor format)) rect

    let loadBmp (pixelFormat: uint32) (fileName:string) : Surface =
        let bitmapSurface = Native.SdlLoadBmpRw(SDL.Utility.withUtf8String (fun ptr->SDL.RWops.Native.SdlRWFromFile(ptr,"rb")) fileName, 1)
        let convertedSurface = Native.SdlConvertSurfaceFormat(bitmapSurface,pixelFormat,0u)
        Native.SdlFreeSurface bitmapSurface
        new Surface(convertedSurface)

    let saveBmp (fileName:string) (surface:Surface) :bool =
        0 = Native.SdlSaveBmpRw(surface.Pointer, SDL.Utility.withUtf8String (fun ptr->SDL.RWops.Native.SdlRWFromFile(ptr,"wb")) fileName, 1)

    let upperBlit (srcrect:SDL.Geometry.Rectangle option) (src:Surface) (dstrect:SDL.Geometry.Rectangle option) (dst:Surface) =
        SDL.Geometry.withSdlRectPointer (fun srcptr -> SDL.Geometry.withSdlRectPointer (fun dstptr -> 0 = Native.SdlUpperBlit(src.Pointer,srcptr,dst.Pointer,dstptr)) dstrect) srcrect

    let blit = upperBlit

    let lowerBlit (srcrect:SDL.Geometry.Rectangle option) (src:Surface) (dstrect:SDL.Geometry.Rectangle option) (dst:Surface) =
        SDL.Geometry.withSdlRectPointer (fun srcptr -> SDL.Geometry.withSdlRectPointer (fun dstptr -> 0 = Native.SdlLowerBlit(src.Pointer,srcptr,dst.Pointer,dstptr)) dstrect) srcrect

    let upperBlitScaled (srcrect:SDL.Geometry.Rectangle option) (src:Surface) (dstrect:SDL.Geometry.Rectangle option) (dst:Surface) =
        SDL.Geometry.withSdlRectPointer (fun srcptr -> SDL.Geometry.withSdlRectPointer (fun dstptr -> 0 = Native.SdlUpperBlitScaled(src.Pointer,srcptr,dst.Pointer,dstptr)) dstrect) srcrect

    let lowerBlitScaled (srcrect:SDL.Geometry.Rectangle option) (src:Surface) (dstrect:SDL.Geometry.Rectangle option) (dst:Surface) =
        SDL.Geometry.withSdlRectPointer (fun srcptr -> SDL.Geometry.withSdlRectPointer (fun dstptr -> 0 = Native.SdlLowerBlitScaled(src.Pointer,srcptr,dst.Pointer,dstptr)) dstrect) srcrect

    let softStretch (srcrect:SDL.Geometry.Rectangle option) (src:Surface) (dstrect:SDL.Geometry.Rectangle option) (dst:Surface) =
        SDL.Geometry.withSdlRectPointer (fun srcptr -> SDL.Geometry.withSdlRectPointer (fun dstptr -> 0 = Native.SdlSoftStretch(src.Pointer,srcptr,dst.Pointer,dstptr)) dstrect) srcrect

    let setColorKey (color:SDL.Pixel.Color option) (surface:Surface) =
        let fmt = 
            (surface |> getFormat)
        let key = 
            if color.IsSome then SDL.Pixel.mapColor fmt color.Value else 0u
        let flag = 
            if color.IsSome then 1 else 0
        0 = Native.SdlSetColorKey(surface.Pointer, flag, key)

    let getColorKey (surface:Surface) :SDL.Pixel.Color option =
        let mutable key: uint32 = 0u
        let pkey = &&key
        match Native.SdlGetColorKey(surface.Pointer,pkey) with
        | 0 ->
            let fmt = 
                (surface |> getFormat)
            key |> SDL.Pixel.getColor fmt |> Some
        | _ -> None

    let lockBind (surface:Surface) (func: unit -> unit) :bool =
        if 0 = Native.SdlLockSurface surface.Pointer then
            func()
            Native.SdlUnlockSurface surface.Pointer
            true
        else
            false

    let setRLE (surface:Surface) (flag:bool) :bool =
        0 = Native.SdlSetSurfaceRLE(surface.Pointer,(if flag then 1 else 0))

    let setModulation (color:SDL.Pixel.Color) (surface:Surface) :bool = 
        (0 = Native.SdlSetSurfaceColorMod(surface.Pointer, color.Red, color.Green, color.Blue)) && (0 = Native.SdlSetSurfaceAlphaMod(surface.Pointer, color.Alpha))
    
    let getModulation (surface:Surface) :SDL.Pixel.Color option =
        let mutable r : uint8 = 0uy
        let mutable g : uint8 = 0uy
        let mutable b : uint8 = 0uy
        let mutable a : uint8 = 0uy
        let pr = &&r
        let pg = &&g
        let pb = &&b
        let pa = &&a
        let result = Native.SdlGetSurfaceColorMod(surface.Pointer,pr,pg,pb), Native.SdlGetSurfaceAlphaMod(surface.Pointer,pa)
        match result with
        | (0,0) -> {Red=r;Green=g;Blue=b;Alpha=a} |> Some
        | _ -> None
