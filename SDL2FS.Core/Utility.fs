﻿namespace SDL

#nowarn "9"

open System.Text
open System.Runtime.InteropServices
open System
open Microsoft.FSharp.NativeInterop

[<AutoOpen>]
module Operators = 
    let (|>*) f g =
        f |> g |> ignore

[<AutoOpen>]
module Utility = 

    module internal Native =
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_malloc")>]
        extern IntPtr WinSdlMalloc(uint32 size)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_calloc")>]
        extern IntPtr WinSdlCalloc(uint32 nmemb, uint32 size)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_free")>]
        extern void WinSdlFree(IntPtr mem);

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_malloc")>]
        extern IntPtr LinuxSdlMalloc(uint32 size)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_calloc")>]
        extern IntPtr LinuxSdlCalloc(uint32 nmemb, uint32 size)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_free")>]
        extern void LinuxSdlFree(IntPtr mem);

        let SdlMalloc = (WinSdlMalloc , LinuxSdlMalloc) |> toNative
        let SdlCalloc = (WinSdlCalloc , LinuxSdlCalloc) |> toNative
        let SdlFree   = (WinSdlFree   , LinuxSdlFree  ) |> toNative

    let private allocString (encoder:string->byte[]) (text:string) =
        let bytes = encoder(text)
        let pinnedArray = GCHandle.Alloc(bytes, GCHandleType.Pinned)
        pinnedArray

    let private withString (encoder:string->byte[]) (func:IntPtr->'T) (text:string) =
        let pinnedArray =
            (encoder,text)
            ||> allocString
        let result = pinnedArray.AddrOfPinnedObject() |> func
        pinnedArray.Free()
        result

    let internal allocUtf8String (text:string) =
        allocString Encoding.UTF8.GetBytes text

    let internal withUtf8String (func:IntPtr->'T) (text:string) =
        withString Encoding.UTF8.GetBytes func text

    let internal withAsciiString (func: IntPtr->'T) (text:string) =
        withString Encoding.ASCII.GetBytes func text


    let private intPtrToString (encoder:byte[]->string) (ptr:IntPtr):string = 
        if ptr = IntPtr.Zero then
            null
        else
            let byteEmitter (bytePtr:nativeptr<byte>) =
                match bytePtr |> NativePtr.read with
                | 0uy ->  None
                | nextByte -> Some (nextByte, 1 |> NativePtr.add bytePtr)

            ptr 
            |> NativePtr.ofNativeInt<byte>
            |> Seq.unfold byteEmitter
            |> Seq.toArray
            |> encoder


    let internal intPtrToStringUtf8 =
        intPtrToString Encoding.UTF8.GetString


    let internal intPtrToStringAscii=
        intPtrToString Encoding.ASCII.GetString

    type Pointer(ptr:IntPtr, destroyFunc: IntPtr->unit) =
        let mutable disposed = false
        let pointer = ptr
        member this.Pointer
            with get() = if disposed then IntPtr.Zero else ptr
        member this.Destroy() : unit =
            if ptr <> IntPtr.Zero then
                destroyFunc(ptr)
        member private x.Dispose(disposing:bool) =
            match disposed, disposing with
            | false, true ->
                if ptr <> IntPtr.Zero then
                    destroyFunc(ptr)
            | _ ->
                ()
            disposed <- true
        override x.Finalize() = x.Dispose(false)
        interface IDisposable with
            member x.Dispose()=
                x.Dispose(true)
                GC.SuppressFinalize(x)
