﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System
open SDL
open Microsoft.FSharp.NativeInterop

module Texture =

    [<Flags>]
    type BlendMode =
        | None = 0
        | Blend = 1
        | Add = 2
        | Mod = 4

    type Access =
        | Static = 0
        | Streaming = 1
        | Target = 2

    [<Flags>]
    type Modulate =
        | None = 0
        | Color = 1
        | Alpha = 2

    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateTexture")>]
        extern IntPtr WinSdlCreateTexture(IntPtr renderer, uint32 format, int access, int w, int h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyTexture")>]
        extern void WinSdlDestroyTexture(IntPtr texture)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateTextureFromSurface")>]
        extern IntPtr WinSdlCreateTextureFromSurface(IntPtr renderer, IntPtr surface)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_QueryTexture")>]
        extern int WinSdlQueryTexture(IntPtr texture, uint32* format, int* access, int* w, int* h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetTextureColorMod")>]
        extern int WinSdlSetTextureColorMod(IntPtr texture, uint8 r, uint8 g, uint8 b)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetTextureColorMod")>]
        extern int WinSdlGetTextureColorMod(IntPtr texture, uint8* r, uint8* g, uint8* b)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetTextureAlphaMod")>]
        extern int WinSdlSetTextureAlphaMod(IntPtr texture, uint8 alpha)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetTextureAlphaMod")>]
        extern int WinSdlGetTextureAlphaMod(IntPtr texture, uint8* alpha)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetTextureBlendMode")>]
        extern int WinSdlSetTextureBlendMode(IntPtr texture, int blendMode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetTextureBlendMode")>]
        extern int WinSdlGetTextureBlendMode(IntPtr texture, int* blendMode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateTexture")>]
        extern int WinSdlUpdateTexture(IntPtr texture, IntPtr rect, IntPtr pixels, int pitch)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateYUVTexture")>]
        extern int WinSdlUpdateYUVTexture(IntPtr texture, IntPtr rect, IntPtr Yplane, int Ypitch, IntPtr Uplane, int Upitch, IntPtr Vplane, int Vpitch)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LockTexture")>]
        extern int WinSdlLockTexture(IntPtr texture, IntPtr rect, IntPtr pixels, IntPtr pitch)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UnlockTexture")>]
        extern void WinSdlUnlockTexture(IntPtr texture)

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateTexture")>]
        extern IntPtr LinuxSdlCreateTexture(IntPtr renderer, uint32 format, int access, int w, int h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyTexture")>]
        extern void LinuxSdlDestroyTexture(IntPtr texture)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateTextureFromSurface")>]
        extern IntPtr LinuxSdlCreateTextureFromSurface(IntPtr renderer, IntPtr surface)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_QueryTexture")>]
        extern int LinuxSdlQueryTexture(IntPtr texture, uint32* format, int* access, int* w, int* h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetTextureColorMod")>]
        extern int LinuxSdlSetTextureColorMod(IntPtr texture, uint8 r, uint8 g, uint8 b)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetTextureColorMod")>]
        extern int LinuxSdlGetTextureColorMod(IntPtr texture, uint8* r, uint8* g, uint8* b)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetTextureAlphaMod")>]
        extern int LinuxSdlSetTextureAlphaMod(IntPtr texture, uint8 alpha)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetTextureAlphaMod")>]
        extern int LinuxSdlGetTextureAlphaMod(IntPtr texture, uint8* alpha)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetTextureBlendMode")>]
        extern int LinuxSdlSetTextureBlendMode(IntPtr texture, int blendMode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetTextureBlendMode")>]
        extern int LinuxSdlGetTextureBlendMode(IntPtr texture, int* blendMode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateTexture")>]
        extern int LinuxSdlUpdateTexture(IntPtr texture, IntPtr rect, IntPtr pixels, int pitch)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UpdateYUVTexture")>]
        extern int LinuxSdlUpdateYUVTexture(IntPtr texture, IntPtr rect, IntPtr Yplane, int Ypitch, IntPtr Uplane, int Upitch, IntPtr Vplane, int Vpitch)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LockTexture")>]
        extern int LinuxSdlLockTexture(IntPtr texture, IntPtr rect, IntPtr pixels, IntPtr pitch)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UnlockTexture")>]
        extern void LinuxSdlUnlockTexture(IntPtr texture)

        let SdlCreateTexture           = (WinSdlCreateTexture           , LinuxSdlCreateTexture           ) |> toNative
        let SdlDestroyTexture          = (WinSdlDestroyTexture          , LinuxSdlDestroyTexture          ) |> toNative
        let SdlCreateTextureFromSurface= (WinSdlCreateTextureFromSurface, LinuxSdlCreateTextureFromSurface) |> toNative
        let SdlQueryTexture            = (WinSdlQueryTexture            , LinuxSdlQueryTexture            ) |> toNative
        let SdlSetTextureColorMod      = (WinSdlSetTextureColorMod      , LinuxSdlSetTextureColorMod      ) |> toNative
        let SdlGetTextureColorMod      = (WinSdlGetTextureColorMod      , LinuxSdlGetTextureColorMod      ) |> toNative
        let SdlSetTextureAlphaMod      = (WinSdlSetTextureAlphaMod      , LinuxSdlSetTextureAlphaMod      ) |> toNative
        let SdlGetTextureAlphaMod      = (WinSdlGetTextureAlphaMod      , LinuxSdlGetTextureAlphaMod      ) |> toNative
        let SdlSetTextureBlendMode     = (WinSdlSetTextureBlendMode     , LinuxSdlSetTextureBlendMode     ) |> toNative
        let SdlGetTextureBlendMode     = (WinSdlGetTextureBlendMode     , LinuxSdlGetTextureBlendMode     ) |> toNative
        let SdlUpdateTexture           = (WinSdlUpdateTexture           , LinuxSdlUpdateTexture           ) |> toNative
        let SdlUpdateYUVTexture        = (WinSdlUpdateYUVTexture        , LinuxSdlUpdateYUVTexture        ) |> toNative
        let SdlLockTexture             = (WinSdlLockTexture             , LinuxSdlLockTexture             ) |> toNative
        let SdlUnlockTexture           = (WinSdlUnlockTexture           , LinuxSdlUnlockTexture           ) |> toNative


    type Texture(ptr: IntPtr, destroyFunc: IntPtr -> unit) =
        inherit SDL.Utility.Pointer(ptr, destroyFunc)
        new(ptr: IntPtr) = new Texture(ptr, Native.SdlDestroyTexture)
        static member Invalid = new Texture(IntPtr.Zero, Native.SdlDestroyTexture)


    let create (format: uint32) (access: Access) (w: int, h: int) (renderer: SDL.Utility.Pointer) =
        let ptr = Native.SdlCreateTexture(renderer.Pointer, format, access |> int, w, h)
        new Texture(ptr)

    let fromSurface (renderer: SDL.Utility.Pointer) surface =
        let ptr = Native.SdlCreateTextureFromSurface(renderer.Pointer, surface)
        new Texture(ptr, Native.SdlDestroyTexture)

    let update (dstrect: SDL.Geometry.Rectangle option) (src: SDL.Surface.Surface) (texture: Texture): bool =
        dstrect
        |> SDL.Geometry.withSdlRectPointer (fun rectptr ->
            let surf =
                src.Pointer
                |> NativePtr.ofNativeInt<SDL.Surface.SdlSurface>
                |> NativePtr.read
            0 = Native.SdlUpdateTexture(texture.Pointer, rectptr, surf.pixels, surf.pitch))

    let query (texture: Texture): (uint32 * Access * int * int) option =
        let mutable format = 0u
        let mutable access = 0
        let mutable width = 0
        let mutable height = 0
        let pformat = &&format
        let paccess = &&access
        let pwidth = &&width
        let pheight = &&height
        let result = Native.SdlQueryTexture(texture.Pointer, pformat, paccess, pwidth, pheight)
        if result = 0
        then Some(format, access |> enum<Access>, width, height)
        else None

    let setModulation (color: Pixel.Color) (texture: Texture): bool =
        let colorResult = Native.SdlSetTextureColorMod(texture.Pointer, color.Red, color.Green, color.Blue)
        if colorResult = 0
        then Native.SdlSetTextureAlphaMod(texture.Pointer, color.Alpha) = 0
        else false

    let getModulation (texture: Texture): Pixel.Color option =
        let mutable r = 0uy
        let mutable g = 0uy
        let mutable b = 0uy
        let pr = &&r
        let pg = &&g
        let pb = &&b

        let colorResult = Native.SdlGetTextureColorMod(texture.Pointer, pr, pg, pb)

        if colorResult = 0 then
            let mutable a = 0uy
            let pa = &&a
            let alphaResult = Native.SdlGetTextureAlphaMod(texture.Pointer, pa)
            if alphaResult = 0 then
                Some
                    { Pixel.Color.Red = r
                      Green = g
                      Blue = b
                      Alpha = a }
            else
                None
        else
            None

    let setBlendMode (blendMode: BlendMode) (texture: Texture): bool =
        Native.SdlSetTextureBlendMode(texture.Pointer, blendMode |> int) = 0

    let getBlendMode (texture: Texture): BlendMode option =
        let mutable blendMode = 0
        let pblendMode = &&blendMode
        let result = Native.SdlGetTextureBlendMode(texture.Pointer, pblendMode)
        if result = 0 then Some(blendMode |> enum<BlendMode>) else None
