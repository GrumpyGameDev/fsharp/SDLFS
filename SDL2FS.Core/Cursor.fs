﻿namespace SDL


open System
open System.Runtime.InteropServices

module Cursor = 

    type SdlCursor = IntPtr

    type SystemCursor =
        | Arrow = 0
        | Ibeam = 1 
        | Wait = 2 
        | Crosshair=3
        | WaitArrow=4
        | SizeNWSE=5
        | SizeNESW=6
        | SizeWE=7
        | SizeNS=8  
        | SizeAll=9
        | No=10 
        | Hand=11     

    module internal Native = 
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateCursor")>]
        extern SdlCursor WinSdlCreateCursor(uint8 * data,uint8 * mask,int w, int h, int hot_x,int hot_y)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateColorCursor")>]
        extern SdlCursor WinSdlCreateColorCursor(IntPtr surface,int hot_x,int hot_y)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateSystemCursor")>]
        extern SdlCursor WinSdlCreateSystemCursor(SystemCursor id)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetCursor")>]
        extern void WinSdlSetCursor(SdlCursor  cursor)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCursor")>]
        extern SdlCursor WinSdlGetCursor()//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDefaultCursor")>]
        extern SdlCursor WinSdlGetDefaultCursor()//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreeCursor")>]
        extern void WinSdlFreeCursor(SdlCursor  cursor)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ShowCursor")>]
        extern int WinSdlShowCursor(int toggle)//TODO: expose
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateCursor")>]
        extern SdlCursor LinuxSdlCreateCursor(uint8 * data,uint8 * mask,int w, int h, int hot_x,int hot_y)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateColorCursor")>]
        extern SdlCursor LinuxSdlCreateColorCursor(IntPtr surface,int hot_x,int hot_y)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateSystemCursor")>]
        extern SdlCursor LinuxSdlCreateSystemCursor(SystemCursor id)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetCursor")>]
        extern void LinuxSdlSetCursor(SdlCursor  cursor)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCursor")>]
        extern SdlCursor LinuxSdlGetCursor()//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDefaultCursor")>]
        extern SdlCursor LinuxSdlGetDefaultCursor()//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreeCursor")>]
        extern void LinuxSdlFreeCursor(SdlCursor  cursor)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ShowCursor")>]
        extern int LinuxSdlShowCursor(int toggle)//TODO: expose
    
        let SdlCreateCursor       = (WinSdlCreateCursor       , LinuxSdlCreateCursor       ) |> toNative
        let SdlCreateColorCursor  = (WinSdlCreateColorCursor  , LinuxSdlCreateColorCursor  ) |> toNative
        let SdlCreateSystemCursor = (WinSdlCreateSystemCursor , LinuxSdlCreateSystemCursor ) |> toNative
        let SdlSetCursor          = (WinSdlSetCursor          , LinuxSdlSetCursor          ) |> toNative
        let SdlGetCursor          = (WinSdlGetCursor          , LinuxSdlGetCursor          ) |> toNative
        let SdlGetDefaultCursor   = (WinSdlGetDefaultCursor   , LinuxSdlGetDefaultCursor   ) |> toNative
        let SdlFreeCursor         = (WinSdlFreeCursor         , LinuxSdlFreeCursor         ) |> toNative
        let SdlShowCursor         = (WinSdlShowCursor         , LinuxSdlShowCursor         ) |> toNative

