﻿namespace SDL

open System.Runtime.InteropServices
open System

module ScreenSaver =
    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IsScreenSaverEnabled")>]
        extern int WinSdlIsScreenSaverEnabled()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_EnableScreenSaver")>]
        extern void WinSdlEnableScreenSaver()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DisableScreenSaver")>]
        extern void WinSdlDisableScreenSaver()

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IsScreenSaverEnabled")>]
        extern int LinuxSdlIsScreenSaverEnabled()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_EnableScreenSaver")>]
        extern void LinuxSdlEnableScreenSaver()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DisableScreenSaver")>]
        extern void LinuxSdlDisableScreenSaver()

        let SdlIsScreenSaverEnabled= (WinSdlIsScreenSaverEnabled, LinuxSdlIsScreenSaverEnabled) |> toNative
        let SdlEnableScreenSaver   = (WinSdlEnableScreenSaver   , LinuxSdlEnableScreenSaver   ) |> toNative
        let SdlDisableScreenSaver  = (WinSdlDisableScreenSaver  , LinuxSdlDisableScreenSaver  ) |> toNative

    let enable = Native.SdlEnableScreenSaver

    let disable = Native.SdlDisableScreenSaver

    let isEnabled = Native.SdlIsScreenSaverEnabled >> (<>) 0

