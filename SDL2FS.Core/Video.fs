﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System
open Microsoft.FSharp.NativeInterop

module Video =
    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlDisplayMode =
        struct
            val mutable format       : uint32
            val mutable w            : int
            val mutable h            : int 
            val mutable refresh_rate : int
            val mutable driverdata   : IntPtr
        end

    module private Native =
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumVideoDrivers")>]
        extern int WinSdlGetNumVideoDrivers();
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetVideoDriver")>]
        extern IntPtr WinSdlGetVideoDriver(int index);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_VideoInit")>]
        extern int WinSdlVideoInit(IntPtr driver_name);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_VideoQuit")>]
        extern void WinSdlVideoQuit();
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCurrentVideoDriver")>]
        extern IntPtr WinSdlGetCurrentVideoDriver();
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumVideoDisplays")>]
        extern int WinSdlGetNumVideoDisplays();
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayName")>]
        extern IntPtr WinSdlGetDisplayName(int displayIndex);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayBounds")>]
        extern int WinSdlGetDisplayBounds(int displayIndex, SDL.Geometry.SdlRect* rect);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayDPI")>]
        extern int WinSdlGetDisplayDPI(int displayIndex, float* ddpi, float* hdpi, float* vdpi);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumDisplayModes")>]
        extern int WinSdlGetNumDisplayModes(int displayIndex);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayMode")>]
        extern int WinSdlGetDisplayMode(int displayIndex, int modeIndex, IntPtr mode);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCurrentDisplayMode")>]
        extern int WinSdlGetCurrentDisplayMode(int displayIndex, IntPtr mode);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetClosestDisplayMode")>]
        extern IntPtr WinSdlGetClosestDisplayMode(int displayIndex, IntPtr mode, IntPtr closest);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDesktopDisplayMode")>]
        extern int WinSdlGetDesktopDisplayMode(int displayIndex, IntPtr mode);
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumVideoDrivers")>]
        extern int LinuxSdlGetNumVideoDrivers();
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetVideoDriver")>]
        extern IntPtr LinuxSdlGetVideoDriver(int index);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_VideoInit")>]
        extern int LinuxSdlVideoInit(IntPtr driver_name);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_VideoQuit")>]
        extern void LinuxSdlVideoQuit();
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCurrentVideoDriver")>]
        extern IntPtr LinuxSdlGetCurrentVideoDriver();
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumVideoDisplays")>]
        extern int LinuxSdlGetNumVideoDisplays();
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayName")>]
        extern IntPtr LinuxSdlGetDisplayName(int displayIndex);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayBounds")>]
        extern int LinuxSdlGetDisplayBounds(int displayIndex, SDL.Geometry.SdlRect* rect);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayDPI")>]
        extern int LinuxSdlGetDisplayDPI(int displayIndex, float* ddpi, float* hdpi, float* vdpi);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumDisplayModes")>]
        extern int LinuxSdlGetNumDisplayModes(int displayIndex);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDisplayMode")>]
        extern int LinuxSdlGetDisplayMode(int displayIndex, int modeIndex, IntPtr mode);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCurrentDisplayMode")>]
        extern int LinuxSdlGetCurrentDisplayMode(int displayIndex, IntPtr mode);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetClosestDisplayMode")>]
        extern IntPtr LinuxSdlGetClosestDisplayMode(int displayIndex, IntPtr mode, IntPtr closest);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetDesktopDisplayMode")>]
        extern int LinuxSdlGetDesktopDisplayMode(int displayIndex, IntPtr mode);
    
        let SdlGetNumVideoDrivers = (WinSdlGetNumVideoDrivers, LinuxSdlGetNumVideoDrivers) |> toNative
        let SdlGetVideoDriver = (WinSdlGetVideoDriver, LinuxSdlGetVideoDriver) |> toNative
        let SdlVideoInit = (WinSdlVideoInit, LinuxSdlVideoInit) |> toNative
        let SdlVideoQuit = (WinSdlVideoQuit, LinuxSdlVideoQuit) |> toNative
        let SdlGetCurrentVideoDriver = (WinSdlGetCurrentVideoDriver, LinuxSdlGetCurrentVideoDriver) |> toNative
        let SdlGetNumVideoDisplays = (WinSdlGetNumVideoDisplays, LinuxSdlGetNumVideoDisplays) |> toNative
        let SdlGetDisplayName = (WinSdlGetDisplayName, LinuxSdlGetDisplayName) |> toNative
        let SdlGetDisplayBounds = (WinSdlGetDisplayBounds, LinuxSdlGetDisplayBounds) |> toNative
        let SdlGetDisplayDPI = (WinSdlGetDisplayDPI, LinuxSdlGetDisplayDPI) |> toNative
        let SdlGetNumDisplayModes = (WinSdlGetNumDisplayModes, LinuxSdlGetNumDisplayModes) |> toNative
        let SdlGetDisplayMode = (WinSdlGetDisplayMode, LinuxSdlGetDisplayMode) |> toNative
        let SdlGetCurrentDisplayMode = (WinSdlGetCurrentDisplayMode, LinuxSdlGetCurrentDisplayMode) |> toNative
        let SdlGetClosestDisplayMode = (WinSdlGetClosestDisplayMode, LinuxSdlGetClosestDisplayMode) |> toNative
        let SdlGetDesktopDisplayMode = (WinSdlGetDesktopDisplayMode, LinuxSdlGetDesktopDisplayMode) |> toNative

    /////////////////////////////////////////////////////////////////////////////////
    //Drivers
    /////////////////////////////////////////////////////////////////////////////////

    let getDrivers () :string list =
        [0..(Native.SdlGetNumVideoDrivers()-1)]
        |> List.map (Native.SdlGetVideoDriver >> SDL.Utility.intPtrToStringAscii)

    let getCurrentDriver: unit -> string =
        Native.SdlGetCurrentVideoDriver
        >> SDL.Utility.intPtrToStringAscii

    let internal initVideo (driverName:string) :bool =
        driverName
        |> SDL.Utility.withAsciiString(fun ptr-> Native.SdlVideoInit(ptr) = 0)

    let internal quitVideo () :unit =
        Native.SdlVideoQuit()

    type Video (driverName:string) =
        do
            initVideo driverName
            |> ignore
        interface IDisposable with
            member this.Dispose() = 
                quitVideo()

    /////////////////////////////////////////////////////////////////////////////////
    //Modes
    /////////////////////////////////////////////////////////////////////////////////

    type DisplayMode =
        {Format: uint32;
        Width:int;
        Height:int;
        RefreshRate:int;
        Data:IntPtr}

    let internal sdlDisplayModeToDisplayMode (mode:SdlDisplayMode) :DisplayMode =
        {Format = mode.format;
        Width = mode.w;
        Height= mode.h;
        RefreshRate = mode.refresh_rate;
        Data = mode.driverdata}

    let internal displayModeToSdlDisplayMode (mode:DisplayMode) :SdlDisplayMode =
        SdlDisplayMode(
            format = mode.Format,
            w = mode.Width,
            h= mode.Height,
            refresh_rate = mode.RefreshRate,
            driverdata = mode.Data)

    let internal getDisplayModes (index:int) : List<DisplayMode> =
        [0..(Native.SdlGetNumDisplayModes(index) - 1)]
        |> List.map 
            (fun modeIndex->
                let mutable mode:SdlDisplayMode = SdlDisplayMode()
                let ptr = NativePtr.toNativeInt &&mode
                
                Native.SdlGetDisplayMode(index,modeIndex,ptr)
                |> ignore

                sdlDisplayModeToDisplayMode mode)
    
    let internal getCurrentDisplayMode (index:int) :DisplayMode =
        let mutable mode:SdlDisplayMode = SdlDisplayMode()
        let ptr = NativePtr.toNativeInt &&mode
                
        Native.SdlGetCurrentDisplayMode(index, ptr)
        |> ignore

        sdlDisplayModeToDisplayMode mode
    
    let internal getDesktopDisplayMode (index:int) :DisplayMode =
        let mutable mode:SdlDisplayMode = SdlDisplayMode()
        let ptr = NativePtr.toNativeInt &&mode
                
        Native.SdlGetDesktopDisplayMode(index, ptr)
        |> ignore

        sdlDisplayModeToDisplayMode mode

    let getClosestDisplayMode (index:int) (mode:DisplayMode) :DisplayMode =
        let mutable displayMode' = displayModeToSdlDisplayMode mode
        let ptr = NativePtr.toNativeInt  &&displayMode'

        let mutable resultMode:SdlDisplayMode = SdlDisplayMode()
        let resultPtr = NativePtr.toNativeInt &&resultMode
                
        Native.SdlGetClosestDisplayMode(index, ptr, resultPtr)
        |> ignore

        sdlDisplayModeToDisplayMode resultMode

    /////////////////////////////////////////////////////////////////////////////////
    //Displays
    /////////////////////////////////////////////////////////////////////////////////

    let internal getDisplayName (index:int) :string = 
        Native.SdlGetDisplayName(index)
        |> SDL.Utility.intPtrToStringAscii

    let internal getDisplayBounds (index:int) : Geometry.Rectangle =
        let mutable rect:SDL.Geometry.SdlRect = SDL.Geometry.SdlRect()
        let prect = &&rect

        Native.SdlGetDisplayBounds(index,prect)
        |> ignore

        rect
        |> SDL.Geometry.sdlRectToRectangle

    type DisplayDPI = 
        {Diagonal: float; Horizontal: float; Vertical:float}

    let internal getDisplayDPI (index:int) : DisplayDPI option =
        let mutable ddpi:float = 0.0
        let mutable hdpi:float = 0.0
        let mutable vdpi:float = 0.0
        let pddpi = &&ddpi
        let phdpi = &&hdpi
        let pvdpi = &&vdpi

        let result = 
            Native.SdlGetDisplayDPI(index, pddpi, phdpi, pvdpi)

        if result = 0 then
            {Diagonal = ddpi; Horizontal = hdpi; Vertical = vdpi} |> Some
        else
            None

    type DisplayProperties = 
        {Index:int; 
        Name:string; 
        Bounds: Geometry.Rectangle; 
        DPI: DisplayDPI option;
        CurrentMode: DisplayMode;
        DesktopMode: DisplayMode;
        AvailableModes: DisplayMode list}

    let getDisplays() : DisplayProperties list =
        [0..(Native.SdlGetNumVideoDisplays()-1)]
        |> List.map 
            (fun displayIndex->
                {Index=displayIndex;
                Name=getDisplayName displayIndex;
                Bounds=getDisplayBounds displayIndex;
                DPI=getDisplayDPI displayIndex;
                CurrentMode=getCurrentDisplayMode displayIndex;
                DesktopMode = getDesktopDisplayMode displayIndex;
                AvailableModes = getDisplayModes displayIndex})
