﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open SDL
open System
open Microsoft.FSharp.NativeInterop

[<AutoOpen>]
module Geometry = 
    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlPoint =
        struct
            val mutable x: int
            val mutable y: int
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlRect = 
        struct
            val mutable x :int
            val mutable y :int
            val mutable w :int
            val mutable h :int
        end

    module private Native =
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasIntersection")>]
        extern int WinSdlHasIntersection(SdlRect* A, SdlRect* B)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IntersectRect")>]
        extern int WinSdlIntersectRect(SdlRect* A, SdlRect* B, SdlRect* result)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UnionRect")>]
        extern void WinSdlUnionRect(SdlRect* A, SdlRect* B, SdlRect* result)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_EnclosePoints")>]
        extern int WinSdlEnclosePoints(SdlPoint* points, int count, SdlRect* clip, SdlRect* result)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IntersectRectAndLine")>]
        extern int WinSdlIntersectRectAndLine(SdlRect* rect, int* X1, int* Y1, int* X2, int* Y2)//TODO: expose
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasIntersection")>]
        extern int LinuxSdlHasIntersection(SdlRect* A, SdlRect* B)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IntersectRect")>]
        extern int LinuxSdlIntersectRect(SdlRect* A, SdlRect* B, SdlRect* result)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_UnionRect")>]
        extern void LinuxSdlUnionRect(SdlRect* A, SdlRect* B, SdlRect* result)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_EnclosePoints")>]
        extern int LinuxSdlEnclosePoints(SdlPoint* points, int count, SdlRect* clip, SdlRect* result)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IntersectRectAndLine")>]
        extern int LinuxSdlIntersectRectAndLine(SdlRect* rect, int* X1, int* Y1, int* X2, int* Y2)//TODO: expose
    
        let SdlHasIntersection      = (WinSdlHasIntersection      , LinuxSdlHasIntersection     ) |> toNative
        let SdlIntersectRect        = (WinSdlIntersectRect        , LinuxSdlIntersectRect       ) |> toNative
        let SdlUnionRect            = (WinSdlUnionRect            , LinuxSdlUnionRect           ) |> toNative
        let SdlEnclosePoints        = (WinSdlEnclosePoints        , LinuxSdlEnclosePoints       ) |> toNative
        let SdlIntersectRectAndLine = (WinSdlIntersectRectAndLine , LinuxSdlIntersectRectAndLine) |> toNative

    type Point = 
        {
            X: int
            Y: int
        }
        member this.SetX (x:int) : Point =
            {this with X = x}
        member this.SetY (y:int) : Point =
            {this with Y = y}

        member this.MoveX (deltaX:int) : Point =
            deltaX + this.X
            |> this.SetX
        member this.MoveY (deltaY:int) : Point =
            deltaY + this.Y
            |> this.SetY
        member this.MoveBy (delta:Point) : Point =
            {
                X = this.X + delta.X
                Y = this.Y + delta.Y
            }
        member this.ScaleX (scaleX:int) : Point =
            scaleX * this.X
            |> this.SetX
        member this.ScaleY (scaleY:int) : Point =
            scaleY * this.Y
            |> this.SetY
        member this.ScaleBy (scale:Point) : Point =
            {
                X = this.X * scale.X
                Y = this.Y * scale.Y
            }
        static member (~-) (point:Point) : Point =
            {
                X=(-point.X)
                Y=(-point.Y)
            }
        static member (~+) (point:Point) : Point =
            point
           
        static member (+) (first:Point, second:Point) : Point =
            second.MoveBy first

        static member (-) (first:Point, second:Point) : Point =
            (-second) + first

        static member (*) (first:Point, second:Point) : Point =
            second.ScaleBy first

    module Point =
        let create (x:int, y:int) : Point =
            {
                X = x
                Y = y
            }

        let scaleBy (scale:Point) (point:Point) : Point =
            point.ScaleBy scale

        let moveBy (delta:Point) (point:Point) : Point =
            point.MoveBy delta
        

    type Rectangle = 
        {
            X: int
            Y: int
            Width: int
            Height: int
        }
        member x.IsEmpty : bool =
            x.Width<=0 || x.Height<=0
        member x.Contains (point:Point) : bool =
            point.X>=x.X && point.Y>=x.Y && point.X<(x.X+x.Width) && point.Y<(x.Y+x.Height)

    let internal rectangleToSdlRect (r:Rectangle) :SdlRect =
        SdlRect(x=r.X,y=r.Y,w=r.Width,h=r.Height)

    let internal withSdlRectPointer (func:IntPtr->'T) (rectangle:Rectangle option)=
        let mutable sdlrect = SdlRect()
        let rectptr =
            if rectangle.IsNone then
                IntPtr.Zero
            else
                sdlrect <- (rectangle.Value |> rectangleToSdlRect)
                NativePtr.toNativeInt &&sdlrect
        func rectptr


    let internal pointToSdlPoint (p:Point) :SdlPoint =
        SdlPoint(x=p.X,y=p.Y)

    let internal sdlRectToRectangle (r:SdlRect):Rectangle =
        {X = r.x; Y=r.y; Width=r.w; Height=r.h}

    let internal sdlPointToPoint (p:SdlPoint) :Point =
        {X = p.x; Y=p.y}

    module Rectangle =
        let create (x:int,y:int,w:int,h:int): Rectangle =
            {
                X = x
                Y = y
                Width = w
                Height = h
            }

        let contains (point: Point) (rectangle:Rectangle) :bool =
            rectangle.Contains point

        let isEmpty (rectangle:Rectangle option) :bool =
            match rectangle with
            | None -> true
            | Some r -> r.IsEmpty

        let equals (first:Rectangle option) (second:Rectangle option) : bool =
            match (first,second) with
            | (None, None) -> true
            | (Some r1, Some r2) -> r1.X=r2.X && r1.Y=r2.Y && r1.Width = r2.Width && r1.Height = r2.Height
            | _ -> false

        let hasIntersection (a:Rectangle) (b:Rectangle): bool =
            let mutable r1 = a |> rectangleToSdlRect
            let mutable r2 = b |> rectangleToSdlRect
            let pr1 = &&r1
            let pr2 = &&r2
            0 <> Native.SdlHasIntersection(pr1, pr2)

        let intersect (a:Rectangle) (b:Rectangle): Rectangle =
            let mutable r1 = a |> rectangleToSdlRect
            let mutable r2 = b |> rectangleToSdlRect
            let mutable r3 = SdlRect()
            let pr1 = &&r1
            let pr2 = &&r2
            let pr3 = &&r3
            Native.SdlIntersectRect(pr1, pr2, pr3) |> ignore
            sdlRectToRectangle(r3)

        let union (a:Rectangle) (b:Rectangle): Rectangle =
            let mutable r1 = a |> rectangleToSdlRect
            let mutable r2 = b |> rectangleToSdlRect
            let mutable r3 = SdlRect()
            let pr1 = &&r1
            let pr2 = &&r2
            let pr3 = &&r3
            Native.SdlUnionRect(pr1, pr2, pr3) |> ignore
            sdlRectToRectangle(r3)
    
    type Rectangle with
        member x.Intersects (other:Rectangle) : bool =
            (x,other)
            ||> Rectangle.intersect
            |> Some
            |> Rectangle.isEmpty

        static member (+) (first:Rectangle, second:Rectangle) : Rectangle =
            Rectangle.union first second

        static member (-) (first:Rectangle, second:Rectangle) : Rectangle =
            Rectangle.intersect first second