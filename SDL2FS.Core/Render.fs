﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System
open SDL
open SDL.Surface
open FSharp.NativeInterop

module Render = 

    [<Flags>]
    type Flags = 
        | Software      = 0x00000001
        | Accelerated   = 0x00000002
        | PresentVSync  = 0x00000004
        | TargetTexture = 0x00000008

    [<Flags>]
    type Flip =
        | None = 0x00000000
        | Horizontal = 0x00000001
        | Vertical = 0x00000002

    [<StructLayout(LayoutKind.Sequential)>]
    type internal NativeRendererInfo =
        struct
            val mutable name:IntPtr
            val mutable flags:uint32
            val mutable num_texture_formats:uint32
            val mutable texture_format0: uint32//this is goofy, but works for now
            val mutable texture_format1: uint32
            val mutable texture_format2: uint32
            val mutable texture_format3: uint32
            val mutable texture_format4: uint32
            val mutable texture_format5: uint32
            val mutable texture_format6: uint32
            val mutable texture_format7: uint32
            val mutable texture_format8: uint32
            val mutable texture_format9: uint32
            val mutable texture_format10: uint32
            val mutable texture_format11: uint32
            val mutable texture_format12: uint32
            val mutable texture_format13: uint32
            val mutable texture_format14: uint32
            val mutable texture_format15: uint32
            val mutable max_texture_width:int
            val mutable max_texture_height:int
        end

     type RendererInfo =
         {Name:string;
         Flags:Flags;
         TextureFormats:seq<uint32>;
         MaximumTextureWidth:int;
         MaximumTextureHeight:int}
                                           
    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateRenderer")>]
        extern IntPtr WinCreateRenderer(IntPtr window, int index, uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyRenderer")>]
        extern void WinDestroyRenderer(IntPtr renderer)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderClear")>]
        extern int WinRenderClear(IntPtr renderer)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderPresent")>]
        extern void WinRenderPresent(IntPtr renderer)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumRenderDrivers")>]
        extern int WinGetNumRenderDrivers()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderDriverInfo")>]
        extern int WinGetRenderDriverInfo(int index, NativeRendererInfo* info)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateSoftwareRenderer")>]
        extern IntPtr WinCreateSoftwareRenderer(IntPtr surface)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRendererInfo")>]
        extern int WinGetRendererInfo(IntPtr renderer, NativeRendererInfo*  info)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRendererOutputSize")>]
        extern int WinGetRendererOutputSize(IntPtr renderer, int* w, int* h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderTargetSupported")>]
        extern int WinRenderTargetSupported(IntPtr renderer)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRenderTarget")>]
        extern int WinSetRenderTarget(IntPtr renderer, IntPtr texture)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderTarget")>]
        extern IntPtr WinGetRenderTarget(IntPtr renderer)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetLogicalSize")>]
        extern int WinRenderSetLogicalSize(IntPtr renderer, int w, int h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetLogicalSize")>]
        extern void WinRenderGetLogicalSize(IntPtr renderer, int* w, int* h)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetViewport")>]
        extern int WinRenderSetViewport(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetViewport")>]
        extern void WinRenderGetViewport(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetClipRect")>]
        extern int WinRenderSetClipRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetClipRect")>]
        extern void WinRenderGetClipRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderIsClipEnabled")>]
        extern int WinRenderIsClipEnabled(IntPtr renderer)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetScale")>]
        extern int WinRenderSetScale(IntPtr  renderer, float scaleX, float scaleY)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetScale")>]
        extern void WinRenderGetScale(IntPtr renderer, float* scaleX, float* scaleY)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRenderDrawColor")>]
        extern int WinSetRenderDrawColor(IntPtr renderer, uint8 r, uint8 g, uint8 b, uint8 a)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderDrawColor")>]
        extern int WinGetRenderDrawColor(IntPtr renderer, uint8*  r, uint8*  g, uint8*  b, uint8*  a)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRenderDrawBlendMode")>]
        extern int WinSetRenderDrawBlendMode(IntPtr renderer, int blendMode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderDrawBlendMode")>]
        extern int WinGetRenderDrawBlendMode(IntPtr renderer, int* blendMode)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawPoint")>]
        extern int WinRenderDrawPoint(IntPtr renderer, int x, int y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawPoints")>]
        extern int WinRenderDrawPoints(IntPtr renderer, IntPtr points, int count)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawLine")>]
        extern int WinRenderDrawLine(IntPtr renderer, int x1, int y1, int x2, int y2)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawLines")>]
        extern int WinRenderDrawLines(IntPtr renderer, IntPtr   points, int count)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawRect")>]
        extern int WinRenderDrawRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawRects")>]
        extern int WinRenderDrawRects(IntPtr renderer, IntPtr rects, int count)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderFillRect")>]
        extern int WinRenderFillRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderFillRects")>]
        extern int WinRenderFillRects(IntPtr renderer, IntPtr rects, int count)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderCopy")>]
        extern int WinRenderCopy(IntPtr renderer, IntPtr texture, IntPtr srcrect, IntPtr dstrect)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderCopyEx")>]
        extern int WinRenderCopyEx(IntPtr renderer, IntPtr  texture, IntPtr srcrect, IntPtr dstrect, double angle, IntPtr  center, int flip)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderReadPixels")>]
        extern int WinRenderReadPixels(IntPtr  renderer,IntPtr rect,uint32 format,IntPtr pixels, int pitch)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GL_BindTexture")>]
        extern int WinGLBindTexture(IntPtr texture, IntPtr texw, IntPtr texh)//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GL_UnbindTexture")>]
        extern int WinGLUnbindTexture(IntPtr texture)//TODO: expose

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateRenderer")>]
        extern IntPtr LinuxCreateRenderer(IntPtr window, int index, uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DestroyRenderer")>]
        extern void LinuxDestroyRenderer(IntPtr renderer)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderClear")>]
        extern int LinuxRenderClear(IntPtr renderer)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderPresent")>]
        extern void LinuxRenderPresent(IntPtr renderer)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetNumRenderDrivers")>]
        extern int LinuxGetNumRenderDrivers()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderDriverInfo")>]
        extern int LinuxGetRenderDriverInfo(int index, NativeRendererInfo* info)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CreateSoftwareRenderer")>]
        extern IntPtr LinuxCreateSoftwareRenderer(IntPtr surface)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRendererInfo")>]
        extern int LinuxGetRendererInfo(IntPtr renderer, NativeRendererInfo*  info)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRendererOutputSize")>]
        extern int LinuxGetRendererOutputSize(IntPtr renderer, int* w, int* h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderTargetSupported")>]
        extern int LinuxRenderTargetSupported(IntPtr renderer)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRenderTarget")>]
        extern int LinuxSetRenderTarget(IntPtr renderer, IntPtr texture)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderTarget")>]
        extern IntPtr LinuxGetRenderTarget(IntPtr renderer)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetLogicalSize")>]
        extern int LinuxRenderSetLogicalSize(IntPtr renderer, int w, int h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetLogicalSize")>]
        extern void LinuxRenderGetLogicalSize(IntPtr renderer, int* w, int* h)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetViewport")>]
        extern int LinuxRenderSetViewport(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetViewport")>]
        extern void LinuxRenderGetViewport(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetClipRect")>]
        extern int LinuxRenderSetClipRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetClipRect")>]
        extern void LinuxRenderGetClipRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderIsClipEnabled")>]
        extern int LinuxRenderIsClipEnabled(IntPtr renderer)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderSetScale")>]
        extern int LinuxRenderSetScale(IntPtr  renderer, float scaleX, float scaleY)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderGetScale")>]
        extern void LinuxRenderGetScale(IntPtr renderer, float* scaleX, float* scaleY)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRenderDrawColor")>]
        extern int LinuxSetRenderDrawColor(IntPtr renderer, uint8 r, uint8 g, uint8 b, uint8 a)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderDrawColor")>]
        extern int LinuxGetRenderDrawColor(IntPtr renderer, uint8*  r, uint8*  g, uint8*  b, uint8*  a)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRenderDrawBlendMode")>]
        extern int LinuxSetRenderDrawBlendMode(IntPtr renderer, int blendMode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRenderDrawBlendMode")>]
        extern int LinuxGetRenderDrawBlendMode(IntPtr renderer, int* blendMode)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawPoint")>]
        extern int LinuxRenderDrawPoint(IntPtr renderer, int x, int y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawPoints")>]
        extern int LinuxRenderDrawPoints(IntPtr renderer, IntPtr points, int count)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawLine")>]
        extern int LinuxRenderDrawLine(IntPtr renderer, int x1, int y1, int x2, int y2)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawLines")>]
        extern int LinuxRenderDrawLines(IntPtr renderer, IntPtr   points, int count)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawRect")>]
        extern int LinuxRenderDrawRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderDrawRects")>]
        extern int LinuxRenderDrawRects(IntPtr renderer, IntPtr rects, int count)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderFillRect")>]
        extern int LinuxRenderFillRect(IntPtr renderer, Geometry.SdlRect* rect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderFillRects")>]
        extern int LinuxRenderFillRects(IntPtr renderer, IntPtr rects, int count)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderCopy")>]
        extern int LinuxRenderCopy(IntPtr renderer, IntPtr texture, IntPtr srcrect, IntPtr dstrect)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderCopyEx")>]
        extern int LinuxRenderCopyEx(IntPtr renderer, IntPtr  texture, IntPtr srcrect, IntPtr dstrect, double angle, IntPtr  center, int flip)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RenderReadPixels")>]
        extern int LinuxRenderReadPixels(IntPtr  renderer,IntPtr rect,uint32 format,IntPtr pixels, int pitch)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GL_BindTexture")>]
        extern int LinuxGLBindTexture(IntPtr texture, IntPtr texw, IntPtr texh)//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GL_UnbindTexture")>]
        extern int LinuxGLUnbindTexture(IntPtr texture)//TODO: expose

        let CreateRenderer = (WinCreateRenderer, LinuxCreateRenderer) |> toNative
        let DestroyRenderer = (WinDestroyRenderer, LinuxDestroyRenderer) |> toNative
        let RenderClear = (WinRenderClear, LinuxRenderClear) |> toNative
        let RenderPresent = (WinRenderPresent, LinuxRenderPresent) |> toNative
        let GetNumRenderDrivers = (WinGetNumRenderDrivers, LinuxGetNumRenderDrivers) |> toNative
        let GetRenderDriverInfo = (WinGetRenderDriverInfo, LinuxGetRenderDriverInfo) |> toNative
        let CreateSoftwareRenderer = (WinCreateSoftwareRenderer, LinuxCreateSoftwareRenderer) |> toNative
        let GetRendererInfo = (WinGetRendererInfo, LinuxGetRendererInfo) |> toNative
        let GetRendererOutputSize = (WinGetRendererOutputSize, LinuxGetRendererOutputSize) |> toNative
        let RenderTargetSupported = (WinRenderTargetSupported, LinuxRenderTargetSupported) |> toNative
        let SetRenderTarget = (WinSetRenderTarget, LinuxSetRenderTarget) |> toNative
        let GetRenderTarget = (WinGetRenderTarget, LinuxGetRenderTarget) |> toNative
        let RenderSetLogicalSize = (WinRenderSetLogicalSize, LinuxRenderSetLogicalSize) |> toNative
        let RenderGetLogicalSize = (WinRenderGetLogicalSize, LinuxRenderGetLogicalSize) |> toNative
        let RenderSetViewport = (WinRenderSetViewport, LinuxRenderSetViewport) |> toNative
        let RenderGetViewport = (WinRenderGetViewport, LinuxRenderGetViewport) |> toNative
        let RenderSetClipRect = (WinRenderSetClipRect, LinuxRenderSetClipRect) |> toNative
        let RenderGetClipRect = (WinRenderGetClipRect, LinuxRenderGetClipRect) |> toNative
        let RenderIsClipEnabled = (WinRenderIsClipEnabled, LinuxRenderIsClipEnabled) |> toNative
        let RenderSetScale = (WinRenderSetScale, LinuxRenderSetScale) |> toNative
        let RenderGetScale = (WinRenderGetScale, LinuxRenderGetScale) |> toNative
        let SetRenderDrawColor = (WinSetRenderDrawColor, LinuxSetRenderDrawColor) |> toNative
        let GetRenderDrawColor = (WinGetRenderDrawColor, LinuxGetRenderDrawColor) |> toNative
        let SetRenderDrawBlendMode = (WinSetRenderDrawBlendMode, LinuxSetRenderDrawBlendMode) |> toNative
        let GetRenderDrawBlendMode = (WinGetRenderDrawBlendMode, LinuxGetRenderDrawBlendMode) |> toNative
        let RenderDrawPoint = (WinRenderDrawPoint, LinuxRenderDrawPoint) |> toNative
        let RenderDrawPoints = (WinRenderDrawPoints, LinuxRenderDrawPoints) |> toNative
        let RenderDrawLine = (WinRenderDrawLine, LinuxRenderDrawLine) |> toNative
        let RenderDrawLines = (WinRenderDrawLines, LinuxRenderDrawLines) |> toNative
        let RenderDrawRect = (WinRenderDrawRect, LinuxRenderDrawRect) |> toNative
        let RenderDrawRects = (WinRenderDrawRects, LinuxRenderDrawRects) |> toNative
        let RenderFillRect = (WinRenderFillRect, LinuxRenderFillRect) |> toNative
        let RenderFillRects = (WinRenderFillRects, LinuxRenderFillRects) |> toNative
        let RenderCopy = (WinRenderCopy, LinuxRenderCopy) |> toNative
        let RenderCopyEx = (WinRenderCopyEx, LinuxRenderCopyEx) |> toNative
        let RenderReadPixels = (WinRenderReadPixels, LinuxRenderReadPixels) |> toNative
        let GLBindTexture = (WinGLBindTexture, LinuxGLBindTexture) |> toNative
        let GLUnbindTexture = (WinGLUnbindTexture, LinuxGLUnbindTexture) |> toNative

    type Renderer(ptr:IntPtr, destroyFunc: IntPtr->unit) = 
        inherit SDL.Utility.Pointer(ptr, destroyFunc)
        new (ptr:IntPtr) = new Renderer(ptr, Native.DestroyRenderer)
        static member Invalid = new Renderer(IntPtr.Zero)


    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // Renderer
    /////////////////////////////////////////////////////////////////////////////////////////////////////

    let create (window:SDL.Utility.Pointer) (index:int option) (flags:Flags) :Renderer =
        let ptr = Native.CreateRenderer(window.Pointer, (if index.IsSome then index.Value else -1), flags |> uint32)
        new Renderer(ptr)

    let createSoftware (surface:Surface) :Renderer =
        let ptr = Native.CreateSoftwareRenderer(surface.Pointer)
        new Renderer(ptr)

    let clear (renderer:Renderer) :bool =
        0 = Native.RenderClear(renderer.Pointer)

    let present (renderer:Renderer) :unit =
        Native.RenderPresent(renderer.Pointer)

    let setDrawColor (color:Pixel.Color) (renderer:Renderer) :bool =
        0 = Native.SetRenderDrawColor(renderer.Pointer,color.Red,color.Green,color.Blue,color.Alpha)

    let getDrawColor (renderer:Renderer) :Pixel.Color option = 
        let mutable r = 0uy
        let mutable g = 0uy
        let mutable b = 0uy
        let mutable a = 0uy
        let pr = &&r
        let pg = &&g
        let pb = &&b
        let pa = &&a

        let result = 
            Native.GetRenderDrawColor(renderer.Pointer, pr, pg, pb, pa)

        if result=0 then
            Some {Pixel.Color.Red = r; Green = g; Blue=b; Alpha=a}
        else
            None

    let setLogicalSize (w:int,h:int) (renderer:Renderer) :bool  =
        0 = Native.RenderSetLogicalSize(renderer.Pointer,w |> int,h |> int)

    let getLogicalSize (renderer:Renderer) : int * int =
        let mutable w = 0
        let mutable h = 0
        let pw = &&w
        let ph = &&h
        Native.RenderGetLogicalSize(renderer.Pointer, pw, ph)
        (w,h)

    let copy 
            (texture:Texture.Texture) 
            (srcrect:Geometry.Rectangle option) 
            (dstrect:Geometry.Rectangle option) 
            (renderer:Renderer) 
            : bool =
        Geometry.withSdlRectPointer
            (fun src -> 
                Geometry.withSdlRectPointer
                    (fun dst -> 
                        0 = Native.RenderCopy(renderer.Pointer,texture.Pointer,src,dst)) 
                    dstrect) 
                srcrect

    let copyEx 
            (texture:Texture.Texture) 
            (srcrect:Geometry.Rectangle option) 
            (dstrect:Geometry.Rectangle option) 
            (angle:float) 
            (center:Geometry.Point option) 
            (flip:Flip) 
            (renderer:Renderer) 
            : bool =
        SDL.Geometry.withSdlRectPointer(
            fun src -> 
                SDL.Geometry.withSdlRectPointer(
                    fun dst -> 
                        if center.IsSome then
                            let mutable pt = Geometry.pointToSdlPoint center.Value
                            0 = Native.RenderCopyEx(renderer.Pointer,texture.Pointer,src,dst, angle, (&&pt) |> NativePtr.toNativeInt , flip |> int)
                        else
                            0 = Native.RenderCopyEx(renderer.Pointer,texture.Pointer,src,dst, angle, IntPtr.Zero, flip |> int)) dstrect) srcrect

    let private nativeRendererInfoToRendererInfo 
            (from:NativeRendererInfo) 
            : RendererInfo =
        let name = 
            from.name 
            |> Utility.intPtrToStringAscii
        let flags = 
            from.flags 
            |> int32 
            |> enum<Flags>
        let formats = //this is totally jank, but gets it done
            [
                from.texture_format0
                from.texture_format1
                from.texture_format2
                from.texture_format3
                from.texture_format4
                from.texture_format5
                from.texture_format6
                from.texture_format7
                from.texture_format8
                from.texture_format9
                from.texture_format10
                from.texture_format11
                from.texture_format12
                from.texture_format13
                from.texture_format14
                from.texture_format15
            ] 
            |> List.take 
                (from.num_texture_formats |> int)
        {
            Name                 = name
            Flags                = flags 
            TextureFormats       = formats
            MaximumTextureWidth  = from.max_texture_width //this is always returning as 0
            MaximumTextureHeight = from.max_texture_height //this is always returning as 0
        }

    let internal getDriverInfo (index:int) : RendererInfo =
        let mutable result = NativeRendererInfo()
        let presult = &&result

        Native.GetRenderDriverInfo (index, presult)
        |> ignore

        result
        |> nativeRendererInfoToRendererInfo

    let getDrivers () : RendererInfo list=
        [0..(Native.GetNumRenderDrivers()-1)]
        |> List.map getDriverInfo

    let getInfo (renderer:Renderer) : RendererInfo option =
        let mutable info = NativeRendererInfo()
        let pinfo = &&info
        
        let result = 
            Native.GetRendererInfo (renderer.Pointer, pinfo)

        if result=0 then
            Some (info |> nativeRendererInfoToRendererInfo)
        else
            None

    let getOutputSize (renderer:Renderer) : (int * int) option =
        let mutable w = 0
        let mutable h = 0
        let pw = &&w
        let ph = &&h


        let result = 
            Native.GetRendererOutputSize(renderer.Pointer, pw, ph)

        if result = 0 then
            Some (w,h)
        else
            None

    let supportsRenderTarget (renderer:Renderer) : bool = 
        Native.RenderTargetSupported(renderer.Pointer) <> 0

    let setRenderTarget (texture:Texture.Texture option) (renderer:Renderer) : bool =
        Native.SetRenderTarget(renderer.Pointer,if texture.IsSome then texture.Value.Pointer else IntPtr.Zero) = 0

    let getRenderTarget (renderer:Renderer) : Texture.Texture option =
        let ptr = 
            Native.GetRenderTarget(renderer.Pointer)
        if ptr=IntPtr.Zero then
            None
        else
            new Texture.Texture(ptr, ignore) |> Some

    let setViewPort (rect:Geometry.Rectangle option) (renderer:Renderer) : bool =
        if rect.IsSome then
            let mutable r = Geometry.rectangleToSdlRect rect.Value
            let pr = &&r
            Native.RenderSetViewport(renderer.Pointer,pr) = 0
        else
            Native.RenderSetViewport(renderer.Pointer,IntPtr.Zero |> NativePtr.ofNativeInt<Geometry.SdlRect>) = 0

    let getViewPort (renderer:Renderer) : Geometry.Rectangle =
        let mutable r = Geometry.SdlRect()
        let pr = &&r

        Native.RenderGetViewport(renderer.Pointer, pr)

        r
        |> Geometry.sdlRectToRectangle

    let setClip (rect:Geometry.Rectangle option) (renderer:Renderer) : bool =
        if rect.IsSome then
            let mutable r = Geometry.rectangleToSdlRect rect.Value
            let pr = &&r

            Native.RenderSetClipRect(renderer.Pointer, pr ) = 0
        else    
            Native.RenderSetClipRect(renderer.Pointer, IntPtr.Zero |> NativePtr.ofNativeInt<Geometry.SdlRect> ) = 0

    let getClip (renderer:Renderer) : Geometry.Rectangle option =
        let mutable r = Geometry.SdlRect()
        let pr = &&r

        Native.RenderGetClipRect(renderer.Pointer, pr)

        let result = Geometry.sdlRectToRectangle r |> Some

        if Geometry.Rectangle.isEmpty result then
            None
        else
            result

    let isClipping (renderer:Renderer) : bool =
        Native.RenderIsClipEnabled(renderer.Pointer) = 0

    let setScale (scaleX:float,scaleY:float) (renderer:Renderer) : bool =
        Native.RenderSetScale(renderer.Pointer, scaleX, scaleY) = 0

    let getScale (renderer:Renderer) : float * float =
        let mutable x = 0.0
        let mutable y = 0.0
        let px = &&x
        let py = &&y

        Native.RenderGetScale(renderer.Pointer, px, py)

        (x,y)

    let drawPoint (point:Geometry.Point) (renderer:Renderer) : bool =
        Native.RenderDrawPoint(renderer.Pointer,point.X,point.Y) = 0

    let private drawThings<'Thing,'NativeThing when 'NativeThing:unmanaged> (thingSize:uint32) (convertor:'Thing->'NativeThing) (renderFunction: (IntPtr * IntPtr * int)->int) (things:'Thing list) (renderer:Renderer) : bool =
        let ptr = Utility.Native.SdlCalloc(things.Length |> uint32, thingSize)

        let np = ptr |> NativePtr.ofNativeInt<'NativeThing>

        things
        |> List.fold
            (fun p pt -> 
                NativePtr.write p (pt |> convertor)
                NativePtr.add p 1) np
        |> ignore

        let result = 
            renderFunction(renderer.Pointer, ptr, things.Length) = 0

        Utility.Native.SdlFree(ptr)

        result

    let drawPoints (points:Geometry.Point list) (renderer:Renderer) : bool = 
        drawThings<Geometry.Point, Geometry.SdlPoint> 8u Geometry.pointToSdlPoint Native.RenderDrawPoints points renderer

    let drawLine (first:Geometry.Point) (second:Geometry.Point) (renderer:Renderer) : bool =
        Native.RenderDrawLine(renderer.Pointer, first.X, first.Y, second.X, second.Y) = 0

    let drawLines (points:Geometry.Point list) (renderer:Renderer) : bool = 
        drawThings<Geometry.Point, Geometry.SdlPoint> 8u Geometry.pointToSdlPoint Native.RenderDrawLines points renderer

    let drawRectangle (rect:Geometry.Rectangle option) (renderer:Renderer) : bool =
        if rect.IsSome then
            let mutable r = Geometry.rectangleToSdlRect rect.Value
            let pr = &&r
            Native.RenderDrawRect(renderer.Pointer, pr) = 0
        else    
            Native.RenderDrawRect(renderer.Pointer, IntPtr.Zero |> NativePtr.ofNativeInt<Geometry.SdlRect>) = 0

    let drawRectangles (points:Geometry.Rectangle list) (renderer:Renderer) : bool = 
        drawThings<Geometry.Rectangle, Geometry.SdlRect> 16u Geometry.rectangleToSdlRect Native.RenderDrawRects points renderer

    let fillRectangle (rect:Geometry.Rectangle option) (renderer:Renderer) : bool =
        if rect.IsSome then
            let mutable r = Geometry.rectangleToSdlRect rect.Value
            let pr = &&r
            Native.RenderFillRect(renderer.Pointer, pr) = 0
        else    
            Native.RenderFillRect(renderer.Pointer, IntPtr.Zero |> NativePtr.ofNativeInt<Geometry.SdlRect>) = 0

    let fillRectangles (points:Geometry.Rectangle list) (renderer:Renderer) : bool = 
        drawThings<Geometry.Rectangle, Geometry.SdlRect> 16u Geometry.rectangleToSdlRect Native.RenderFillRects points renderer

    //SDL_GL_BindTexture
    //SDL_GL_UnbindTexture

    let setDrawBlendMode (blendMode:Texture.BlendMode) (renderer:Renderer) : bool =
        Native.SetRenderDrawBlendMode(renderer.Pointer, blendMode |> int) = 0

    let getDrawBlendMode (renderer: Renderer) : Texture.BlendMode option =
        let mutable blendMode: int = 0
        let pblendMode = &&blendMode
        let result = Native.GetRenderDrawBlendMode(renderer.Pointer, pblendMode)
        if result = 0 then
            Some (blendMode |> enum<Texture.BlendMode>)
        else
            None

    //SDL_RenderReadPixels
