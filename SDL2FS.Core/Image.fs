namespace SDL

open System.Runtime.InteropServices
open System
open Microsoft.FSharp.NativeInterop

module Image =

    [<Flags>]
    type InitFlags =
        | JPG = 1
        | PNG = 2
        | TIF = 4
        | WEBP = 8

    module private Native = 
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_Init(uint32 flags)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern void IMG_Quit()
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadTyped_RW(IntPtr src, int freesrc, IntPtr type)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_Load(IntPtr file)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_Load_RW(IntPtr src, int freesrc)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadTexture(IntPtr renderer, IntPtr file)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadTexture_RW(IntPtr renderer, IntPtr src, int freesrc)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadTextureTyped_RW(IntPtr renderer, IntPtr src, int freesrc, IntPtr type)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isICO(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isCUR(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isBMP(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isGIF(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isJPG(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isLBM(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isPCX(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isPNG(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isPNM(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isSVG(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isTIF(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isXCF(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isXPM(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isXV(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_isWEBP(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadICO_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadCUR_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadBMP_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadGIF_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadJPG_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadLBM_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadPCX_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadPNG_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadPNM_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadSVG_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadTIF_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadXCF_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadXPM_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadXV_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_LoadWEBP_RW(IntPtr src)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern IntPtr IMG_ReadXPMFromArray(IntPtr xpm)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_SavePNG(IntPtr surface, IntPtr file)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_SavePNG_RW(IntPtr surface, IntPtr dst, int freedst)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_SaveJPG(IntPtr surface, IntPtr file, int quality)
        [<DllImport(NativeLibNameImage, CallingConvention = CallingConvention.Cdecl)>]
        extern int IMG_SaveJPG_RW(IntPtr surface, IntPtr dst, int freedst, int quality)
