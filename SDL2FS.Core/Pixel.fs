﻿namespace SDL

open System.Runtime.InteropServices
open System
open SDL
open Microsoft.FSharp.NativeInterop

#nowarn "9" "51"

module Pixel = 

    type PixelType =
        | Unknown  = 0
        | Index1   = 1
        | Index4   = 2
        | Index8   = 3
        | Packed8  = 4
        | Packed16 = 5
        | Packed32 = 6
        | ArrayU8  = 7
        | ArrayU16 = 8
        | ArrayU32 = 9
        | ArrayF16 = 10
        | ArrayF32 = 11

    type BitmapOrder =
        | BitmapOrderNone = 0
        | BitmapOrder4321 = 1
        | BitmapOrder1234 = 2

    type PackedOrder =
        | PackedNone = 0
        | XRGB       = 1
        | RGBX       = 2
        | ARGB       = 3
        | RGBA       = 4
        | XBGR       = 5
        | BGRX       = 6
        | ABGR       = 7
        | BGRA       = 8

    type ArrayOrder =
        | ArrayOrderNone = 0
        | RGB            = 1
        | RGBA           = 2
        | ARGB=3
        | BGR=4
        | BGRA=5
        | ABGR=6

    type PackedLayout =
        | PackedLayoutNone=0
        | PackedLayout332=1
        | PackedLayout4444=2
        | PackedLayout1555=3
        | PackedLayout5551=4
        | PackedLayout565=5
        | PackedLayout8888=6
        | PackedLayout2101010=7
        | PackedLayout1010102=9

    let private definePixelFormat (typ, order, layout, bits, bytes) =
        ((1 <<< 28) ||| ((typ) <<< 24) ||| ((order) <<< 20) ||| ((layout) <<< 16) ||| ((bits) <<< 8) ||| ((bytes) <<< 0)) |> uint32


    let UnknownFormat     = 0u
    let Index1LSBFormat   = definePixelFormat(PixelType.Index1   |> int, BitmapOrder.BitmapOrder4321  |> int, 0                                |> int,  1, 0)
    let Index1MSBFormat   = definePixelFormat(PixelType.Index1   |> int, BitmapOrder.BitmapOrder1234  |> int, 0                                |> int,  1, 0)
    let Index4LSBFormat   = definePixelFormat(PixelType.Index4   |> int, BitmapOrder.BitmapOrder4321  |> int, 0                                |> int,  4, 0)
    let Index4MSBFormat   = definePixelFormat(PixelType.Index4   |> int, BitmapOrder.BitmapOrder1234  |> int, 0                                |> int,  4, 0)
    let Index8Format      = definePixelFormat(PixelType.Index8   |> int, 0                            |> int, 0                                |> int,  8, 1)
    let RGB332Format      = definePixelFormat(PixelType.Packed8  |> int, PackedOrder.XRGB             |> int, PackedLayout.PackedLayout332     |> int,  8, 1)
    let RGB444Format      = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.XRGB             |> int, PackedLayout.PackedLayout4444    |> int, 12, 2)
    let RGB555Format      = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.XRGB             |> int, PackedLayout.PackedLayout1555    |> int, 15, 2)
    let BGR555Format      = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.XBGR             |> int, PackedLayout.PackedLayout1555    |> int, 15, 2)
    let ARGB4444Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.ARGB             |> int, PackedLayout.PackedLayout4444    |> int, 16, 2)
    let RGBA4444Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.RGBA             |> int, PackedLayout.PackedLayout4444    |> int, 16, 2)
    let ABGR4444Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.ABGR             |> int, PackedLayout.PackedLayout4444    |> int, 16, 2)
    let BGRA4444Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.BGRA             |> int, PackedLayout.PackedLayout4444    |> int, 16, 2)
    let ARGB1555Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.ARGB             |> int, PackedLayout.PackedLayout1555    |> int, 16, 2)
    let RGBA5551Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.RGBA             |> int, PackedLayout.PackedLayout5551    |> int, 16, 2)
    let ABGR1555Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.ABGR             |> int, PackedLayout.PackedLayout1555    |> int, 16, 2)
    let BGRA5551Format    = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.BGRA             |> int, PackedLayout.PackedLayout5551    |> int, 16, 2)
    let RGB565Format      = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.XRGB             |> int, PackedLayout.PackedLayout565     |> int, 16, 2)
    let BGR565Format      = definePixelFormat(PixelType.Packed16 |> int, PackedOrder.XBGR             |> int, PackedLayout.PackedLayout565     |> int, 16, 2)
    let RGB24Format       = definePixelFormat(PixelType.ArrayU8  |> int, ArrayOrder.RGB               |> int, 0                                |> int, 24, 3)
    let BGR24Format       = definePixelFormat(PixelType.ArrayU8  |> int, ArrayOrder.BGR               |> int, 0                                |> int, 24, 3)
    let RGB888Format      = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.XRGB             |> int, PackedLayout.PackedLayout8888    |> int, 24, 4)
    let RGBX8888Format    = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.RGBX             |> int, PackedLayout.PackedLayout8888    |> int, 24, 4)
    let BGR888Format      = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.XBGR             |> int, PackedLayout.PackedLayout8888    |> int, 24, 4)
    let BGRX8888Format    = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.BGRX             |> int, PackedLayout.PackedLayout8888    |> int, 24, 4)
    let ARGB8888Format    = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.ARGB             |> int, PackedLayout.PackedLayout8888    |> int, 32, 4)
    let RGBA8888Format    = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.RGBA             |> int, PackedLayout.PackedLayout8888    |> int, 32, 4)
    let ABGR8888Format    = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.ABGR             |> int, PackedLayout.PackedLayout8888    |> int, 32, 4)
    let BGRA8888Format    = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.BGRA             |> int, PackedLayout.PackedLayout8888    |> int, 32, 4)
    let ARGB2101010Format = definePixelFormat(PixelType.Packed32 |> int, PackedOrder.ARGB             |> int, PackedLayout.PackedLayout2101010 |> int, 32, 4)

    let private formatNameTable = 
        [
        (UnknownFormat     ,"Unknown"    );
        (Index1LSBFormat   ,"Index1LSB"  );
        (Index1MSBFormat   ,"Index1MSB"  );
        (Index4LSBFormat   ,"Index4LSB"  );
        (Index4MSBFormat   ,"Index4MSB"  );
        (Index8Format      ,"Index8"     );
        (RGB332Format      ,"RGB332"     );
        (RGB444Format      ,"RGB444"     );
        (RGB555Format      ,"RGB555"     );
        (BGR555Format      ,"BGR555"     );
        (ARGB4444Format    ,"ARGB4444"   );
        (RGBA4444Format    ,"RGBA4444"   );
        (ABGR4444Format    ,"ABGR4444"   );
        (BGRA4444Format    ,"BGRA4444"   );
        (ARGB1555Format    ,"ARGB1555"   );
        (RGBA5551Format    ,"RGBA5551"   );
        (ABGR1555Format    ,"ABGR1555"   );
        (BGRA5551Format    ,"BGRA5551"   );
        (RGB565Format      ,"RGB565"     );
        (BGR565Format      ,"BGR565"     );
        (RGB24Format       ,"RGB24"      );
        (BGR24Format       ,"BGR24"      );
        (RGB888Format      ,"RGB888"     );
        (RGBX8888Format    ,"RGBX8888"   );
        (BGR888Format      ,"BGR888"     );
        (BGRX8888Format    ,"BGRX8888"   );
        (ARGB8888Format    ,"ARGB8888"   );
        (RGBA8888Format    ,"RGBA8888"   );
        (ABGR8888Format    ,"ABGR8888"   );
        (BGRA8888Format    ,"BGRA8888"   );
        (ARGB2101010Format ,"ARGB2101010")]
        |> Map.ofList

    let getFormatName (format:uint32) : string =
        let value = 
            formatNameTable
            |> Map.tryFind format
        if value.IsSome then
            value.Value
        else
            String.Empty


    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlColor =
        struct
            val mutable r: uint8
            val mutable g: uint8
            val mutable b: uint8
            val mutable a: uint8
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlPalette =
        struct
            val ncolors: int
            val colors: IntPtr
            val version: uint32
            val refcount: int
        end


    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlPixelFormat =
        struct
            val format: uint32
            val palette: IntPtr//SDL_Palette*
            val BitsPerPixel: uint8
            val BytesPerPixel: uint8
            val padding: uint16
            val Rmask: uint32
            val Gmask: uint32
            val Bmask: uint32
            val Amask: uint32
            val Rloss: uint8
            val Gloss: uint8
            val Bloss: uint8
            val Aloss: uint8
            val Rshift: uint8
            val Gshift: uint8
            val Bshift: uint8
            val Ashift: uint8
            val refcount: int
            val next: IntPtr;//SDL_PixelFormat*
        end

    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetPixelFormatName")>]
        extern IntPtr WinSdlGetPixelFormatName(uint32 formatEnum)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PixelFormatEnumToMasks")>]
        extern int WinSdlPixelFormatEnumToMasks(uint32 formatEnum,int* bpp,uint32* Rmask,uint32* Gmask,uint32* Bmask,uint32* Amask)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MasksToPixelFormatEnum")>]
        extern uint32 WinSdlMasksToPixelFormatEnum(int bpp,uint32 Rmask,uint32 Gmask,uint32 Bmask,uint32 Amask)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_AllocFormat")>]
        extern IntPtr WinSdlAllocFormat(uint32 formatEnum)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreeFormat")>]
        extern void WinSdlFreeFormat(IntPtr format)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_AllocPalette")>]
        extern IntPtr WinSdlAllocPalette(int ncolors)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreePalette")>]
        extern void WinSdlFreePalette(IntPtr palette)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetPixelFormatPalette")>]
        extern int WinSdlSetPixelFormatPalette(IntPtr format,IntPtr palette)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetPaletteColors")>]
        extern int WinSdlSetPaletteColors(IntPtr palette, SdlColor* colors,int firstcolor, int ncolors)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MapRGB")>]
        extern uint32 WinSdlMapRGB(IntPtr format,uint8 r, uint8 g, uint8 b)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MapRGBA")>]
        extern uint32 WinSdlMapRGBA(IntPtr format,uint8 r, uint8 g, uint8 b,uint8 a)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRGB")>]
        extern void WinSdlGetRGB(uint32 pixel,IntPtr format,uint8* r, uint8* g, uint8* b)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRGBA")>]
        extern void WinSdlGetRGBA(uint32 pixel,IntPtr format,uint8* r, uint8* g, uint8* b,uint8* a)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CalculateGammaRamp")>]
        extern void WinSdlCalculateGammaRamp(float gamma, IntPtr ramp)

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetPixelFormatName")>]
        extern IntPtr LinuxSdlGetPixelFormatName(uint32 formatEnum)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PixelFormatEnumToMasks")>]
        extern int LinuxSdlPixelFormatEnumToMasks(uint32 formatEnum,int* bpp,uint32* Rmask,uint32* Gmask,uint32* Bmask,uint32* Amask)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MasksToPixelFormatEnum")>]
        extern uint32 LinuxSdlMasksToPixelFormatEnum(int bpp,uint32 Rmask,uint32 Gmask,uint32 Bmask,uint32 Amask)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_AllocFormat")>]
        extern IntPtr LinuxSdlAllocFormat(uint32 formatEnum)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreeFormat")>]
        extern void LinuxSdlFreeFormat(IntPtr format)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_AllocPalette")>]
        extern IntPtr LinuxSdlAllocPalette(int ncolors)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FreePalette")>]
        extern void LinuxSdlFreePalette(IntPtr palette)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetPixelFormatPalette")>]
        extern int LinuxSdlSetPixelFormatPalette(IntPtr format,IntPtr palette)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetPaletteColors")>]
        extern int LinuxSdlSetPaletteColors(IntPtr palette, SdlColor* colors,int firstcolor, int ncolors)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MapRGB")>]
        extern uint32 LinuxSdlMapRGB(IntPtr format,uint8 r, uint8 g, uint8 b)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_MapRGBA")>]
        extern uint32 LinuxSdlMapRGBA(IntPtr format,uint8 r, uint8 g, uint8 b,uint8 a)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRGB")>]
        extern void LinuxSdlGetRGB(uint32 pixel,IntPtr format,uint8* r, uint8* g, uint8* b)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRGBA")>]
        extern void LinuxSdlGetRGBA(uint32 pixel,IntPtr format,uint8* r, uint8* g, uint8* b,uint8* a)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CalculateGammaRamp")>]
        extern void LinuxSdlCalculateGammaRamp(float gamma, IntPtr ramp)

        let SdlGetPixelFormatName     = (WinSdlGetPixelFormatName     , LinuxSdlGetPixelFormatName    ) |> toNative
        let SdlPixelFormatEnumToMasks = (WinSdlPixelFormatEnumToMasks , LinuxSdlPixelFormatEnumToMasks) |> toNative
        let SdlMasksToPixelFormatEnum = (WinSdlMasksToPixelFormatEnum , LinuxSdlMasksToPixelFormatEnum) |> toNative
        let SdlAllocFormat            = (WinSdlAllocFormat            , LinuxSdlAllocFormat           ) |> toNative
        let SdlFreeFormat             = (WinSdlFreeFormat             , LinuxSdlFreeFormat            ) |> toNative
        let SdlAllocPalette           = (WinSdlAllocPalette           , LinuxSdlAllocPalette          ) |> toNative
        let SdlFreePalette            = (WinSdlFreePalette            , LinuxSdlFreePalette           ) |> toNative
        let SdlSetPixelFormatPalette  = (WinSdlSetPixelFormatPalette  , LinuxSdlSetPixelFormatPalette ) |> toNative
        let SdlSetPaletteColors       = (WinSdlSetPaletteColors       , LinuxSdlSetPaletteColors      ) |> toNative
        let SdlMapRGB                 = (WinSdlMapRGB                 , LinuxSdlMapRGB                ) |> toNative
        let SdlMapRGBA                = (WinSdlMapRGBA                , LinuxSdlMapRGBA               ) |> toNative
        let SdlGetRGB                 = (WinSdlGetRGB                 , LinuxSdlGetRGB                ) |> toNative
        let SdlGetRGBA                = (WinSdlGetRGBA                , LinuxSdlGetRGBA               ) |> toNative
        let SdlCalculateGammaRamp     = (WinSdlCalculateGammaRamp     , LinuxSdlCalculateGammaRamp    ) |> toNative

    type Color = 
        {
            Red: uint8
            Green: uint8
            Blue :uint8
            Alpha: uint8
        }
        member x.SetRed(r:uint8) : Color =
            {x with Red = r}
        member x.SetGreen(g:uint8) : Color =
            {x with Green = g}
        member x.SetBlue(b:uint8) : Color =
            {x with Blue = b}
        member x.SetAlpha(a:uint8) : Color =
            {x with Alpha = a}

    module Color =
        let fromRGBA(r:uint8,g:uint8,b:uint8,a:uint8) : Color =
            {
                Red = r
                Green = g
                Blue = b
                Alpha = a
            }

    type Palette = IntPtr

    type PixelFormat = IntPtr

    type PixelFormatInfo =
        {Format: uint32;
        Palette: Palette;
        BitsPerPixel: int;
        BytesPerPixel: int;
        RMask: uint32;
        GMask: uint32;
        BMask: uint32;
        AMask: uint32}

    let formatEnumName (format:uint32) :string = 
        Native.SdlGetPixelFormatName(format)
        |> SDL.Utility.intPtrToStringUtf8

    let formatEnumToMasks (formatEnum: uint32) : (int*uint32*uint32*uint32*uint32) option =
        let mutable bpp=0
        let mutable rmask=0u
        let mutable gmask=0u
        let mutable bmask=0u
        let mutable amask=0u
        let pbpp = &&bpp
        let prmask = &&rmask
        let pgmask = &&gmask
        let pbmask = &&bmask
        let pamask = &&amask
        if Native.SdlPixelFormatEnumToMasks(formatEnum,pbpp,prmask,pgmask,pbmask,pamask) <> 0 then
            Some (bpp,rmask,gmask,bmask,amask)
        else
            None

    let masksToFormatEnum (bpp:int,rmask:uint32,gmask:uint32,bmask:uint32,amask:uint32) :uint32 =
        Native.SdlMasksToPixelFormatEnum(bpp,rmask,gmask,bmask,amask)

    let alloc (formatEnum: uint32) :PixelFormat =
        Native.SdlAllocFormat formatEnum

    let free format =
        Native.SdlFreeFormat format

    let allocPalette colorCount =
        Native.SdlAllocPalette(colorCount)

    let freePalette palette =
        Native.SdlFreePalette(palette)

    let setPalette palette format =
        0 = Native.SdlSetPixelFormatPalette(format,palette)

    let setPaletteColor index (color:Color) palette =
        let mutable c = SdlColor()
        let pc = &&c
        c.r <- color.Red
        c.g <- color.Green
        c.b <- color.Blue
        c.a <- color.Alpha
        0 = Native.SdlSetPaletteColors(palette,pc,index,1)

    let getPaletteColorCount (palette:IntPtr) : int=
        if palette=IntPtr.Zero then
            0
        else
            let pal = 
                palette
                |> NativePtr.ofNativeInt<SdlPalette>
                |> NativePtr.read
            pal.ncolors

    let getPaletteColor (index:int) palette :Color option=
        if palette = IntPtr.Zero || index<0 || index>= (palette |> getPaletteColorCount) then
            None
        else
            let pal = 
                palette
                |> NativePtr.ofNativeInt<SdlPalette>
                |> NativePtr.read
            let colors :nativeptr<SdlColor> =
                pal.colors
                |> NativePtr.ofNativeInt<SdlColor>
            let color = 
                NativePtr.add colors index
                |> NativePtr.read
            Some {Red=color.r;Green=color.g;Blue=color.b;Alpha=color.a}
    
    let mapColor (format:PixelFormat) (color: Color) :uint32 = 
        Native.SdlMapRGBA(format,color.Red,color.Green,color.Blue,color.Alpha)

    let getColor (format:PixelFormat) (value:uint32) :Color =
        let mutable r=0uy
        let mutable g=0uy
        let mutable b=0uy
        let mutable a=0uy
        let pr = &&r
        let pg = &&g
        let pb = &&b
        let pa = &&a
        Native.SdlGetRGBA(value,format,pr,pg,pb,pa)
        {Red=r;Green=g;Blue=b;Alpha=a}