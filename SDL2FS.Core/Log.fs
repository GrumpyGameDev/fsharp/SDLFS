﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System

module Log =

    let CategoryApplication =  0
    let CategoryError       =  1
    let CategoryAssert      =  2
    let CategorySystem      =  3
    let CategoryAudio       =  4
    let CategoryVideo       =  5
    let CategoryRender      =  6
    let CategoryInput       =  7
    let CategoryTest        =  8
    let CategoryReserved1   =  9
    let CategoryReserved2   = 10
    let CategoryReserved3   = 11
    let CategoryReserved4   = 12
    let CategoryReserved5   = 13
    let CategoryReserved6   = 14
    let CategoryReserved7   = 15
    let CategoryReserved8   = 16
    let CategoryReserved9   = 17
    let CategoryReserved10  = 18
    let CategoryCustom      = 19

    type Priority = 
        | Verbose  = 1
        | Debug    = 2
        | Info     = 3
        | Warn     = 4
        | Error    = 5
        | Critical = 6

    type OutputFunction = delegate of nativeint * nativeint * nativeint * nativeint -> unit

    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Log")>]
        extern void WinSdlLog([<MarshalAs(UnmanagedType.LPStr)>]string fmt)    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogGetPriority")>]
        extern Priority WinSdlLogGetPriority(int category);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogSetAllPriority")>]
        extern void WinSdlLogSetAllPriority(Priority priority);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogSetPriority")>]
        extern void WinSdlLogSetPriority(int category, Priority priority);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogResetPriorities")>]
        extern void WinSdlLogResetPriorities();
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogVerbose")>]
        extern void WinSdlLogVerbose(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogDebug")>]
        extern void WinSdlLogDebug(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogInfo")>]
        extern void WinSdlLogInfo(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogWarn")>]
        extern void WinSdlLogWarn(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogError")>]
        extern void WinSdlLogError(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogCritical")>]
        extern void WinSdlLogCritical(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogMessage")>]
        extern void WinSdlLogMessage(int category, Priority priority,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogGetOutputFunction")>]
        extern void WinSdlLogGetOutputFunction(OutputFunction& callback, IntPtr* userdata);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogSetOutputFunction")>]
        extern void WinSdlLogSetOutputFunction(OutputFunction callback, IntPtr userdata);

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Log")>]
        extern void LinuxSdlLog([<MarshalAs(UnmanagedType.LPStr)>]string fmt)    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogGetPriority")>]
        extern Priority LinuxSdlLogGetPriority(int category);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogSetAllPriority")>]
        extern void LinuxSdlLogSetAllPriority(Priority priority);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogSetPriority")>]
        extern void LinuxSdlLogSetPriority(int category, Priority priority);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogResetPriorities")>]
        extern void LinuxSdlLogResetPriorities();
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogVerbose")>]
        extern void LinuxSdlLogVerbose(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogDebug")>]
        extern void LinuxSdlLogDebug(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogInfo")>]
        extern void LinuxSdlLogInfo(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogWarn")>]
        extern void LinuxSdlLogWarn(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogError")>]
        extern void LinuxSdlLogError(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogCritical")>]
        extern void LinuxSdlLogCritical(int category,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogMessage")>]
        extern void LinuxSdlLogMessage(int category, Priority priority,  [<MarshalAs(UnmanagedType.LPStr)>]string fmt);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogGetOutputFunction")>]
        extern void LinuxSdlLogGetOutputFunction(OutputFunction& callback, IntPtr* userdata);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_LogSetOutputFunction")>]
        extern void LinuxSdlLogSetOutputFunction(OutputFunction callback, IntPtr userdata);

        let SdlLog                 = (WinSdlLog                  , LinuxSdlLog                 ) |> toNative
        let SdlLogGetPriority      = (WinSdlLogGetPriority       , LinuxSdlLogGetPriority      ) |> toNative
        let SdlLogSetAllPriority   = (WinSdlLogSetAllPriority    , LinuxSdlLogSetAllPriority   ) |> toNative
        let SdlLogSetPriority      = (WinSdlLogSetPriority       , LinuxSdlLogSetPriority      ) |> toNative
        let SdlLogResetPriorities  = (WinSdlLogResetPriorities   , LinuxSdlLogResetPriorities  ) |> toNative
        let SdlLogVerbose          = (WinSdlLogVerbose           , LinuxSdlLogVerbose          ) |> toNative
        let SdlLogDebug            = (WinSdlLogDebug             , LinuxSdlLogDebug            ) |> toNative
        let SdlLogInfo             = (WinSdlLogInfo              , LinuxSdlLogInfo             ) |> toNative
        let SdlLogWarn             = (WinSdlLogWarn              , LinuxSdlLogWarn             ) |> toNative
        let SdlLogError            = (WinSdlLogError             , LinuxSdlLogError            ) |> toNative
        let SdlLogCritical         = (WinSdlLogCritical          , LinuxSdlLogCritical         ) |> toNative
        let SdlLogMessage          = (WinSdlLogMessage           , LinuxSdlLogMessage          ) |> toNative
        //let SdlLogGetOutputFunction= (WinSdlLogGetOutputFunction , LinuxSdlLogGetOutputFunction) |> toNative
        let SdlLogSetOutputFunction= (WinSdlLogSetOutputFunction , LinuxSdlLogSetOutputFunction) |> toNative

    let setOutputFunction (callback:OutputFunction) :unit =
        Native.SdlLogSetOutputFunction(callback, IntPtr.Zero)

    // let getOutputFunction () :OutputFunction =
    //     let mutable callback:OutputFunction = OutputFunction(fun a b c d -> ())
    //     let mutable ptr:IntPtr = IntPtr.Zero
    //     let pcallback = &callback
    //     let pptr = &&ptr
    //     Native.SdlLogGetOutputFunction(pcallback,pptr)
    //     callback

    let log (message:string) :unit =
        Native.SdlLog message

    let getPriority (category:int) :Priority =
        Native.SdlLogGetPriority (category)

    let setAllPriority (priority:Priority) :unit =
        Native.SdlLogSetAllPriority(priority)

    let setPriority (priority:Priority) (category:int) :unit =
        Native.SdlLogSetPriority (category, priority)

    let resetPriorities () =
        Native.SdlLogResetPriorities()

    //keep log functions from exploding!
    let private escapeLogString (message:string) : string =
        message.Replace("%","%%")

    let verbose (category:int) (message:string) :unit =
        Native.SdlLogVerbose (category, message |> escapeLogString)

    let debug (category:int) (message:string) :unit =
        Native.SdlLogDebug (category, message |> escapeLogString)

    let error (category:int) (message:string) :unit =
        Native.SdlLogError (category, message |> escapeLogString)

    let info (category:int) (message:string) :unit =
        Native.SdlLogInfo (category, message |> escapeLogString)

    let warn (category:int) (message:string) :unit =
        Native.SdlLogWarn (category, message |> escapeLogString)

    let critical (category:int) (message:string) :unit =
        Native.SdlLogCritical (category, message |> escapeLogString)

    let message (priority:Priority) (category:int) (message:string) :unit =
        Native.SdlLogMessage (category, priority, message |> escapeLogString)

