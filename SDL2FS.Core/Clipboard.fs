﻿namespace SDL

open System.Runtime.InteropServices
open System

module Clipboard =
        
    module private Native =
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetClipboardText")>]
        extern int WinSdlSetClipboardText(IntPtr text);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetClipboardText")>]
        extern IntPtr WinSdlGetClipboardText();
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasClipboardText")>]
        extern int WinSdlHasClipboardText();

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetClipboardText")>]
        extern int LinuxSdlSetClipboardText(IntPtr text);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetClipboardText")>]
        extern IntPtr LinuxSdlGetClipboardText();
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasClipboardText")>]
        extern int LinuxSdlHasClipboardText();

        let SdlSetClipboardText = (WinSdlSetClipboardText , LinuxSdlSetClipboardText) |> toNative
        let SdlGetClipboardText = (WinSdlGetClipboardText , LinuxSdlGetClipboardText) |> toNative
        let SdlHasClipboardText = (WinSdlHasClipboardText , LinuxSdlHasClipboardText) |> toNative

    let setText (text:string) :bool =
        text
        |> Utility.withUtf8String(Native.SdlSetClipboardText)
        <> 0

    let getText () :string =
        let ptr = Native.SdlGetClipboardText()
        let text = 
            ptr
            |> Utility.intPtrToStringUtf8
        if ptr<>IntPtr.Zero then
            Utility.Native.SdlFree(ptr)
        else
            ()
        text

    let hasText () :bool =
        Native.SdlHasClipboardText()
        <> 0