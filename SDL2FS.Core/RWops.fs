﻿namespace SDL

open System
open System.Runtime.InteropServices

#nowarn "9"

module RWops = 
    type RWops = IntPtr

    module internal Native =
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi, EntryPoint="SDL_RWFromFile")>]
        extern RWops WinSdlRWFromFile(IntPtr file, string mode)
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi, EntryPoint="SDL_RWFromFile")>]
        extern RWops LinuxSdlRWFromFile(IntPtr file, string mode)
    
        let SdlRWFromFile = (WinSdlRWFromFile, LinuxSdlRWFromFile) |> toNative
    

