﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System
open SDL
open SDL.GameController

module Event =

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlQuitEvent =
        struct
            val Type: uint32
            val Timestamp: uint32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlKeysym = 
        struct
            val Scancode: int32
            val Sym: int32
            val Mod: uint16
            val Unused: uint32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlCommonEvent =
        struct
            val Type: uint32
            val Timestamp: uint32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlWindowEvent =
        struct
            val Type: uint32
            val Timestamp: uint32
            val WindowID: uint32
            val Event: uint8     
            val Padding1: uint8
            val Padding2: uint8
            val Padding3: uint8
            val Data1: int32  
            val Data2: int32   
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlKeyboardEvent =
        struct
            val Type: uint32
            val Timestamp: uint32
            val WindowID: uint32
            val State: uint8
            val Repeat: uint8
            val Padding2: uint8
            val Padding3: uint8
            val Keysym: SdlKeysym
        end

    [<StructLayout(LayoutKind.Explicit, Size=52)>]
    type internal SdlTextEditingEvent =
        struct
            [<FieldOffset(0)>]
            val Type: uint32
            [<FieldOffset(4)>]
            val Timestamp: uint32
            [<FieldOffset(8)>]
            val WindowID: uint32
            [<FieldOffset(12)>]
            val Text: byte//really a byte[32]
            [<FieldOffset(44)>]
            val Start: int32
            [<FieldOffset(48)>]
            val Length: int32
        end

    [<StructLayout(LayoutKind.Explicit, Size=44)>]
    type internal SdlTextInputEvent =
        struct
            [<FieldOffset(0)>]
            val Type: uint32
            [<FieldOffset(4)>]
            val Timestamp: uint32
            [<FieldOffset(8)>]
            val WindowID: uint32
            [<FieldOffset(12)>]
            val Text: byte//really a byte[32]
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlMouseMotionEvent =
        struct
            val Type: uint32
            val Timestamp: uint32
            val WindowID: uint32
            val Which: uint32
            val State: uint32
            val X: int32
            val Y: int32
            val Xrel: int32
            val Yrel: int32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlMouseButtonEvent =
        struct
            val Type: uint32
            val Timestamp: uint32
            val WindowID: uint32
            val Which: uint32
            val Button: uint8
            val State: uint8
            val Clicks: uint8
            val Padding1: uint8
            val X: int32
            val Y: int32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlMouseWheelEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val WindowID:uint32
            val Which:uint32
            val X:int32
            val Y:int32
            val Direction:uint32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlJoyAxisEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
            val Axis:uint8
            val Padding1:uint8
            val Padding2:uint8
            val Padding3:uint8
            val Value:int16
            val Padding4:uint16
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlJoyBallEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
            val Ball:uint8
            val Padding1:uint8
            val Padding2:uint8
            val Padding3:uint8
            val Xrel:int16
            val Yrel:int16
        end 

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlJoyHatEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
            val Hat:uint8
            val Value:uint8
            val Padding1:uint8
            val Padding2:uint8
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlJoyButtonEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
            val Button:uint8
            val State:uint8
            val Padding1:uint8
            val Padding2:uint8
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlJoyDeviceEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlControllerAxisEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
            val Axis:uint8
            val Padding1:uint8
            val Padding2:uint8
            val Padding3:uint8
            val Value:int16
            val Padding4:uint16
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlControllerButtonEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
            val Button:uint8
            val State:uint8
            val Padding1:uint8
            val Padding2:uint8
        end 

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlControllerDeviceEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:int32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlAudioDeviceEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Which:uint32
            val Iscapture:uint8
            val Padding1:uint8
            val Padding2:uint8
            val Padding3:uint8
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlTouchFingerEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val TouchId:int64
            val FingerId:int64
            val X:float
            val Y:float
            val Dx:float
            val Dy:float
            val Pressure:float
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlMultiGestureEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val TouchId:int64
            val DTheta:float
            val DDist:float
            val X:float
            val Y:float
            val NumFingers:uint16
            val Padding:uint16
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlDollarGestureEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val TouchId:int64
            val GestureId:int64
            val NumFingers:uint32
            val Error:float
            val X:float
            val Y:float
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlDropEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val File:IntPtr
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlOSEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlUserEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val WindowID:uint32
            val Code:int32
            val Data1:IntPtr
            val Data2:IntPtr
        end

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlSysWMEvent=
        struct
            val Type:uint32
            val Timestamp:uint32
            val Msg:IntPtr
        end
    
    [<StructLayout(LayoutKind.Explicit, Size=56)>]
    type internal SdlEvent =
        struct
            [<FieldOffset(0)>]
            val Type: uint32
            [<FieldOffset(0)>]
            val Common:SdlCommonEvent
            [<FieldOffset(0)>]
            val Window:SdlWindowEvent
            [<FieldOffset(0)>]
            val Key:SdlKeyboardEvent
            [<FieldOffset(0)>]
            val Edit:SdlTextEditingEvent
            [<FieldOffset(0)>]
            val Text:SdlTextInputEvent
            [<FieldOffset(0)>]
            val Motion:SdlMouseMotionEvent
            [<FieldOffset(0)>]
            val Button:SdlMouseButtonEvent
            [<FieldOffset(0)>]
            val Wheel:SdlMouseWheelEvent
            [<FieldOffset(0)>]
            val Jaxis:SdlJoyAxisEvent
            [<FieldOffset(0)>]
            val Jball:SdlJoyBallEvent
            [<FieldOffset(0)>]
            val Jhat:SdlJoyHatEvent
            [<FieldOffset(0)>]
            val Jbutton:SdlJoyButtonEvent
            [<FieldOffset(0)>]
            val Jdevice:SdlJoyDeviceEvent
            [<FieldOffset(0)>]
            val Caxis:SdlControllerAxisEvent
            [<FieldOffset(0)>]
            val Cbutton:SdlControllerButtonEvent
            [<FieldOffset(0)>]
            val Cdevice:SdlControllerDeviceEvent
            [<FieldOffset(0)>]
            val Adevice:SdlAudioDeviceEvent
            [<FieldOffset(0)>]
            val Quit:SdlQuitEvent
            [<FieldOffset(0)>]
            val User:SdlUserEvent
            [<FieldOffset(0)>]
            val Syswm:SdlSysWMEvent
            [<FieldOffset(0)>]
            val Tfinger:SdlTouchFingerEvent
            [<FieldOffset(0)>]
            val Mgesture:SdlMultiGestureEvent
            [<FieldOffset(0)>]
            val Dgesture:SdlDollarGestureEvent
            [<FieldOffset(0)>]
            val Drop:SdlDropEvent
        end

    type EventType =
        | Quit                     = 0x100
        | AppTerminating           = 0x101
        | AppLowmemory             = 0x102
        | AppWillEnterBackground   = 0x103
        | AppDidEnterBackground    = 0x104
        | AppWillEnterForeground   = 0x105
        | AppDidEnterForeground    = 0x106
        | WindowEvent              = 0x200
        | SysWMEvent               = 0x201
        | KeyDown                  = 0x300
        | KeyUp                    = 0x301
        | TextEditing              = 0x302
        | TextInput                = 0x303
        | KeyMapChanged            = 0x304
        | MouseMotion              = 0x400
        | MouseButtonDown          = 0x401
        | MouseButtonUp            = 0x402
        | MouseWheel               = 0x403
        | JoyAxisMotion            = 0x600
        | JoyBallMotion            = 0x601
        | JoyHatMotion             = 0x602
        | JoyButtonDown            = 0x603
        | JoyButtonUp              = 0x604
        | JoyDeviceAdded           = 0x605
        | JoyDeviceRemoved         = 0x606
        | ControllerAxisMotion     = 0x650 
        | ControllerButtonDown     = 0x651   
        | ControllerButtonUp       = 0x652
        | ControllerDeviceAdded    = 0x653
        | ControllerDeviceRemoved  = 0x654
        | ControllerDeviceRemapped = 0x655
        | FingerDown               = 0x700
        | FingerUp                 = 0x701
        | FingerMotion             = 0x702
        | DollarGesture            = 0x800
        | DollarRecord             = 0x801
        | MultiGesture             = 0x802
        | ClipboardUpdate          = 0x900 
        | DropFile                 = 0x1000
        | AudioDeviceAdded         = 0x1100
        | AudioDeviceRemoved       = 0x1101      
        | RenderTargetsReset       = 0x2000
        | RenderDeviceReset        = 0x2001
        | UserEvent                = 0x8000
        | LastEvent                = 0xFFFF

    type EventAction =
        | AddEvent=0
        | PeekEvent=1
        | GetEvent=2

    type EventState =
        | Query  = -1
        | Ignore = 0
        | Disable= 0
        | Enable = 1

    type internal SdlEventFilter = delegate of nativeint * nativeptr<SdlEvent> -> int

    module private Native =
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PumpEvents")>]
        extern void WinSdlPumpEvents()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WaitEvent")>]
        extern int WinSdlWaitEvent(SdlEvent* event);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WaitEventTimeout")>]
        extern int WinSdlWaitEventTimeout(SdlEvent* event,int timeout)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PollEvent")>]
        extern int WinSdlPollEvent(SdlEvent* event);
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PeepEvents")>]
        extern int WinSdlPeepEvents(SdlEvent* events, int numevents,int action,uint32 minType, uint32 maxType)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PushEvent")>]
        extern int WinSdlPushEvent(SdlEvent* event)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RegisterEvents")>]
        extern uint32 WinSdlRegisterEvents(int numevents)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasEvent")>]
        extern int WinSdlHasEvent(uint32 eventType)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasEvents")>]
        extern int WinSdlHasEvents(uint32 minType, uint32 maxType)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FlushEvent")>]
        extern void WinSdlFlushEvent(uint32 eventType)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FlushEvents")>]
        extern void WinSdlFlushEvents(uint32 minType, uint32 maxType)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetEventFilter")>]
        extern void WinSdlSetEventFilter(SdlEventFilter filter,nativeint userdata)//how to pass NULL?
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetEventFilter")>]
        extern int WinSdlGetEventFilter(SdlEventFilter& filter,nativeint* userdata)//cant use SDL_EventFilter*, which may make this unusable...
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FilterEvents")>]
        extern void WinSdlFilterEvents(SdlEventFilter filter,nativeint userdata)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_EventState")>]
        extern uint8 WinSdlEventState(uint32 eventType, int state)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_AddEventWatch")>]
        extern void WinSdlAddEventWatch(SdlEventFilter filter,nativeint userdata)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DelEventWatch")>]
        extern void WinSdlDelEventWatch(SdlEventFilter filter,nativeint userdata)
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PumpEvents")>]
        extern void LinuxSdlPumpEvents()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WaitEvent")>]
        extern int LinuxSdlWaitEvent(SdlEvent* event);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WaitEventTimeout")>]
        extern int LinuxSdlWaitEventTimeout(SdlEvent* event,int timeout)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PollEvent")>]
        extern int LinuxSdlPollEvent(SdlEvent* event);
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PeepEvents")>]
        extern int LinuxSdlPeepEvents(SdlEvent* events, int numevents,int action,uint32 minType, uint32 maxType)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_PushEvent")>]
        extern int LinuxSdlPushEvent(SdlEvent* event)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_RegisterEvents")>]
        extern uint32 LinuxSdlRegisterEvents(int numevents)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasEvent")>]
        extern int LinuxSdlHasEvent(uint32 eventType)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasEvents")>]
        extern int LinuxSdlHasEvents(uint32 minType, uint32 maxType)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FlushEvent")>]
        extern void LinuxSdlFlushEvent(uint32 eventType)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FlushEvents")>]
        extern void LinuxSdlFlushEvents(uint32 minType, uint32 maxType)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetEventFilter")>]
        extern void LinuxSdlSetEventFilter(SdlEventFilter filter,nativeint userdata)//how to pass NULL?
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetEventFilter")>]
        extern int LinuxSdlGetEventFilter(SdlEventFilter& filter,nativeint* userdata)//cant use SDL_EventFilter*, which may make this unusable...
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_FilterEvents")>]
        extern void LinuxSdlFilterEvents(SdlEventFilter filter,nativeint userdata)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_EventState")>]
        extern uint8 LinuxSdlEventState(uint32 eventType, int state)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_AddEventWatch")>]
        extern void LinuxSdlAddEventWatch(SdlEventFilter filter,nativeint userdata)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_DelEventWatch")>]
        extern void LinuxSdlDelEventWatch(SdlEventFilter filter,nativeint userdata)
    
        let SdlPumpEvents       = (WinSdlPumpEvents       , LinuxSdlPumpEvents      ) |> toNative
        let SdlWaitEvent        = (WinSdlWaitEvent        , LinuxSdlWaitEvent       ) |> toNative
        let SdlWaitEventTimeout = (WinSdlWaitEventTimeout , LinuxSdlWaitEventTimeout) |> toNative
        let SdlPollEvent        = (WinSdlPollEvent        , LinuxSdlPollEvent       ) |> toNative
        let SdlPeepEvents       = (WinSdlPeepEvents       , LinuxSdlPeepEvents      ) |> toNative
        let SdlPushEvent        = (WinSdlPushEvent        , LinuxSdlPushEvent       ) |> toNative
        let SdlRegisterEvents   = (WinSdlRegisterEvents   , LinuxSdlRegisterEvents  ) |> toNative
        let SdlHasEvent         = (WinSdlHasEvent         , LinuxSdlHasEvent        ) |> toNative
        let SdlHasEvents        = (WinSdlHasEvents        , LinuxSdlHasEvents       ) |> toNative
        let SdlFlushEvent       = (WinSdlFlushEvent       , LinuxSdlFlushEvent      ) |> toNative
        let SdlFlushEvents      = (WinSdlFlushEvents      , LinuxSdlFlushEvents     ) |> toNative
        let SdlSetEventFilter   = (WinSdlSetEventFilter   , LinuxSdlSetEventFilter  ) |> toNative
        //let SdlGetEventFilter   = (WinSdlGetEventFilter   , LinuxSdlGetEventFilter  ) |> toNative
        let SdlFilterEvents     = (WinSdlFilterEvents     , LinuxSdlFilterEvents    ) |> toNative
        let SdlEventState       = (WinSdlEventState       , LinuxSdlEventState      ) |> toNative
        let SdlAddEventWatch    = (WinSdlAddEventWatch    , LinuxSdlAddEventWatch   ) |> toNative
        let SdlDelEventWatch    = (WinSdlDelEventWatch    , LinuxSdlDelEventWatch   ) |> toNative

    type QuitEvent =
        {Timestamp:uint32}

    type Keysym =
        {Scancode: Keyboard.ScanCode;
        Sym: int32;
        Mod: uint16}

    type KeyboardEvent =
        {Timestamp: uint32;
        WindowID: uint32;
        State: uint8;
        Repeat: uint8;
        Keysym: Keysym}

    type MouseMotionEvent =
        {Timestamp: uint32;
        WindowID: uint32;
        Which: uint32;
        State: uint32;
        X: int32;
        Y: int32;
        Xrel: int32;
        Yrel: int32}

    type MouseButtonEvent =
        {Timestamp: uint32;
        WindowID: uint32;
        Which: uint32;
        Button: uint8;
        State: uint8;
        Clicks: uint8;
        X: int32;
        Y: int32}

    type WindowEvent =
        {Type: uint32;
        Timestamp: uint32;
        WindowID: uint32;
        Event: uint8;
        Data1: int32; 
        Data2: int32}

    type TextEditingEvent =
        {Timestamp: uint32;
        WindowID: uint32;                          
        Text: string;
        Start: int32;                              
        Length: int32}

    type TextInputEvent =
        {Timestamp: uint32;
        WindowID: uint32;                            
        Text:string}

    type MouseWheelEvent=
        {Timestamp:uint32;
        WindowID:uint32;
        Which:uint32;
        X:int32;
        Y:int32;
        Direction:SDL.Mouse.WheelDirection}

    type JoyAxisEvent=
        {Timestamp:uint32;
        Which:int32;
        Axis:uint8;
        Value:int16}

    type JoyBallEvent=
        {Timestamp:uint32;
        Which:int32;
        Ball:uint8;
        Xrel:int16;
        Yrel:int16}

    type JoyHatEvent=
        {
        Timestamp:uint32;
        Which:int32     ;
        Hat:uint8       ;
        Value:uint8     ;
        }

    type JoyButtonEvent=
        {
        Timestamp:uint32;
        Which:int32     ;
        Button:uint8    ;
        State:uint8     ;
        }

    type JoyDeviceEvent=
        {
        Timestamp:uint32;
        Which:int32     ;
        }

    type ControllerAxisEvent=
        {Timestamp:uint32;
        Which:int32;
        Axis:Axis;
        Value:int16}

    type ControllerButtonEvent=
        {
        Timestamp:uint32;
        Which:int32     ;
        Button:Button    ;
        State:uint8     ;
        } 

    type ControllerDeviceEvent=
        {
        Timestamp:uint32;
        Which:int32     ;
        }

    type AudioDeviceEvent=
        {Timestamp:uint32;
        Which:uint32;
        Iscapture:uint8}

    type TouchFingerEvent=
        {
        Timestamp:uint32;
        TouchId:int64   ;
        FingerId:int64  ;
        X:float         ;
        Y:float         ;
        Dx:float        ;
        Dy:float        ;
        Pressure:float  ;
        }

    type MultiGestureEvent=
        {
        Timestamp:uint32 ;
        TouchId:int64    ;
        DTheta:float     ;
        DDist:float      ;
        X:float          ;
        Y:float          ;
        NumFingers:uint16;
        Padding:uint16
        }

    type DollarGestureEvent=
        {
        Timestamp:uint32 ;
        TouchId:int64    ;
        GestureId:int64  ;
        NumFingers:uint32;
        Error:float      ;
        X:float          ;
        Y:float          ;
        }

    type DropEvent=
        {
        Timestamp:uint32;
        File:IntPtr     ;
        }

    type OSEvent=
        {
        Timestamp:uint32;
        }

    type UserEvent=
        {
        Type:uint32;
        Timestamp:uint32;
        WindowID:uint32;
        Code:int32;
        Data1:IntPtr;
        Data2:IntPtr;
        }

    type SysWMEvent=
        {
        Type:uint32;
        Timestamp:uint32;
        Msg:IntPtr;
        }


    type Event = 
        | Quit of QuitEvent

        | KeyDown of KeyboardEvent
        | KeyUp of KeyboardEvent

        | MouseMotion of MouseMotionEvent
        | MouseButtonDown of MouseButtonEvent
        | MouseButtonUp of MouseButtonEvent
        | MouseWheel of MouseWheelEvent              

        | AppTerminating//TODO: handle this?
        | AppLowmemory//TODO: handle this?
        | AppWillEnterBackground//TODO: handle this?
        | AppDidEnterBackground//TODO: handle this?
        | AppWillEnterForeground//TODO: handle this?
        | AppDidEnterForeground//TODO: handle this?

        | WindowEvent of WindowEvent       
        | SysWMEvent of SysWMEvent           
        
        | TextEditing of TextEditingEvent 
        | TextInput of TextInputEvent
        | KeyMapChanged  //TODO: handle this?

        | JoyAxisMotion of JoyAxisEvent            
        | JoyBallMotion of JoyBallEvent           
        | JoyHatMotion of JoyHatEvent            
        | JoyButtonDown of JoyButtonEvent           
        | JoyButtonUp of JoyButtonEvent
        | JoyDeviceAdded of JoyDeviceEvent           
        | JoyDeviceRemoved of JoyDeviceEvent      
       
        //toXXXX/isXXXX for Event done through here
        | ControllerAxisMotion of ControllerAxisEvent    
        | ControllerButtonDown of ControllerButtonEvent     
        | ControllerButtonUp of ControllerButtonEvent       
        | ControllerDeviceAdded of ControllerDeviceEvent
        | ControllerDeviceRemoved of ControllerDeviceEvent
        | ControllerDeviceRemapped of ControllerDeviceEvent

        | FingerDown of TouchFingerEvent      
        | FingerUp of TouchFingerEvent
        | FingerMotion of TouchFingerEvent

        | DollarGesture of DollarGestureEvent      
        | DollarRecord of DollarGestureEvent

        | MultiGesture of MultiGestureEvent

        | ClipboardUpdate   //TODO: handle this?
       
        | DropFile of DropEvent                 

        | AudioDeviceAdded of AudioDeviceEvent    
        | AudioDeviceRemoved  of AudioDeviceEvent            

        | RenderTargetsReset //TODO: handle this?
        | RenderDeviceReset //TODO: handle this?

        | User of UserEvent

        | Other of uint32

        member this.IsQuitEvent : bool =
            match this with
            | Quit _ -> true
            | _      -> false

        member this.IsKeyDownEvent : bool =
            match this with
            | KeyDown _ -> true
            | _         -> false

        member this.IsKeyUpEvent : bool =
            match this with
            | KeyUp _ -> true
            | _       -> false

        member this.IsKeyEvent : bool =
            this.IsKeyDownEvent || this.IsKeyUpEvent

        member this.IsMouseMotionEvent : bool =
            match this with
            | MouseMotion _ -> true
            | _         -> false

        member this.IsMouseButtonDownEvent : bool =
            match this with
            | MouseButtonDown _ -> true
            | _         -> false

        member this.IsMouseButtonUpEvent : bool =
            match this with
            | MouseButtonUp _ -> true
            | _         -> false

        member this.IsMouseButtonEvent : bool =
            this.IsMouseButtonDownEvent || this.IsMouseButtonUpEvent

        member this.IsMouseWheelEvent : bool =
            match this with
            | MouseWheel _ -> true
            | _         -> false

        // member this.IsWindowEvent : bool =
        //     match this with
        //     | WindowEvent _ -> true
        //     | _         -> false

        // member this.IsSysWMEvent : bool =
        //     match this with
        //     | SysWMEvent _ -> true
        //     | _         -> false

        member this.IsTextEditingEvent : bool =
            match this with
            | TextEditing _ -> true
            | _         -> false

        member this.IsTextInputEvent : bool =
            match this with
            | TextInput _ -> true
            | _         -> false

        member this.IsJoyAxisEvent : bool =
            match this with
            | JoyAxisMotion _ -> true
            | _         -> false
        member this.IsJoyBallEvent : bool =
            match this with
            | JoyBallMotion _ -> true
            | _         -> false

        member this.IsJoyHatEvent : bool =
            match this with
            | JoyHatMotion _ -> true
            | _         -> false

        member this.IsJoyButtonDownEvent : bool =
            match this with
            | JoyButtonDown _ -> true
            | _         -> false

        member this.IsJoyButtonUpEvent : bool =
            match this with
            | JoyButtonUp _ -> true
            | _         -> false

        member this.IsJoyButtonEvent : bool =
            this.IsJoyButtonDownEvent || this.IsJoyButtonUpEvent

        member this.IsJoyDeviceAddedEvent : bool =
            match this with
            | JoyDeviceAdded _ -> true
            | _         -> false

        member this.IsJoyDeviceRemovedEvent : bool =
            match this with
            | JoyDeviceRemoved _ -> true
            | _         -> false

        member this.IsJoyDeviceEvent : bool =
            this.IsJoyDeviceAddedEvent || this.IsJoyDeviceRemovedEvent

        member this.ToQuitEvent : QuitEvent option =
            match this with
            | Quit q -> Some q
            | _      -> None

        member this.ToKeyboardEvent : KeyboardEvent option =
            match this with
            | KeyUp k   -> Some k
            | KeyDown k -> Some k
            | _         -> None

        member this.ToMouseMotionEvent : MouseMotionEvent option =
            match this with
            | MouseMotion m -> Some m
            | _             -> None

        member this.ToMouseButtonEvent : MouseButtonEvent option =
            match this with
            | MouseButtonDown m -> Some m
            | MouseButtonUp m   -> Some m
            | _                 -> None

        member this.ToMouseWheelEvent : MouseWheelEvent option =
            match this with
            | MouseWheel m -> Some m
            | _            -> None

        member this.ToWindowEvent : WindowEvent option =
            match this with
            | WindowEvent w -> Some w
            | _             -> None

        member this.ToSysWMEvent : SysWMEvent option =
            match this with
            | SysWMEvent s -> Some s
            | _            -> None

        member this.ToTextEditingEvent : TextEditingEvent option =
            match this with
            | TextEditing e -> Some e
            | _             -> None

        member this.ToTextInputEvent : TextInputEvent option =
            match this with
            | TextInput i -> Some i
            | _           -> None

        member this.ToJoyAxisEvent : JoyAxisEvent option =
            match this with
            | JoyAxisMotion j -> Some j
            | _               -> None

        member this.ToJoyBallEvent : JoyBallEvent option =
            match this with 
            | JoyBallMotion j -> Some j
            | _               -> None

        member this.ToJoyHatEvent : JoyHatEvent option =
            match this with
            | JoyHatMotion j -> Some j
            | _              -> None

        member this.ToJoyButtonEvent : JoyButtonEvent option =
            match this with
            | JoyButtonDown j -> Some j
            | JoyButtonUp j   -> Some j
            | _ -> None

        member this.ToJoyDeviceEvent : JoyDeviceEvent option =
            match this with
            | JoyDeviceAdded j   -> Some j
            | JoyDeviceRemoved j -> Some j
            | _                  -> None


    let private toQuitEvent (event:SdlQuitEvent) :QuitEvent =
        {Timestamp = event.Timestamp}

    let private toKeyboardEvent (event:SdlKeyboardEvent) : KeyboardEvent =
        {Timestamp = event.Timestamp;
        WindowID = event.WindowID;
        State = event.State;
        Repeat = event.Repeat;
        Keysym = {Scancode = event.Keysym.Scancode |> enum<SDL.Keyboard.ScanCode>; Sym=event.Keysym.Sym;Mod=event.Keysym.Mod}}

    let private toMouseMotionEvent (event: SdlMouseMotionEvent) :MouseMotionEvent =
        {Timestamp=event.Timestamp;
        WindowID=event.WindowID;
        Which=event.Which;
        State=event.State;
        X=event.X;
        Y=event.Y;
        Xrel=event.Xrel;
        Yrel=event.Yrel}

    let private toMouseButtonEvent (event:SdlMouseButtonEvent) :MouseButtonEvent =
        {Timestamp = event.Timestamp;
        WindowID = event.WindowID;
        Which = event.Which;
        Button = event.Button;
        State = event.State;
        Clicks = event.Clicks;
        X = event.X;
        Y = event.Y}

    let private toMouseWheelEvent (event:SdlMouseWheelEvent) :MouseWheelEvent =
        {Timestamp=event.Timestamp;
        WindowID=event.WindowID;
        Which=event.Which;
        X=event.X;
        Y=event.Y;
        Direction=event.Direction |> int32 |> enum<SDL.Mouse.WheelDirection>}

    let private toAudioDeviceEvent (event:SdlAudioDeviceEvent) :AudioDeviceEvent =
        {Timestamp=event.Timestamp;
        Which=event.Which;
        Iscapture=event.Iscapture}    

    let private toControllerAxisEvent (event:SdlControllerAxisEvent) :ControllerAxisEvent =
        {Timestamp=event.Timestamp;
        Which=event.Which;
        Axis=event.Axis |> byteToAxis;
        Value=event.Value}
    
    let private toControllerButtonEvent (event:SdlControllerButtonEvent) :ControllerButtonEvent =
        {Timestamp=event.Timestamp;
        Which=event.Which;
        Button=event.Button |> byteToButton;
        State=event.State} 
    
    let private toControllerDeviceEvent (event:SdlControllerDeviceEvent) :ControllerDeviceEvent=
        {Timestamp=event.Timestamp;
        Which=event.Which}

    let private toJoyAxisEvent (event:SdlJoyAxisEvent) : JoyAxisEvent =
        {Timestamp = event.Timestamp;
        Which = event.Which;
        Axis = event.Axis;
        Value = event.Value}    

    let private toJoyBallEvent (event:SdlJoyBallEvent) : JoyBallEvent =
        {Timestamp = event.Timestamp;
        Which = event.Which;
        Ball = event.Ball;
        Xrel = event.Xrel;
        Yrel = event.Yrel}    

    let private toJoyButtonEvent (event:SdlJoyButtonEvent) : JoyButtonEvent =
        {Timestamp = event.Timestamp;
        Which = event.Which;
        Button = event.Button;
        State = event.State}    
            
    let private toJoyHatEvent (event:SdlJoyHatEvent) : JoyHatEvent =
        {Timestamp = event.Timestamp;
        Which = event.Which;
        Hat = event.Hat;
        Value = event.Value}    
            
    let private toJoyDeviceEvent (event:SdlJoyDeviceEvent) : JoyDeviceEvent =
        {Timestamp = event.Timestamp;
        Which = event.Which}    
            

    let private convertEvent (result: bool, event:SdlEvent) =
        match result, (event.Type |> int |> enum<EventType>) with
        | true, EventType.Quit                     -> event.Quit |> toQuitEvent |> Quit |> Some

        | true, EventType.KeyDown                  -> event.Key |> toKeyboardEvent |> KeyDown |> Some
        | true, EventType.KeyUp                    -> event.Key |> toKeyboardEvent |> KeyUp |> Some

        | true, EventType.MouseMotion              -> event.Motion |> toMouseMotionEvent |> MouseMotion |> Some

        | true, EventType.MouseButtonDown          -> event.Button |> toMouseButtonEvent |> MouseButtonDown |> Some
        | true, EventType.MouseButtonUp            -> event.Button |> toMouseButtonEvent |> MouseButtonUp |> Some

        | true, EventType.MouseWheel               -> event.Wheel |> toMouseWheelEvent |> MouseWheel |> Some

        | true, EventType.AudioDeviceAdded         -> event.Adevice |> toAudioDeviceEvent |> AudioDeviceAdded |> Some
        | true, EventType.AudioDeviceRemoved       -> event.Adevice |> toAudioDeviceEvent |> AudioDeviceRemoved |> Some

        | true, EventType.JoyAxisMotion            -> event.Jaxis |> toJoyAxisEvent |> JoyAxisMotion |> Some
        | true, EventType.JoyBallMotion            -> event.Jball |> toJoyBallEvent |> JoyBallMotion |> Some
        | true, EventType.JoyButtonDown            -> event.Jbutton |> toJoyButtonEvent |> JoyButtonDown |> Some
        | true, EventType.JoyButtonUp              -> event.Jbutton |> toJoyButtonEvent |> JoyButtonUp |> Some        
        | true, EventType.JoyHatMotion             -> event.Jhat |> toJoyHatEvent |> JoyHatMotion |> Some
        | true, EventType.JoyDeviceAdded           -> event.Jdevice |> toJoyDeviceEvent |> JoyDeviceAdded |> Some
        | true, EventType.JoyDeviceRemoved         -> event.Jdevice |> toJoyDeviceEvent |> JoyDeviceRemoved |> Some

        | true, EventType.ControllerAxisMotion     -> event.Caxis |> toControllerAxisEvent |> ControllerAxisMotion |> Some

        | true, EventType.ControllerButtonDown     -> event.Cbutton |> toControllerButtonEvent |> ControllerButtonDown |> Some
        | true, EventType.ControllerButtonUp       -> event.Cbutton |> toControllerButtonEvent |> ControllerButtonUp |> Some

        | true, EventType.ControllerDeviceAdded    -> event.Cdevice |> toControllerDeviceEvent |> ControllerDeviceAdded |> Some
        | true, EventType.ControllerDeviceRemoved  -> event.Cdevice |> toControllerDeviceEvent |> ControllerDeviceRemoved |> Some
        | true, EventType.ControllerDeviceRemapped -> event.Cdevice |> toControllerDeviceEvent |> ControllerDeviceRemapped |> Some

        | true, _                                  -> event.Type |> Other |> Some
        | _, _                                     -> None
    
    let wait (timeout:int option) =
        let mutable event = SdlEvent()
        let pevent = &&event
        let result = 
            match timeout with
            | None -> Native.SdlWaitEvent(pevent) = 1 
            | Some x -> Native.SdlWaitEventTimeout(pevent,x) = 1
        convertEvent (result,event)
    
    let poll () =
        let mutable event = SdlEvent()
        let pevent = &&event
        let result = Native.SdlPollEvent(pevent) = 1
        convertEvent (result,event)

    let pump = Native.SdlPumpEvents

    let register = Native.SdlRegisterEvents

    let has (eventType:uint32) :bool =
        Native.SdlHasEvent(eventType) <> 0

    let hasAny (minType:uint32) (maxType:uint32) : bool =
        Native.SdlHasEvents(minType,maxType) <> 0

    let flush (eventType:uint32) =
        Native.SdlFlushEvent(eventType)

    let flushAny (minType:uint32) (maxType:uint32) =
        Native.SdlFlushEvents(minType,maxType)