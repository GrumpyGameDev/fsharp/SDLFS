﻿namespace SDL

open System.Runtime.InteropServices
open System

module Platform = 

    module private Native =
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetPlatform")>]
        extern IntPtr WinSdlGetPlatform ()
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetPlatform")>]
        extern IntPtr LinuxSdlGetPlatform ()
    
        let SdlGetPlatform = (WinSdlGetPlatform, LinuxSdlGetPlatform) |> toNative


    let getPlatform () :string =
        Native.SdlGetPlatform()
        |> Utility.intPtrToStringAscii

module CpuInfo =
    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCPUCount")>]
        extern int WinSdlGetCPUCount()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCPUCacheLineSize")>]
        extern int WinSdlGetCPUCacheLineSize()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasRDTSC")>]
        extern int WinSdlHasRDTSC()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasAltiVec")>]
        extern int WinSdlHasAltiVec()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasMMX")>]
        extern int WinSdlHasMMX()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Has3DNow")>]
        extern int WinSdlHas3DNow()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE")>]
        extern int WinSdlHasSSE()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE2")>]
        extern int WinSdlHasSSE2()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE3")>]
        extern int WinSdlHasSSE3()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE41")>]
        extern int WinSdlHasSSE41()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE42")>]
        extern int WinSdlHasSSE42()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasAVX")>]
        extern int WinSdlHasAVX()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasAVX2")>]
        extern int WinSdlHasAVX2()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSystemRAM")>]
        extern int WinSdlGetSystemRAM()

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCPUCount")>]
        extern int LinuxSdlGetCPUCount()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetCPUCacheLineSize")>]
        extern int LinuxSdlGetCPUCacheLineSize()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasRDTSC")>]
        extern int LinuxSdlHasRDTSC()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasAltiVec")>]
        extern int LinuxSdlHasAltiVec()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasMMX")>]
        extern int LinuxSdlHasMMX()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Has3DNow")>]
        extern int LinuxSdlHas3DNow()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE")>]
        extern int LinuxSdlHasSSE()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE2")>]
        extern int LinuxSdlHasSSE2()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE3")>]
        extern int LinuxSdlHasSSE3()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE41")>]
        extern int LinuxSdlHasSSE41()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasSSE42")>]
        extern int LinuxSdlHasSSE42()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasAVX")>]
        extern int LinuxSdlHasAVX()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_HasAVX2")>]
        extern int LinuxSdlHasAVX2()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetSystemRAM")>]
        extern int LinuxSdlGetSystemRAM()

        let SdlGetCPUCount         = (WinSdlGetCPUCount         , LinuxSdlGetCPUCount        ) |> toNative
        let SdlGetCPUCacheLineSize = (WinSdlGetCPUCacheLineSize , LinuxSdlGetCPUCacheLineSize) |> toNative
        let SdlHasRDTSC            = (WinSdlHasRDTSC            , LinuxSdlHasRDTSC           ) |> toNative
        let SdlHasAltiVec          = (WinSdlHasAltiVec          , LinuxSdlHasAltiVec         ) |> toNative
        let SdlHasMMX              = (WinSdlHasMMX              , LinuxSdlHasMMX             ) |> toNative
        let SdlHas3DNow            = (WinSdlHas3DNow            , LinuxSdlHas3DNow           ) |> toNative
        let SdlHasSSE              = (WinSdlHasSSE              , LinuxSdlHasSSE             ) |> toNative
        let SdlHasSSE2             = (WinSdlHasSSE2             , LinuxSdlHasSSE2            ) |> toNative
        let SdlHasSSE3             = (WinSdlHasSSE3             , LinuxSdlHasSSE3            ) |> toNative
        let SdlHasSSE41            = (WinSdlHasSSE41            , LinuxSdlHasSSE41           ) |> toNative
        let SdlHasSSE42            = (WinSdlHasSSE42            , LinuxSdlHasSSE42           ) |> toNative
        let SdlHasAVX              = (WinSdlHasAVX              , LinuxSdlHasAVX             ) |> toNative
        let SdlHasAVX2             = (WinSdlHasAVX2             , LinuxSdlHasAVX2            ) |> toNative
        let SdlGetSystemRAM        = (WinSdlGetSystemRAM        , LinuxSdlGetSystemRAM       ) |> toNative

    let getCPUCount()         = Native.SdlGetCPUCount()
    let getCPUCacheLineSize() = Native.SdlGetCPUCacheLineSize()
    let hasRDTSC()            = Native.SdlHasRDTSC() <> 0
    let hasAltiVec()          = Native.SdlHasAltiVec() <> 0
    let hasMMX()              = Native.SdlHasMMX() <> 0
    let has3DNow()            = Native.SdlHas3DNow() <> 0
    let hasSSE()              = Native.SdlHasSSE() <> 0
    let hasSSE2()             = Native.SdlHasSSE2() <> 0
    let hasSSE3()             = Native.SdlHasSSE3() <> 0
    let hasSSE41()            = Native.SdlHasSSE41() <> 0
    let hasSSE42()            = Native.SdlHasSSE42() <> 0
    let hasAVX()              = Native.SdlHasAVX() <> 0
    let hasAVX2()             = Native.SdlHasAVX2() <> 0
    let getSystemRAM()        = Native.SdlGetSystemRAM()
