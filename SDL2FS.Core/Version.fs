﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System
open Microsoft.FSharp.NativeInterop

module Version =

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlVersion =
        struct
            val major: uint8
            val minor: uint8
            val patch: uint8
        end

    module private Native =
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetVersion")>]
        extern void WinSdlGetVersion(IntPtr ver)    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi, EntryPoint="SDL_GetRevision")>]
        extern IntPtr WinSdlGetRevision()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRevisionNumber")>]
        extern int WinSdlGetRevisionNumber()
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetVersion")>]
        extern void LinuxSdlGetVersion(IntPtr ver)    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi, EntryPoint="SDL_GetRevision")>]
        extern IntPtr LinuxSdlGetRevision()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRevisionNumber")>]
        extern int LinuxSdlGetRevisionNumber()
    
        let SdlGetRevision       = (WinSdlGetRevision      , LinuxSdlGetRevision      ) |> toNative
        let SdlGetRevisionNumber = (WinSdlGetRevisionNumber, LinuxSdlGetRevisionNumber) |> toNative
        let SdlGetVersion        = (WinSdlGetVersion       , LinuxSdlGetVersion       ) |> toNative

    type Version = 
        {
            Major: uint8
            Minor: uint8
            Patch: uint8
        }

    let get () :Version =
        let mutable version = SdlVersion()
        let pversion = NativePtr.toNativeInt &&version
        Native.SdlGetVersion(pversion)
        {
            Major = version.major
            Minor = version.minor
            Patch = version.patch
        }

    let getRevision () :string =
        Native.SdlGetRevision()
        |> intPtrToStringUtf8

    let getRevisionNumber() :int =
        Native.SdlGetRevisionNumber()