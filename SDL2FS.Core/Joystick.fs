﻿namespace SDL

#nowarn "9" "51"

open System.Runtime.InteropServices
open System
open SDL
open Microsoft.FSharp.NativeInterop

module Joystick =

    [<StructLayout(LayoutKind.Sequential)>]
    type internal SdlJoystickGUID =
        struct
            [<MarshalAs(UnmanagedType.ByValArray, SizeConst = 16, ArraySubType = UnmanagedType.U1)>]
            val Data: byte array
        end

    type PowerLevel = 
        | Unknown = -1
        | Empty   =  0
        | Low     =  1
        | Medium  =  2
        | Full    =  3
        | Wired   =  4

    type Joystick = Utility.Pointer

    type EventState =
        | Ignore = 0
        | Enable = 1

    module private Native =
        //[<DllImport(NativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="")>]
        //extern void SDL_JoystickGetGUIDString(SdlJoystickGUID guid, IntPtr pszGUID, int cbGUID);//don't need it
        //[<DllImport(NativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="")>]
        //extern SdlJoystickGUID SDL_JoystickGetGUIDFromString(IntPtr pchGUID);//don't need it

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_NumJoysticks")>]
        extern int WinSdlNumJoysticks()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNameForIndex")>]
        extern IntPtr WinSdlJoystickNameForIndex(int device_index)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickOpen")>]
        extern IntPtr WinSdlJoystickOpen(int device_index)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickClose")>]
        extern void WinSdlJoystickClose(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickFromInstanceID")>]
        extern IntPtr WinSdlJoystickFromInstanceID(int joyid)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickInstanceID")>]
        extern int WinSdlJoystickInstanceID(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickName")>]
        extern IntPtr WinSdlJoystickName(IntPtr joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetAttached")>]
        extern int WinSdlJoystickGetAttached(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickCurrentPowerLevel")>]
        extern int WinSdlJoystickCurrentPowerLevel(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetDeviceGUID")>]
        extern SdlJoystickGUID WinSdlJoystickGetDeviceGUID(int device_index)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetGUID")>]
        extern SdlJoystickGUID WinSdlJoystickGetGUID(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickEventState")>]
        extern int WinSdlJoystickEventState(int state)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickUpdate")>]
        extern void WinSdlJoystickUpdate()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumAxes")>]
        extern int WinSdlJoystickNumAxes(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetAxis")>]
        extern int16 WinSdlJoystickGetAxis(IntPtr  joystick, int axis)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumHats")>]
        extern int WinSdlJoystickNumHats(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetHat")>]
        extern byte WinSdlJoystickGetHat(IntPtr  joystick, int hat)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumBalls")>]
        extern int WinSdlJoystickNumBalls(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetBall")>]
        extern int WinSdlJoystickGetBall(IntPtr  joystick, int ball, int *dx, int *dy)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumButtons")>]
        extern int WinSdlJoystickNumButtons(IntPtr  joystick)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetButton")>]
        extern byte WinSdlJoystickGetButton(IntPtr  joystick, int button)

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_NumJoysticks")>]
        extern int LinuxSdlNumJoysticks()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNameForIndex")>]
        extern IntPtr LinuxSdlJoystickNameForIndex(int device_index)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickOpen")>]
        extern IntPtr LinuxSdlJoystickOpen(int device_index)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickClose")>]
        extern void LinuxSdlJoystickClose(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickFromInstanceID")>]
        extern IntPtr LinuxSdlJoystickFromInstanceID(int joyid)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickInstanceID")>]
        extern int LinuxSdlJoystickInstanceID(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickName")>]
        extern IntPtr LinuxSdlJoystickName(IntPtr joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetAttached")>]
        extern int LinuxSdlJoystickGetAttached(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickCurrentPowerLevel")>]
        extern int LinuxSdlJoystickCurrentPowerLevel(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetDeviceGUID")>]
        extern SdlJoystickGUID LinuxSdlJoystickGetDeviceGUID(int device_index)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetGUID")>]
        extern SdlJoystickGUID LinuxSdlJoystickGetGUID(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickEventState")>]
        extern int LinuxSdlJoystickEventState(int state)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickUpdate")>]
        extern void LinuxSdlJoystickUpdate()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumAxes")>]
        extern int LinuxSdlJoystickNumAxes(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetAxis")>]
        extern int16 LinuxSdlJoystickGetAxis(IntPtr  joystick, int axis)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumHats")>]
        extern int LinuxSdlJoystickNumHats(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetHat")>]
        extern byte LinuxSdlJoystickGetHat(IntPtr  joystick, int hat)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumBalls")>]
        extern int LinuxSdlJoystickNumBalls(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetBall")>]
        extern int LinuxSdlJoystickGetBall(IntPtr  joystick, int ball, int *dx, int *dy)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickNumButtons")>]
        extern int LinuxSdlJoystickNumButtons(IntPtr  joystick)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_JoystickGetButton")>]
        extern byte LinuxSdlJoystickGetButton(IntPtr  joystick, int button)

        let SdlNumJoysticks = (WinSdlNumJoysticks, LinuxSdlNumJoysticks) |> toNative
        let SdlJoystickNameForIndex = (WinSdlJoystickNameForIndex, LinuxSdlJoystickNameForIndex) |> toNative
        let SdlJoystickOpen = (WinSdlJoystickOpen, LinuxSdlJoystickOpen) |> toNative
        let SdlJoystickClose = (WinSdlJoystickClose, LinuxSdlJoystickClose) |> toNative
        let SdlJoystickFromInstanceID = (WinSdlJoystickFromInstanceID, LinuxSdlJoystickFromInstanceID) |> toNative
        let SdlJoystickInstanceID = (WinSdlJoystickInstanceID, LinuxSdlJoystickInstanceID) |> toNative
        let SdlJoystickName = (WinSdlJoystickName, LinuxSdlJoystickName) |> toNative
        let SdlJoystickGetAttached = (WinSdlJoystickGetAttached, LinuxSdlJoystickGetAttached) |> toNative
        let SdlJoystickCurrentPowerLevel = (WinSdlJoystickCurrentPowerLevel, LinuxSdlJoystickCurrentPowerLevel) |> toNative
        let SdlJoystickGetDeviceGUID = (WinSdlJoystickGetDeviceGUID, LinuxSdlJoystickGetDeviceGUID) |> toNative
        let SdlJoystickGetGUID = (WinSdlJoystickGetGUID, LinuxSdlJoystickGetGUID) |> toNative
        let SdlJoystickEventState = (WinSdlJoystickEventState, LinuxSdlJoystickEventState) |> toNative
        let SdlJoystickUpdate = (WinSdlJoystickUpdate, LinuxSdlJoystickUpdate) |> toNative
        let SdlJoystickNumAxes = (WinSdlJoystickNumAxes, LinuxSdlJoystickNumAxes) |> toNative
        let SdlJoystickGetAxis = (WinSdlJoystickGetAxis, LinuxSdlJoystickGetAxis) |> toNative
        let SdlJoystickNumHats = (WinSdlJoystickNumHats, LinuxSdlJoystickNumHats) |> toNative
        let SdlJoystickGetHat = (WinSdlJoystickGetHat, LinuxSdlJoystickGetHat) |> toNative
        let SdlJoystickNumBalls = (WinSdlJoystickNumBalls, LinuxSdlJoystickNumBalls) |> toNative
        let SdlJoystickGetBall = (WinSdlJoystickGetBall, LinuxSdlJoystickGetBall) |> toNative
        let SdlJoystickNumButtons = (WinSdlJoystickNumButtons, LinuxSdlJoystickNumButtons) |> toNative
        let SdlJoystickGetButton = (WinSdlJoystickGetButton, LinuxSdlJoystickGetButton) |> toNative

    let getCount = Native.SdlNumJoysticks

    let getNameForIndex (index:int) : string =
        Native.SdlJoystickNameForIndex(index)
        |> Utility.intPtrToStringAscii

    let create (index:int) : Joystick =
        new Joystick(Native.SdlJoystickOpen(index),fun p->Native.SdlJoystickClose(p))

    let createFromInstanceId (instanceId: int) : Joystick =
        new Joystick(Native.SdlJoystickFromInstanceID(instanceId),fun p->Native.SdlJoystickClose(p))

    let getInstanceId (joystick:Joystick) : int =
        Native.SdlJoystickInstanceID(joystick.Pointer)

    let getName (joystick:Joystick) : string =
        Native.SdlJoystickName(joystick.Pointer)
        |> Utility.intPtrToStringAscii

    let isAttached (joystick:Joystick): bool =
        Native.SdlJoystickGetAttached(joystick.Pointer) <> 0

    let getGuidForIndex (index:int) : Guid =
        let result = Native.SdlJoystickGetDeviceGUID(index)
        Guid(result.Data)

    let getGuid (joystick:Joystick) : Guid =
        let result = Native.SdlJoystickGetGUID(joystick.Pointer)
        Guid(result.Data)

    let setEventState (eventState: EventState) : bool =
        if eventState = EventState.Enable then
            Native.SdlJoystickEventState(1) = 1
        else
            Native.SdlJoystickEventState(0) = 0

    let getEventState () : EventState =
        match Native.SdlJoystickEventState(-1) with
        | 1 -> EventState.Enable
        | _ -> EventState.Ignore

    let update = Native.SdlJoystickUpdate

    let getPowerLevel (joystick:Joystick) : PowerLevel =
        Native.SdlJoystickCurrentPowerLevel(joystick.Pointer)
        |> LanguagePrimitives.EnumOfValue

    let getAxes (joystick:Joystick) : int =
        Native.SdlJoystickNumAxes(joystick.Pointer)

    let getBalls (joystick:Joystick) : int =
        Native.SdlJoystickNumBalls(joystick.Pointer)

    let getButtons (joystick:Joystick) : int =
        Native.SdlJoystickNumButtons(joystick.Pointer)

    let getHats (joystick:Joystick) : int =
        Native.SdlJoystickNumHats(joystick.Pointer)

    let getAxis (joystick:Joystick) (axis:int) : int16 =
        Native.SdlJoystickGetAxis(joystick.Pointer, axis)

    let getBall (joystick:Joystick) (ball:int) : (int*int) =
        let mutable x:int = 0
        let mutable y:int = 0
        let px = &&x
        let py = &&y

        Native.SdlJoystickGetBall(joystick.Pointer, ball, px, py)
        |> ignore

        (x,y)

    let getButton (joystick:Joystick) (button:int) : byte =
        Native.SdlJoystickGetButton(joystick.Pointer, button)

    let getHat (joystick:Joystick) (hat:int) : byte =
        Native.SdlJoystickGetHat(joystick.Pointer, hat)

