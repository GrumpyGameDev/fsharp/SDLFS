﻿namespace SDL

#nowarn "9"

open System.Runtime.InteropServices
open System

module Error = 

    module private Native = 
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ClearError")>]
        extern void WinSdlClearError()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetError")>]
        extern IntPtr WinSdlGetError()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi, EntryPoint="SDL_SetError")>]
        extern int WinSdlSetError(IntPtr fmt)
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_ClearError")>]
        extern void LinuxSdlClearError()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetError")>]
        extern IntPtr LinuxSdlGetError()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi, EntryPoint="SDL_SetError")>]
        extern int LinuxSdlSetError(IntPtr fmt)
    
        let SdlClearError = (WinSdlClearError , LinuxSdlClearError) |> toNative
        let SdlGetError   = (WinSdlGetError   , LinuxSdlGetError  ) |> toNative
        let SdlSetError   = (WinSdlSetError   , LinuxSdlSetError  ) |> toNative

    let set = withUtf8String (Native.SdlSetError >> ignore)

    let get () =
        Native.SdlGetError()
        |> Utility.intPtrToStringUtf8

    let clear = Native.SdlClearError