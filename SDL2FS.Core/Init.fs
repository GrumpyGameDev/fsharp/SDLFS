﻿namespace SDL

open System.Runtime.InteropServices
open System

[<AutoOpen>]
module internal NativeLib =
    [<Literal>]
    let WinNativeLibName = "SDL2.dll"
    [<Literal>]
    let LinuxNativeLibName = "SDL2"

    let isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)

    let toNative (windows,linux) = 
        if isWindows then 
            windows 
        else 
            linux    

[<AutoOpen>]
module Init = 

    type MainFunction = nativeint * IntPtr -> nativeint

    module private Native =
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Init")>]
        extern int WinSdlInit(uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Quit")>]
        extern void WinSdlQuit()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_InitSubSystem")>]
        extern int WinSdlInitSubSystem(uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_QuitSubSystem")>]
        extern void WinSdlQuitSubSystem(uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WasInit")>]
        extern uint32 WinSdlWasInit(uint32 flags)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetMainReady")>]
        extern void WinSdlSetMainReady()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WinRTRunApp")>]
        extern void WinSdlWinRTRunApp(MainFunction mainFunction,IntPtr reserved)

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Init")>]
        extern int LinuxSdlInit(uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_Quit")>]
        extern void LinuxSdlQuit()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_InitSubSystem")>]
        extern int LinuxSdlInitSubSystem(uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_QuitSubSystem")>]
        extern void LinuxSdlQuitSubSystem(uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WasInit")>]
        extern uint32 LinuxSdlWasInit(uint32 flags)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetMainReady")>]
        extern void LinuxSdlSetMainReady()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WinRTRunApp")>]
        extern void LinuxSdlWinRTRunApp(MainFunction mainFunction,IntPtr reserved)

        let SdlInit          = (WinSdlInit          , LinuxSdlInit)          |> toNative
        let SdlQuit          = (WinSdlQuit          , LinuxSdlQuit)          |> toNative
        let SdlInitSubSystem = (WinSdlInitSubSystem , LinuxSdlInitSubSystem) |> toNative
        let SdlQuitSubSystem = (WinSdlQuitSubSystem , LinuxSdlQuitSubSystem) |> toNative
        let SdlWasInit       = (WinSdlWasInit       , LinuxSdlWasInit)       |> toNative
        let SdlSetMainReady  = (WinSdlSetMainReady  , LinuxSdlSetMainReady)  |> toNative
        let SdlWinRTRunApp   = (WinSdlWinRTRunApp   , LinuxSdlWinRTRunApp)   |> toNative

    [<Flags>]
    type Flags =
        | None           = 0x00000000
        | Timer          = 0x00000001
        | Audio          = 0x00000010
        | Video          = 0x00000020
        | Joystick       = 0x00000200
        | Haptic         = 0x00001000
        | GameController = 0x00002000
        | Events         = 0x00004000
        | Everything     = 0x0000FFFF

    type System(flags:Flags list) =
        let mutable disposed = false
        do
            Native.SdlInit(flags |> List.reduce (|||) |> uint32) |> ignore
        member x.InitSubSystem (flags: Flags list) :bool =
            0 = Native.SdlInitSubSystem(flags |> List.reduce (|||) |> uint32)
        member x.QuitSubSystem (flags: Flags list) :unit =
            Native.SdlQuitSubSystem(flags |> List.reduce (|||) |> uint32)
        member x.WasInit (flags:Flags list) :bool =
            let reduced = 
                flags 
                |> List.reduce (|||) 
            reduced
            |> uint32
            |> Native.SdlWasInit
            |> int
            |> enum<Flags>
            |> (=) reduced
        member private x.Dispose(disposing:bool) =
            match disposed, disposing with
            | false, true ->
                Native.SdlQuit()
            | _ ->
                ()
            disposed <- true
        override x.Finalize() = x.Dispose(false)
        interface IDisposable with
            member x.Dispose() =
                x.Dispose(true)
                GC.SuppressFinalize(x)

    let setMainReady () =
        Native.SdlSetMainReady()

    let winRTRunApp (mainFunction:MainFunction) =
        Native.SdlWinRTRunApp(mainFunction, IntPtr.Zero)
