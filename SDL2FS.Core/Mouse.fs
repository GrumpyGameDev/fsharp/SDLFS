﻿namespace SDL

#nowarn "51"

open System
open System.Runtime.InteropServices
open SDL

[<AutoOpen>]
module Mouse =

    type Button =
        | Left   = 1
        | Middle = 2
        | Right  = 3
        | X1     = 4
        | X2     = 5


    type SystemCursor =
        | Arrow     = 0
        | IBeam     = 1
        | Wait      = 2
        | Crosshair = 3
        | Waitarrow = 4
        | SizeNWSE  = 5
        | SizeNESW  = 6
        | SizeWE    = 7
        | SizeNS    = 8
        | SizeAll   = 9
        | No        = 10
        | Hand      = 11

    type WheelDirection = 
        | Normal  = 0
        | Flipped = 1

    module internal Native = 
    
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetMouseFocus")>]
        extern IntPtr WinSdlGetMouseFocus()//TODO: expose
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetMouseState")>]
        extern uint32 WinSdlGetMouseState(int *x, int *y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetGlobalMouseState")>]
        extern uint32 WinSdlGetGlobalMouseState(int *x, int *y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRelativeMouseState")>]
        extern uint32 WinSdlGetRelativeMouseState(int *x, int *y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WarpMouseInWindow")>]
        extern void WinSdlWarpMouseInWindow(IntPtr window,int x, int y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WarpMouseGlobal")>]
        extern int WinSdlWarpMouseGlobal(int x, int y)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRelativeMouseMode")>]
        extern int WinSdlSetRelativeMouseMode(int enabled)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CaptureMouse")>]
        extern int WinSdlCaptureMouse(int enabled)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRelativeMouseMode")>]
        extern int WinSdlGetRelativeMouseMode()
    
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetMouseFocus")>]
        extern IntPtr LinuxSdlGetMouseFocus()//TODO: expose
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetMouseState")>]
        extern uint32 LinuxSdlGetMouseState(int *x, int *y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetGlobalMouseState")>]
        extern uint32 LinuxSdlGetGlobalMouseState(int *x, int *y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRelativeMouseState")>]
        extern uint32 LinuxSdlGetRelativeMouseState(int *x, int *y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WarpMouseInWindow")>]
        extern void LinuxSdlWarpMouseInWindow(IntPtr window,int x, int y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_WarpMouseGlobal")>]
        extern int LinuxSdlWarpMouseGlobal(int x, int y)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_SetRelativeMouseMode")>]
        extern int LinuxSdlSetRelativeMouseMode(int enabled)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_CaptureMouse")>]
        extern int LinuxSdlCaptureMouse(int enabled)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GetRelativeMouseMode")>]
        extern int LinuxSdlGetRelativeMouseMode()
    
        let SdlGetMouseFocus         = (WinSdlGetMouseFocus         , LinuxSdlGetMouseFocus        ) |> toNative
        let SdlGetMouseState         = (WinSdlGetMouseState         , LinuxSdlGetMouseState        ) |> toNative
        let SdlGetGlobalMouseState   = (WinSdlGetGlobalMouseState   , LinuxSdlGetGlobalMouseState  ) |> toNative
        let SdlGetRelativeMouseState = (WinSdlGetRelativeMouseState , LinuxSdlGetRelativeMouseState) |> toNative
        let SdlWarpMouseInWindow     = (WinSdlWarpMouseInWindow     , LinuxSdlWarpMouseInWindow    ) |> toNative
        let SdlWarpMouseGlobal       = (WinSdlWarpMouseGlobal       , LinuxSdlWarpMouseGlobal      ) |> toNative
        let SdlSetRelativeMouseMode  = (WinSdlSetRelativeMouseMode  , LinuxSdlSetRelativeMouseMode ) |> toNative
        let SdlCaptureMouse          = (WinSdlCaptureMouse          , LinuxSdlCaptureMouse         ) |> toNative
        let SdlGetRelativeMouseMode  = (WinSdlGetRelativeMouseMode  , LinuxSdlGetRelativeMouseMode ) |> toNative

    type MouseState =
        {Position:Geometry.Point;Buttons:Set<Button>}

    let private toButtonSet (buttons:uint32) : Set<Button> =
        (Set.empty, [(Button.Left,0x1u);(Button.Middle,0x2u);(Button.Right,0x4u);(Button.X1,0x8u);(Button.X2,0x10u)])
        ||> List.fold (fun buttonSet (button,flag) ->
            if buttons ||| flag = flag then
                buttonSet
                |> Set.add button
            else
                buttonSet)

    let getMouseState () :MouseState =
        let mutable x:int = 0
        let mutable y:int = 0
        let px = &&x
        let py = &&y
        let buttons = Native.SdlGetMouseState(px,py)
        {Position = {X = x;Y = y};Buttons = buttons |> toButtonSet}

    let getGlobalMouseState () :MouseState =
        let mutable x:int = 0
        let mutable y:int = 0
        let px = &&x
        let py = &&y
        let buttons = Native.SdlGetGlobalMouseState(px, py)
        {Position = {X = x;Y = y};Buttons = buttons |> toButtonSet}

    let getRelativeMouseState () :MouseState =
        let mutable x:int = 0
        let mutable y:int = 0
        let px = &&x
        let py = &&y
        let buttons = Native.SdlGetRelativeMouseState(px, py)
        {Position = {X = x;Y = y};Buttons = buttons |> toButtonSet}

    let warpMouseInWindow (window:SDL.Utility.Pointer) (xy:Geometry.Point) :unit =
        Native.SdlWarpMouseInWindow(window.Pointer,xy.X, xy.Y)

    let warpMouseInCurrentWindow (xy:SDL.Geometry.Point) :unit =
        Native.SdlWarpMouseInWindow(IntPtr.Zero,xy.X,xy.Y)

    let warpMouseGlobal (xy:SDL.Geometry.Point) :bool =
        0 = Native.SdlWarpMouseGlobal(xy.X,xy.Y)

    let setRelativeMouseMode (flag:bool) :bool = 
        0 = Native.SdlSetRelativeMouseMode(if flag then 1 else 0)

    let getRelativeMouseMove () :bool =
        0 <> Native.SdlGetRelativeMouseMode()

    let captureMouse (flag:bool) : bool =
        0 = Native.SdlCaptureMouse(if flag then 1 else 0)