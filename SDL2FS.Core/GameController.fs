﻿namespace SDL

#nowarn "9"

open System.Runtime.InteropServices
open System
open SDL
open Microsoft.FSharp.NativeInterop

module GameController =

    type BindType =
        | None   = 0
        | Button = 1
        | Axis   = 2
        | Hat    = 3

    [<StructLayout(LayoutKind.Explicit, Size=12)>]
    type internal SdlGameControllerButtonBind =
        struct
            [<FieldOffset(0)>]
            val BindType: int
            [<FieldOffset(4)>]
            val Axis: int
            [<FieldOffset(4)>]
            val Hat: int
            [<FieldOffset(8)>]
            val HatMask: int
        end

    type Axis =
        | Invalid      = -1
        | LeftX        =  0
        | LeftY        =  1
        | RightX       =  2
        | RightY       =  3
        | TriggerLeft  =  4
        | TriggerRight =  5

    let internal byteToAxis (b: uint8) : Axis =
        match b with
        | 0uy -> Axis.LeftX
        | 1uy -> Axis.LeftY
        | 2uy -> Axis.RightX
        | 3uy -> Axis.RightY
        | 4uy -> Axis.TriggerLeft
        | 5uy -> Axis.TriggerRight
        | _ -> Axis.Invalid

    type Button =
        | Invalid       = -1
        | A             =  0
        | B             =  1
        | X             =  2
        | Y             =  3
        | Back          =  4
        | Guide         =  5
        | Start         =  6
        | LeftStick     =  7
        | RightStick    =  8
        | LeftShoulder  =  9
        | RightShoulder = 10 
        | DPadUp        = 11
        | DPadDown      = 12
        | DPadLeft      = 13
        | DPadRight     = 14

    let internal byteToButton(b:uint8) : Button =
        match b with
        |  0uy -> Button.A
        |  1uy -> Button.B
        |  2uy -> Button.X
        |  3uy -> Button.Y
        |  4uy -> Button.Back
        |  5uy -> Button.Guide
        |  6uy -> Button.Start
        |  7uy -> Button.LeftStick
        |  8uy -> Button.RightStick
        |  9uy -> Button.LeftShoulder
        | 10uy -> Button.RightShoulder
        | 11uy -> Button.DPadUp
        | 12uy -> Button.DPadDown
        | 13uy -> Button.DPadLeft
        | 14uy -> Button.DPadRight
        | _    -> Button.Invalid

    type EventState =
        | Ignore = 0
        | Enable = 1

    module private Native =

        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerOpen")>]
        extern IntPtr WinSdlGameControllerOpen(int joystick_index)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerClose")>]
        extern void WinSdlGameControllerClose(IntPtr gamecontroller)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IsGameController")>]
        extern int WinSdlIsGameController(int joystick_index)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerNameForIndex")>]
        extern IntPtr WinSdlGameControllerNameForIndex(int joystick_index)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerName")>]
        extern IntPtr WinSdlGameControllerName(IntPtr gamecontroller)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetAttached")>]
        extern int WinSdlGameControllerGetAttached(IntPtr gamecontroller)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerEventState")>]
        extern int WinSdlGameControllerEventState(int state)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerUpdate")>]
        extern void WinSdlGameControllerUpdate()
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetAxisFromString")>]
        extern int WinSdlGameControllerGetAxisFromString(IntPtr pchString)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetStringForAxis")>]
        extern IntPtr WinSdlGameControllerGetStringForAxis(int axis)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetAxis")>]
        extern int16 WinSdlGameControllerGetAxis(IntPtr gamecontroller, int axis)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetButtonFromString")>]
        extern int WinSdlGameControllerGetButtonFromString(IntPtr pchString)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetStringForButton")>]
        extern IntPtr WinSdlGameControllerGetStringForButton(int button)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetButton")>]
        extern uint8 WinSdlGameControllerGetButton(IntPtr gamecontroller,int button)
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerAddMappingsFromRW")>]
        extern int WinSdlGameControllerAddMappingsFromRW(IntPtr rw, int freerw )
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerAddMapping")>]
        extern int WinSdlGameControllerAddMapping(IntPtr mappingString )
        [<DllImport(WinNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerMapping")>]
        extern IntPtr WinSdlGameControllerMapping(IntPtr gamecontroller)

        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerOpen")>]
        extern IntPtr LinuxSdlGameControllerOpen(int joystick_index)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerClose")>]
        extern void LinuxSdlGameControllerClose(IntPtr gamecontroller)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_IsGameController")>]
        extern int LinuxSdlIsGameController(int joystick_index)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerNameForIndex")>]
        extern IntPtr LinuxSdlGameControllerNameForIndex(int joystick_index)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerName")>]
        extern IntPtr LinuxSdlGameControllerName(IntPtr gamecontroller)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetAttached")>]
        extern int LinuxSdlGameControllerGetAttached(IntPtr gamecontroller)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerEventState")>]
        extern int LinuxSdlGameControllerEventState(int state)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerUpdate")>]
        extern void LinuxSdlGameControllerUpdate()
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetAxisFromString")>]
        extern int LinuxSdlGameControllerGetAxisFromString(IntPtr pchString)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetStringForAxis")>]
        extern IntPtr LinuxSdlGameControllerGetStringForAxis(int axis)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetAxis")>]
        extern int16 LinuxSdlGameControllerGetAxis(IntPtr gamecontroller, int axis)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetButtonFromString")>]
        extern int LinuxSdlGameControllerGetButtonFromString(IntPtr pchString)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetStringForButton")>]
        extern IntPtr LinuxSdlGameControllerGetStringForButton(int button)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerGetButton")>]
        extern uint8 LinuxSdlGameControllerGetButton(IntPtr gamecontroller,int button)
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerAddMappingsFromRW")>]
        extern int LinuxSdlGameControllerAddMappingsFromRW(IntPtr rw, int freerw )
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerAddMapping")>]
        extern int LinuxSdlGameControllerAddMapping(IntPtr mappingString )
        [<DllImport(LinuxNativeLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint="SDL_GameControllerMapping")>]
        extern IntPtr LinuxSdlGameControllerMapping(IntPtr gamecontroller)

        let SdlGameControllerOpen                = (WinSdlGameControllerOpen                , LinuxSdlGameControllerOpen               ) |> toNative
        let SdlGameControllerClose               = (WinSdlGameControllerClose               , LinuxSdlGameControllerClose              ) |> toNative
        let SdlIsGameController                  = (WinSdlIsGameController                  , LinuxSdlIsGameController                 ) |> toNative
        let SdlGameControllerNameForIndex        = (WinSdlGameControllerNameForIndex        , LinuxSdlGameControllerNameForIndex       ) |> toNative
        let SdlGameControllerName                = (WinSdlGameControllerName                , LinuxSdlGameControllerName               ) |> toNative
        let SdlGameControllerGetAttached         = (WinSdlGameControllerGetAttached         , LinuxSdlGameControllerGetAttached        ) |> toNative
        let SdlGameControllerEventState          = (WinSdlGameControllerEventState          , LinuxSdlGameControllerEventState         ) |> toNative
        let SdlGameControllerUpdate              = (WinSdlGameControllerUpdate              , LinuxSdlGameControllerUpdate             ) |> toNative
        let SdlGameControllerGetAxisFromString   = (WinSdlGameControllerGetAxisFromString   , LinuxSdlGameControllerGetAxisFromString  ) |> toNative
        let SdlGameControllerGetStringForAxis    = (WinSdlGameControllerGetStringForAxis    , LinuxSdlGameControllerGetStringForAxis   ) |> toNative
        let SdlGameControllerGetAxis             = (WinSdlGameControllerGetAxis             , LinuxSdlGameControllerGetAxis            ) |> toNative
        let SdlGameControllerGetButtonFromString = (WinSdlGameControllerGetButtonFromString , LinuxSdlGameControllerGetButtonFromString) |> toNative
        let SdlGameControllerGetStringForButton  = (WinSdlGameControllerGetStringForButton  , LinuxSdlGameControllerGetStringForButton ) |> toNative
        let SdlGameControllerGetButton           = (WinSdlGameControllerGetButton           , LinuxSdlGameControllerGetButton          ) |> toNative
        let SdlGameControllerAddMappingsFromRW   = (WinSdlGameControllerAddMappingsFromRW   , LinuxSdlGameControllerAddMappingsFromRW  ) |> toNative
        let SdlGameControllerAddMapping          = (WinSdlGameControllerAddMapping          , LinuxSdlGameControllerAddMapping         ) |> toNative
        let SdlGameControllerMapping             = (WinSdlGameControllerMapping             , LinuxSdlGameControllerMapping            ) |> toNative

        //extern IntPtr SDL_GameControllerMappingForGUID( SDL_JoystickGUID guid )

        //extern IntPtr SDL_GameControllerFromInstanceID(SDL_JoystickID joyid)
        //extern SDL_Joystick *SDL_GameControllerGetJoystick(SDL_GameController *gamecontroller)

        //extern SdlGameControllerButtonBind SDL_GameControllerGetBindForAxis(SDL_GameController *gamecontroller, SDL_GameControllerAxis axis)
        //extern SdlGameControllerButtonBind SDL_GameControllerGetBindForButton(SDL_GameController *gamecontroller, SDL_GameControllerButton button)

    type Controller(ptr:IntPtr, destroyFunc: IntPtr->unit) = 
        inherit SDL.Utility.Pointer(ptr, destroyFunc)
        new (ptr:IntPtr) = new Controller(ptr, Native.SdlGameControllerClose)

    let create (index: int)  : Controller =
        new Controller(Native.SdlGameControllerOpen(index), fun p -> Native.SdlGameControllerClose(p))

    let isController (index: int) : bool =
        Native.SdlIsGameController(index) <> 0

    let getNameForIndex (index: int) : string =
        Native.SdlGameControllerNameForIndex(index)
        |> Utility.intPtrToStringAscii

    let getName (controller: Controller) : string =
        Native.SdlGameControllerName(controller.Pointer)
        |> Utility.intPtrToStringAscii

    let isAttached (controller: Controller) : bool = 
        Native.SdlGameControllerGetAttached(controller.Pointer) <> 0

    let setEventState (eventState: EventState) : bool =
        if eventState = EventState.Enable then
            Native.SdlGameControllerEventState(1) = 1
        else
            Native.SdlGameControllerEventState(0) = 0

    let getEventState () : EventState =
        match Native.SdlGameControllerEventState(-1) with
        | 1 -> EventState.Enable
        | _ -> EventState.Ignore

    let update = Native.SdlGameControllerUpdate

    let getAxisFromName (name:string) :Axis = 
        name
        |> Utility.withAsciiString (fun ptr->Native.SdlGameControllerGetAxisFromString(ptr))
        |> LanguagePrimitives.EnumOfValue

    let getAxisName (axis: Axis) : string =
        Native.SdlGameControllerGetStringForAxis(axis |> int)
        |> Utility.intPtrToStringAscii

    let getAxisValue (axis:Axis)  (controller:Controller):int16 =
        Native.SdlGameControllerGetAxis(controller.Pointer, axis |> int)

    let getButtonFromName (name:string) :Axis = 
        name
        |> Utility.withAsciiString (fun ptr->Native.SdlGameControllerGetButtonFromString(ptr))
        |> LanguagePrimitives.EnumOfValue

    let getButtonName (button: Button) : string =
        Native.SdlGameControllerGetStringForButton(button |> int)
        |> Utility.intPtrToStringAscii

    let getButtonValue (button: Button) (controller:Controller):uint8 =
        Native.SdlGameControllerGetButton(controller.Pointer, button |> int)