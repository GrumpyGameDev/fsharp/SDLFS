﻿open SDL

let rec eventPump (renderer:Render.Renderer) : unit =
    renderer
    |>* Render.clear

    renderer
    |>* Render.present

    match Event.wait None with
    | Some (Event.Quit _) ->
        ()
    | _ ->
        eventPump renderer

let windowTitle = "Boilerplate"
let windowWidth = 640
let windowHeight = 480

[<EntryPoint>]
let main argv =
    use system = new System([
                            Flags.Video
                            Flags.Events
                        ])  

    use window = Window.create (windowTitle, Window.Position.Centered, windowWidth, windowHeight, Window.Flags.None)

    use renderer = Render.create window None Render.Flags.Accelerated

    renderer
    |> eventPump
    
    0
