﻿
[<EntryPoint>]
let main (argv:string array) : int =
    match argv.Length with
    | 0 ->
        printfn "Usage:"
        printfn "SDL2FS.ImageDemo n"
        printfn "Shows a particular demonstration (n) of the image loading capabilities. Clicking the X on the windows or hitting the escape key closes the window."
        printfn "Values for n:"
        printfn "\t1 - loading a BMP and displaying it)"
    | n ->
        match System.Int32.TryParse(argv.[0]) with
        | true, 1 ->
            Example1.demo()
        | _  ->
            ()
    0
